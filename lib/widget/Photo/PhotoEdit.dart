import 'dart:typed_data';
import 'dart:ui';
import 'package:crop/crop.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:scsc_veteran/common/Constantes.dart';

class PhotoEdit extends StatefulWidget {
  AvatarEdit() {}

  @override
  _PhotoEditPageState createState() => _PhotoEditPageState();
}

class _PhotoEditPageState extends State<PhotoEdit> {

  XFile? image=null;
  Uint8List? buffer=null;

  final controller = CropController();

  @override
  void initState() {
    super.initState();
    init();
  }

  init() {
    pickImage();
  }

  void majPhoto() async {
    var pixelRatio = MediaQuery.of(context).devicePixelRatio;

    var newPhoto = await controller.crop(pixelRatio: 1);

    dynamic bd = await newPhoto.toByteData(format: ImageByteFormat.png);

    
    
    //XFile f = XFile.fromData(bd.buffer.asUint8List());
    //f.saveTo("avatar.jpg");
    Navigator.of(context).pop(bd.buffer.asUint8List());
    //uploadImage.data=;
    //startUpload(bd.buffer.asUint8List());
  }


  void pickImage() async {
    ImagePicker _picker = ImagePicker();
        _picker.pickImage(
          source: ImageSource.gallery,
          //maxWidth: Constantes.MAX_SIZE_PHOTO_AVATAR,
          //maxHeight: Constantes.MAX_SIZE_PHOTO_AVATAR,
          imageQuality: 100,
        ).then((img) {
          if(img != null){
            image=img;
            img.readAsBytes().then((tab) {
              setState(() {
                buffer=tab;
              });
            });
          }
        });

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            buffer != null
                ? IconButton(
                    iconSize: 44,
                    icon: Icon(
                      Icons.check,
                      color: Colors.white,
                    ),
                    tooltip: 'valide',
                    onPressed: () {
                      majPhoto();
                    })
                : Container(),
          ],
          title: Text("Photo edit"),
        ),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            //imageFile != null ? Image.file(imageFile) : ca,
            _getImage(),
            // _getTraitement(),

          ],
        )),
        //bottomNavigationBar: _getCmd()
    );
  }

  Widget _getImage() {
    double radius = (MediaQuery.of(context).size.width / 2) - 40;
    Widget w;
    if(buffer == null){
      w=Container();
    }
    else {
      w = _getCropImage(2 * radius);
    }

    return (w);
  }

  /*Widget _getCmd() {
    List<Widget> l = [];
    double heightIcon = 40;

    Widget iconPick = IconButton(
        color: Colors.white,
        icon: Icon(Icons.photo_library),
        iconSize: heightIcon,
        tooltip: 'Albums',
        onPressed: () {
          pickImage();
        });

    l.add(IconButton(
        color: Colors.white,
        icon: Icon(Icons.rotate_left),
        iconSize: heightIcon,
        tooltip: 'Rotate',
        onPressed: () {
          controller.rotation -= 45;
        }));

    l.add(IconButton(
        color: Colors.white,
        icon: Icon(Icons.remove),
        iconSize: heightIcon,
        tooltip: 'moins',
        onPressed: () {
          controller.scale -= 0.25;
        }));

    l.add(IconButton(
        color: Colors.white,
        icon: Icon(Icons.add),
        iconSize: heightIcon,
        tooltip: 'plus',
        onPressed: () {
          controller.scale += 0.25;
        }));

    Container c = Container(
        decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.secondary //Colors.yellow,
            ), //       <--- BoxDecoration here
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            iconPick,
            uploadImage != null
                ? Row(
                    children: l,
                  )
                : Container()
          ],
        ));

    return (c);
  }*/

  Widget _getCropImage(double radius) {
    BoxShape shape = BoxShape.circle;

    Crop c = Crop(
      /*onChanged: (decomposition) {
        print(
            "Scale : ${decomposition.scale}, Rotation: ${decomposition.rotation}, translation: ${decomposition.translation}");
      },*/
      controller: controller,
      shape: shape,
      backgroundColor: Theme.of(context).colorScheme.secondary,
      //dimColor: Colors.white, //,

      child: Image(
          image: MemoryImage(buffer!)), //Image.asset('images/bk_terrain.jpg',fit: BoxFit.cover,),
    );

    Container con = Container(width: radius, height: radius, child: c);
    return (con);
  }




}
