import 'dart:async';

import 'package:flutter/material.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntMessage.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/MessageService.dart';
import 'package:scsc_veteran/services/UserService.dart';
import 'package:intl/intl.dart';

import 'ElemMessage.dart';

class Message extends StatefulWidget {
  @override
  _MessagePageState createState() => _MessagePageState();
}

class _MessagePageState extends State<Message> {


  StreamSubscription<List<EntMessage>>? _subMessages=null;

  EntUser? _currentUser=null;
  List<EntMessage> _listeMessage=[];
  late ItemScrollController itemScrollController;
  late ItemPositionsListener itemPositionListener;
  final ctrlMessage = TextEditingController();
  bool flagWait=false;

  @override
  void initState() {
    super.initState();
    itemScrollController = ItemScrollController();
    itemPositionListener = ItemPositionsListener.create();
    _initUser();
    _initMessage();

  }

  @override
  void dispose() {
    Util.freeSub(_subMessages);

    super.dispose();
  }

  void _initUser(){
    UserService.getInstance().getCurrentUser().then((u) {
      setState(() {
        _currentUser = u;
      });
    });
  }

  void _initMessage(){
    _subMessages=MessageService.getInstance().getObsMessage().listen((l) {
      Util.getPersistInt(Constantes.PERSIST_LAST_ID_MESSAGE).then((val) {
        //print("val =" + val.toString());
        int tsLastMessage = val ?? DateTime.now().millisecondsSinceEpoch;
        l.forEach((e) {
          if(e.ts <= tsLastMessage){
          //  print("ancien msg = " +e.ts.toString() +  " -> last=" +   tsLastMessage.toString());
            e.flagNew=false;
          }
          else{
            //print("nouveau msg = " +e.ts.toString() +  " -> last=" +   tsLastMessage.toString());
            e.flagNew=true;
          }
        });

        if(l.length > 0){
          tsLastMessage = l.last.date.millisecondsSinceEpoch;
        }
        Util.setPersistInt(Constantes.PERSIST_LAST_ID_MESSAGE, tsLastMessage);
        if(this.mounted) {
          setState(() {

            _listeMessage=l;

          });
          Timer(Duration(milliseconds: 50), () => _jumpToFirstNewMessage());
        }
        //print("NB message = " +l.length.toString());




        //_endWait();
      });
    });
  }

  /*void _nav(String route) {
    Navigator.of(context).pushNamed('$route');
  }*/

  void sendMessage(){
   // _startWait();
    EntMessage message = new EntMessage();
    message.userId = _currentUser!.id;
    message.date = DateTime.now();
    message.message = ctrlMessage.text;
    message.titre = "";

    //FocusScope.of(context).unfocus();
    ctrlMessage.text="";
    //Timer(Duration(milliseconds: 300), () {
      MessageService.getInstance().createMessage(message);
    //});
  }

  _jumpToFirstNewMessage() {
    if (_listeMessage.length > 0) {
      int indexNew = _listeMessage.length - 1;
      EntMessage elem;
      for (int i = _listeMessage.length - 1; i >= 0; i--) {
        elem = _listeMessage[i];
        if (elem.flagNew == true) {
          indexNew = i;
        } else {
          break;
        }
      }
      //print("************** indexNew="+indexNew.toString());
      //itemScrollController.jumpTo(index: indexNew);

      itemScrollController.scrollTo(
          index: indexNew,
          duration: Duration(seconds: 1),
          curve: Curves.easeInOutCubic);
    }
  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });

    //EasyLoading.show(status :msg);
  }

  void _endWait() {
    setState(() {
      flagWait = true;
    });

    //EasyLoading.show(status :msg);
  }

  Widget waitBar() {
    Container c = Container(
      color: Colors.white,
      height: 10,
      child: flagWait
          ? LinearProgressIndicator(
          backgroundColor: Colors.white,
          valueColor: new AlwaysStoppedAnimation<Color>(
              Theme.of(context).colorScheme.primary))
          : Container(),
    );
    return (c);
  }
  @override
  Widget build(BuildContext context) {
    //if (_currentUser == null) return Container();
    return new Scaffold(
          backgroundColor: Color.fromRGBO(240,240,240, 0.9),
          appBar: AppBar(
            title: Text("Messages"),
          ),
          body:ListView(
            children: [
              waitBar(),
              _getBodyMessage(),
             _getInputMessage()
            ]),


    );
  }

  Widget _getInputMessage() {
    Widget input= Container(
      width: MediaQuery.of(context).size.width - 50,
      margin: const EdgeInsets.only(bottom:8.0),
      padding: EdgeInsets.only(left: 4, right: 4),
      child: TextField(
        minLines: 1,
        maxLines: 5,
        controller: ctrlMessage,
        style: TextStyle(
          fontSize: 18,
        ),

        decoration: InputDecoration(

          filled: true,
          fillColor: Colors.white,
          contentPadding: EdgeInsets.only(top:8,left :20,bottom:4),
          hintStyle: TextStyle(fontSize: 18),
            hintText: "Messages",
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide(
                color: Colors.white,
                width: 1,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
               borderSide: BorderSide(
                color: Colors.white,
                width: 1,
              ),
            ),
            ),
      ),
    );

    Widget cmd =  IconButton(
        icon: Icon(Icons.send),
        tooltip: 'Envoyer',
        color:Theme.of(context).colorScheme.primary,
        onPressed: sendMessage);

    Row w = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [input,cmd],
    );
    return(w);
  }

  Widget _getBodyMessage() {

    Widget body = Container(
        height: MediaQuery.of(context).size.height-150,
        //color:Colors.blue,
        //decoration: BoxDecoration(),
        //alignment: Alignment.topLeft,
        margin: const EdgeInsets.only(bottom: 10),
        child: _getScroolViewMessage()
    );
    return (body);
  }

  Widget _getScroolViewMessage(){
    String lastDate = "";
    String strDate = "";

    Widget w=ScrollablePositionedList.builder(
        itemScrollController: itemScrollController,
        itemCount: _listeMessage.length,
        itemBuilder: (BuildContext ctxt, int index) {
          bool flagMe = false;
          if (_currentUser != null) {
            flagMe = (_currentUser!.id == _listeMessage[index].userId);
          }
          strDate = DateFormat.MMMMEEEEd("fr_FR")
              .format(_listeMessage[index].date);
          Column c;
          List<Widget> lw = [];

          if (strDate != lastDate) {
            lw.add(_getElemDate(strDate));
            lastDate = strDate;
          }
          lw.add(_getElemMesage(index, flagMe));
          c = new Column(children: lw);

          return c;
        }
        );
    return(w);
  }

  Widget _getScroolViewMessageNew(){
    String lastDate = "";
    String strDate = "";

    Widget w=ListView.builder(
        //itemScrollController: itemScrollController,
        itemCount: _listeMessage.length,
        itemBuilder: (BuildContext ctxt, int index) {
          bool flagMe = false;
          if (_currentUser != null) {
            flagMe = (_currentUser!.id == _listeMessage[index].userId);
          }
          strDate = DateFormat.MMMMEEEEd("fr_FR")
              .format(_listeMessage[index].date);
          Column c;
          List<Widget> lw = [];

          if (strDate != lastDate) {
            lw.add(_getElemDate(strDate));
            lastDate = strDate;
          }
          lw.add(_getElemMesage(index, flagMe));
          c = new Column(children: lw);

          return c;
        }
    );
    return(w);
  }

  Widget _getElemDate(String strDate) {
    Widget w =
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: MediaQuery.of(context).size.width / 2,
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.primary,
                    borderRadius: BorderRadius.circular(20),
                    //border: Border.all()
                  ),
                  margin: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
                  padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
                  child: Text(strDate,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white)))
            ]
    );
    return (w);
  }

  Widget _getElemMesage(int index,bool flagMe){
   if(index < 0){
     print("index < 0 !!!");
     return(Container());
   }
    Widget w=
    Row(
    mainAxisAlignment: flagMe
    ? MainAxisAlignment.end
        : MainAxisAlignment.start,
    children: <Widget>[
    //Text("dd")
    ElemMessage(_listeMessage[index], flagMe)
    ],
    );

   return(w);


  }
}
