

import 'package:cloud_firestore/cloud_firestore.dart';

class EntEquipe {
  late String id;
  late String adresse;
  late String libelle;
  late String terrain;
  late double latitude;
  late double longitude;
  late String logo;

  EntEquipe(){
    id = "";
    libelle ="";
    logo="";
    terrain="";
    adresse="";
    latitude=0;
    longitude=0;

  }

  static EntEquipe fromRTDB(Map data, String id) {
    EntEquipe e = EntEquipe();
    e.id = id;
    e.libelle = data["libelle"] ?? "";
    e.terrain = data["terrain"] ?? "";
    e.adresse = data["adresse"] ?? "";

    if (data["latitude"] != 0) {
      e.latitude = data["latitude"] ?? 0;
    } else {
      e.latitude = 0;
    }
    if (data["longitude"] != 0) {
      e.longitude = data["longitude"] ?? 0;
    } else {
      e.longitude = 0;
    }
    return (e);
  }

  static EntEquipe fromFB(QueryDocumentSnapshot o) {
    EntEquipe e = EntEquipe();
    e.id = o.id;
    e.libelle = o.get("libelle");
    e.terrain = o.get("terrain");
    e.adresse = o.get("adresse");
    e.logo = o.get("logo");

    if (o.get("latitude") != 0) {
      e.latitude = o.get("latitude");
    } else {
      e.latitude = 0;
    }
    if (o.get("longitude") != 0) {
      e.longitude = o.get("longitude");
    } else {
      e.longitude = 0;
    }
    return (e);
  }

  Object toFB() {
    Object o = {
      "libelle": libelle,
      "terrain": terrain,
      "adresse": adresse,
      "latitude": latitude,
      "longitude": longitude,
      "logo":logo
    };
    return (o);
  }

  EntEquipe clone(){
    EntEquipe e = new EntEquipe();
    e.id=id;
    e.adresse=adresse;
    e.libelle=libelle;
    e.terrain=terrain;
    e.latitude=latitude;
    e.longitude=longitude;
    e.logo=logo;
    return(e);
  }
}
