import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:scsc_veteran/common/Util.dart';

class EntDepense  {
  late String id;
  late String libelle;
  late DateTime date;
  late int montant;

  EntDepense() {
    id = "";
    libelle = "";
    montant = 0;
    date = DateTime.now();
  }

  EntDepense clone(){
    EntDepense d = EntDepense();
    d.montant=montant;
    d.id=id;
    d.libelle=libelle;
    d.date=date;

    return(d);

  }
  static EntDepense fromFB(QueryDocumentSnapshot o) {
    EntDepense v = EntDepense();
    v.id = o.id;
    v.libelle = o.get("libelle") ?? "";
    v.date = Util.getDateTimeFromFB(o.get("date"));
    v.montant = o.get("montant");
    return (v);
  }

  Object toFB() {
    Object o = {
      "libelle": libelle,
      "montant": montant,
      "date": date,
    };
    return (o);
  }
}
