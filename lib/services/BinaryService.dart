
import 'dart:async';
import 'dart:io' as io;
import 'dart:typed_data';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:scsc_veteran/models/EntEquipe.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/models/StatusUpload.dart';

class BinaryService {
  static BinaryService? _singleton;


  static void razSingleton() {
    _singleton = null;
    
  }

  static BinaryService  getInstance() {
    if (_singleton == null) {
      _singleton = BinaryService._();
    }
    return (_singleton!);
  }

  BinaryService._() {

  }

  Future<String> getDefaultEquipeLogo(){
    Completer<String> completer = new Completer<String>();

    FirebaseStorage.instance.ref('equipes/logo_provence.png')
        .getDownloadURL().then((url) {
      completer.complete(url) ;
    });

    return(completer.future);
  }

  Future<StatusUpload> majAvatar(Uint8List buffer,EntUser u) async{
    String path="avatars/" + u.id;
    return(_uploadData(buffer,path,"(Avatar)"));
  }

  Future<StatusUpload> majLogoEquipe(Uint8List buffer,EntEquipe e) async{
    String path="equipes/" + e.id;
    return(_uploadData(buffer,path,"(Logo Equipe)"));
  }

  Future<StatusUpload> _uploadData(Uint8List buffer,String path,String strType) async {


    UploadTask uploadTask;


    // Create a Reference to the file
    Reference ref = FirebaseStorage.instance.ref().child(path);

    /*final metadata = firebase_storage.SettableMetadata(
        contentType: 'image/jpeg',
        customMetadata: {'picked-file-path': file.path});
*/

    uploadTask = ref.putData(buffer);

    return(_gestTask(uploadTask,strType));

  }

  Future<StatusUpload> _uploadFile(XFile file,String path,String strType) async {


    UploadTask uploadTask;


    // Create a Reference to the file
    Reference ref = FirebaseStorage.instance.ref().child(path);

    /*final metadata = firebase_storage.SettableMetadata(
        contentType: 'image/jpeg',
        customMetadata: {'picked-file-path': file.path});
*/

    if (kIsWeb) {
      uploadTask = ref.putData(await file.readAsBytes());
    } else {
      uploadTask = ref.putFile(io.File(file.path));
    }
    return(_gestTask(uploadTask,strType));

  }

  Future<StatusUpload> _gestTask(UploadTask? ut,String strType) async {
    Completer<StatusUpload> completer = new Completer<StatusUpload>();
    StatusUpload statusUpload = StatusUpload();
    statusUpload.strType=strType;

    do {
      if(ut == null){
        statusUpload.reason="UploadTask null";
        completer.complete(statusUpload);
        break;
      }

      ut.snapshotEvents.listen((TaskSnapshot snapshot) {
        switch(snapshot.state) {
          case TaskState.error:
            statusUpload.reason="Erreur lors de l'upload";
            completer.complete(statusUpload);
            break;
          case TaskState.canceled:
            statusUpload.reason="Annulation de l'upload";
            completer.complete(statusUpload);
            break;
          case TaskState.paused:
            statusUpload.reason="Interruption de l'upload";
            completer.complete(statusUpload);
            break;
          case TaskState.running:
            break;
          case TaskState.success:
            snapshot.ref.getDownloadURL().then((url) {
              statusUpload.url=url;
              statusUpload.status=true;
              completer.complete(statusUpload);
            }).catchError((e){
              statusUpload.reason="Erreur lors de la récupération de l'url :" + e.toString();
              completer.complete(statusUpload);
            });
            break;
        }
      }, onError: (e) {
        statusUpload.reason="Erreur lors de l'upload :" + e.toString();
        completer.complete(statusUpload);
      });
    } while (false);

    return (completer.future);
  }

}
