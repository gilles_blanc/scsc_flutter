import 'dart:async';
import 'package:flutter/material.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntMessage.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/MessageService.dart';
import 'package:scsc_veteran/services/UserService.dart';

class MessageCreate extends StatefulWidget {
  Message() {}

  @override
  _MessageCreatePageState createState() => _MessageCreatePageState();
}

final ctrlMessage = TextEditingController();
final ctrlTitre = TextEditingController();

class _MessageCreatePageState extends State<MessageCreate>
    with SingleTickerProviderStateMixin {
  bool flagWait = false;
  bool flagWaitPJ = false;
  EntUser? currentUser=null;
  //bool flagSend;
  late AnimationController _controller;

  //List<PJMessage> _listPJ;
  int nbTask = 0;
  //List<Future<bool>> _listeFutureCompletePJ;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 2))
          ..repeat();

    //flagSend=true;
    //_listeFutureCompletePJ=new List<Future<bool>>();
    //_listPJ = [];
    //currentMessage = new EntMessage();
    UserService.getInstance().getCurrentUser().then((u) {
      setState(() {
        currentUser = u;
      });
    });
    //percent=30;
  }

  @override
  dispose() {
    _controller.dispose(); // you need this
    super.dispose();
  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });

    //EasyLoading.show(status :msg);
  }

  void _endWait() {
    setState(() {
      flagWait = true;
    });

    //EasyLoading.show(status :msg);
  }

  /*void _addPJ(PJMessage o) {
    o.pj.resultUpload.then((result) {
      if (result == true) {
        endUplaodPJ(null);
      } else {
        endUplaodPJ(o);
      }
    });
    //Future.delayed(const Duration(milliseconds: 1000), startUploadPJ);

    setState(() {
      _listPJ.add(o);
      nbTask++;
    });
  }*/

  /*void _delPJ(PJMessage o) {
    //int index=_listPJ.lastIndexOf(o);
    bool flagPerform = o.cancelUploadPJ();
    if (flagPerform == false) {
      endUplaodPJ(o);
    }
  }

  void endUplaodPJ(PJMessage o) {
    setState(() {
      if (o != null) {
        _listPJ.remove(o);
        print("FIN UPLOAD CANCEL");
      } else {
        print("FIN UPLOAD OK");
      }
      nbTask--;
      if (nbTask < 0) nbTask = 0;
    });
  }

  void _addFromAlbum() async {
    CritereObject c = new CritereObject();
    c.maxSize = Constantes.MAX_SIZE_PHOTO_PJ;
    c.maxSizeTB = Constantes.MAX_SIZE_THUMB_PJ;
    c.type = Constantes.TYPE_OBJECT_IMAGE;
    c.source = Constantes.SOURCE_OBJECT_FILE;
    c.flagAvatar = false;
    c.nbMax = -1;

    setState(() {
      flagWaitPJ = true;
    });
    UtilObject.getPJUpload(c).then((lo) {
      print("retourULATION AJOUT PJ nb =" + lo.length.toString());
      lo.forEach((element) {
        element.pathFB = "pj/" + currentUser.id + "/" + element.name;
        element.pathThumbFB = element.pathFB + "_TB";
        _addPJ(PJMessage(element));
      });
      setState(() {
        flagWaitPJ = false;
      });
    }).catchError((onError) {
      print("ANNULATION AJOUT PJ");
    });
  }

  void _addFromFile() async {
    CritereObject c = new CritereObject();
    c.maxSize = Constantes.MAX_SIZE_PHOTO_PJ;
    c.maxSizeTB = Constantes.MAX_SIZE_THUMB_PJ;
    c.type = Constantes.TYPE_OBJECT_OTHER;
    c.source = Constantes.SOURCE_OBJECT_FILE;
    c.flagAvatar = false;
    c.nbMax = -1;

    setState(() {
      flagWaitPJ = true;
    });
    UtilObject.getPJUpload(c).then((lo) {
      lo.forEach((element) {
        element.pathFB = "pj/" + currentUser.id + "/" + element.name;
        element.pathThumbFB = element.pathFB + "_TB";
        _addPJ(PJMessage(element));
      });
      setState(() {
        flagWaitPJ = false;
      });
    });
  }
*/
  void onSend() async {
    //print("from : " + user.id);
    _startWait();
    EntMessage message = new EntMessage();
    message.userId = currentUser!.id;
    message.date = DateTime.now();
    message.message = ctrlMessage.text;
    message.titre = ctrlTitre.text;

    /*_listPJ.forEach((element) {
      message.tabPJ.add(element.pj);
    });
*/
    MessageService.getInstance().createMessage(message).then((mes) {
      if (mes == null) {
        _endWait();
        Util.error(context, "Erreur lors de l'envoi du message");
      } else {
        /*EntNotification notif =
            EntNotification(Constantes.TYPE_NOTIFICATION_MESSAGE, currentUser);
        UserService.getInstance().sendNotification(notif);*/
        _endWait();
        close();
      }
    });
  }

  close() {
    Navigator.of(context).pop(true);
  }

  Future<bool> confirmeCancel() async {
    /*if ((_listPJ.length == 0) &&
        (ctrlMessage.text.length < 10) &&
        (ctrlTitre.text.length < 10)) {
      Completer completer = new Completer<bool>();
      completer.complete(true);
      return (completer.future);
    }*/
    Future<bool> ret =
        Util.confirme(context, "Confirmez-vous l'annulation du message ?");
    ret.then((val) {
      if (val == true) {
        /*_listPJ.forEach((element) {
          element.cancelUploadPJ();
        });*/
      }
    });
    return (ret);
  }

  @override
  Widget build(BuildContext context) {
    if (currentUser == null) return Container();
    return new WillPopScope(
        onWillPop: confirmeCancel,
        child: new Scaffold(
            appBar: AppBar(
              title: Text("Nouveau message"),
              actions: <Widget>[_getCmdSend()],
            ),
            bottomNavigationBar: _getCmd(),
            body: Container(
                child: ListView(
              //mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                waitBar(),
                messageHeader(),
                messageBody(),
              ],
            ))));
  }

 /* Widget _getThumbViewCmd() {
    ListView lv = new ListView.builder(
        itemCount: _listPJ.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext ctxt, int index) {
          //ImageProvider img = MemoryImage(currentMessage.tabPJ[index].dataTB);
          Padding p = new Padding(
              padding: EdgeInsets.only(right: 20),
              child: GestureDetector(
                  onDoubleTap: () {
                    _delPJ(_listPJ[index]);
                  },
                  child: _listPJ[
                      index] /* ProgressiveImage(
                    placeholder: img,
                    thumbnail: img,
                    image: img,
                    height: 50,
                    width: 50,
                    fit: BoxFit.contain,
                  )*/
                  ));
          return p;
        });

    String mes = "Aucune piece jointe";
    if (flagWaitPJ == true) {
      mes = "Récupération des pieces jointes en cours ...";
    }

    Container c = Container(
        //margin: const EdgeInsets.all(10.0),
        //padding: const EdgeInsets.all(5.0),
        width: MediaQuery.of(context).size.width - 120,
        height: 70,
        decoration: BoxDecoration(
            //border: Border.all(),
            //borderRadius: BorderRadius.all(Radius.circular(10)),
            color: Theme.of(context).colorScheme.secondary //Colors.yellow,

            ),
        //       <--- BoxDecoration here

        child: _listPJ.length > 0 ? lv : Text(mes));

    return (c);
  }
*/
  Widget waitBar() {
    Container c = Container(
      color: Colors.white,
      height: 10,
      child: flagWait
          ? LinearProgressIndicator(
              backgroundColor: Colors.white,
              valueColor: new AlwaysStoppedAnimation<Color>(
                  Theme.of(context).colorScheme.primary))
          : Container(),
    );
    return (c);
  }

 /* List<int> getNbPJ() {
    List<int> ret = [];
    int nbPhoto = 0;
    int nbBin = 0;
    _listPJ.forEach((element) {
      if (element.pj.typeFile == Constantes.TYPE_FILE_IMAGE)
        nbPhoto++;
      else
        nbBin++;
    });
    ret.add(nbPhoto);
    ret.add(nbBin);

    return (ret);
  }
*/
  Widget _getCmd() {
    List<Widget> l = [];
    double heightIcon = 40;

    Widget w;
    //List<int> lNb = getNbPJ();

    Container loader = Container(
      margin: EdgeInsets.only(left: 15, right: 5),
      child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(
        Theme.of(context).colorScheme.primary,
      )),
      height: 30.0,
      width: 30.0,
    );

    /*IconButton ibPhoto = IconButton(
        color: Colors.white,
        icon: Icon(Icons.camera_alt),
        iconSize: heightIcon,
        tooltip: 'photos',
        onPressed: () {
          _addFromAlbum();
        });

    IconButton ibBin = IconButton(
        color: Colors.white,
        icon: Icon(Icons.file_copy_outlined),
        iconSize: heightIcon,
        tooltip: 'fichiers',
        onPressed: () {
          _addFromFile();
        });

    Badge badgePhoto = Badge(
      badgeColor: Theme.of(context).colorScheme.primary,
      toAnimate: true,
      badgeContent: Text(
        lNb[0].toString(),
        style: TextStyle(color: Colors.white),
      ),
      child: ibPhoto,
      position: BadgePosition(right: 0, top: 0),
    );

    Badge badgeBin = Badge(
      badgeColor: Theme.of(context).colorScheme.primary,
      toAnimate: true,
      badgeContent: Text(
        lNb[1].toString(),
        style: TextStyle(color: Colors.white),
      ),
      child: ibBin,
      position: BadgePosition(right: 0, top: 0),
    );
*/
   /* if (flagWaitPJ == true) {
      l.add(loader);
    } else {
      if (lNb[0] > 0)
        l.add(badgePhoto);
      else
        l.add(ibPhoto);

      if (!kIsWeb) {
        if (lNb[1] > 0)
          l.add(badgeBin);
        else
          l.add(ibBin);
      }
    }*/
    Container c = Container(
        //margin: const EdgeInsets.all(10.0),
        //padding: const EdgeInsets.all(5.0),
        //width: w,
        decoration: BoxDecoration(
            //border: Border.all(),
            //borderRadius: BorderRadius.all(Radius.circular(10)),
            color: Theme.of(context).colorScheme.secondary //Colors.yellow,

            ), //       <--- BoxDecoration here
        height: 80,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: l,
            ),
            //_getThumbViewCmd()
          ],
        ));
    return (c);
  }

 /* Widget _getProgress() {
    String libelle = "En attente ...";
    Color colLibelle = Colors.white;
    Color colBk = Colors.red; //Theme.of(context).colorScheme.secondary;
    Color colFore = Colors.blue; //Theme.of(context).colorScheme.primary;

    if (30 > 0) {
      libelle = 30.round().toString() + "%";
      colBk = Theme.of(context).colorScheme.secondary;
      colFore = Theme.of(context).colorScheme.primary;
      colLibelle = Colors.white;
    }

    /*if (flagComplete == true) {
      colFore = Colors.grey;
      colBk = Colors.grey;

      libelle = "Chargement terminé";
      colLibelle = Colors.white;
    }*/

    /*RoundedProgressBar pb = RoundedProgressBar(
      childCenter: Text(libelle, style: TextStyle(color: colLibelle)),
      style: RoundedProgressBarStyle(
        backgroundProgress: colBk,
        colorProgress: colFore,
        colorProgressDark: colFore,
        borderWidth: 0,
        widthShadow: 0,
      ),
      percent: 30,
      height: 25,
    );*/

    Row r = Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Flexible(
          child: Padding(
              padding: EdgeInsets.only(top: 5, bottom: 5, right: 5, left: 5),
              child: pb),
        )
      ],
    );

    return (r);
  }*/

  Widget _getCmdSend() {
    IconButton ib = IconButton(
        icon: Icon(Icons.check),
        color: Colors.white,
        onPressed: () {
          onSend();
        });

    Container loader = Container(
      margin: EdgeInsets.only(right: 5),
      child: _getLoader(),
    );

    if ((nbTask <= 0) && (flagWaitPJ == false))
      return (ib);
    else
      return (loader);
  }

  Widget _getLoader() {
    Widget w;
    //EntRefAction action =Referentiel.getInstance().getRefActionFromId(widget.action.id);

    if (_controller != null) {
      w = AnimatedBuilder(
        animation: _controller,
        builder: (_, child) {
          return Transform.rotate(
            angle: _controller.value * 2 * 3.14,
            child: child,
          );
        },
        child: Icon(Icons.upload_rounded)
        /*Image.asset(
            'assets/images/antidote.png',
            scale: 1
        )*/
        ,
      );
    } else {
      w = CircularProgressIndicator();
    }
    return (w);
  }

  Widget messageBody() {
    Container c = Container(
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: TextField(
          autofocus: true,
          controller: ctrlMessage,
          style: TextStyle(
            fontSize: 20,
          ),
          keyboardType: TextInputType.multiline,
          maxLines: 12,
          decoration: InputDecoration(
            labelText: "Message",
          ),
        ));
    return (c);
  }

  Widget messageHeader() {
    Container c = Container(
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: TextField(
          controller: ctrlTitre,
          style: TextStyle(
            fontSize: 20,
          ),
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            labelText: "Titre",
          ),
        ));

    return (c);
  }
}
