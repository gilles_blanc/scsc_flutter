// ignore: file_names
import 'dart:async';
import 'package:collection/src/iterable_extensions.dart';
import 'package:flutter/material.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntActivite.dart';
import 'package:scsc_veteran/models/EntEquipe.dart';
import 'package:scsc_veteran/models/EntMatch.dart';
import 'package:scsc_veteran/models/EntPresenceActivite.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/models/ParamActivite.dart';
import 'package:scsc_veteran/services/EquipeService.dart';
import 'package:scsc_veteran/services/MatchService.dart';
import 'package:scsc_veteran/services/PresenceActiviteService.dart';

class ElemActivite extends StatefulWidget {
  late EntActivite activite;
  EntUser? currentUser;
  late int mode;

  ElemActivite(EntActivite a,EntUser? u,[int mode =Constantes.MODE_ELEM_ACTIVITE_SAISIE]) {
    activite = a;
    currentUser = u;
    this.mode=mode;
  }

  @override
  _ElemActivitePageState createState() => _ElemActivitePageState();
}

class _ElemActivitePageState extends State<ElemActivite> {
  StreamSubscription<List<EntEquipe>>? _subListEquipe=null;
  late StreamSubscription<List<EntPresenceActivite>> _subListPresenceActivite;
  EntPresenceActivite? _myPresence=null;
  int _nbPresent =0;
  EntEquipe? equipe=null;
  List<EntPresenceActivite> _listePresence=[];

  @override
  void initState() {
    super.initState();

    _init();

  }

  @override
  void didUpdateWidget(ElemActivite ea) {

    super.didUpdateWidget(ea);
    _init();

  }

  _init(){
    if(widget.activite.isMatch() == true) _initMatch(widget.activite as EntMatch);

    _initPresence();
  }

  @override
  void dispose() {
    Util.freeSub(_subListEquipe);
    Util.freeSub(_subListPresenceActivite);
    super.dispose();
  }

  _initMatch(EntMatch m) {
    _subListEquipe=EquipeService.getInstance().getObsEquipes().listen((le) {
      setState(() {
        equipe = le.firstWhereOrNull((element) => element.id == m.idEquipe);
        //print("Match id=" + m.id +" contre " + equipe!.libelle);
      });
    });
  }

  _initPresence() {
    _subListPresenceActivite=PresenceActiviteService.getInstance().getObsListPrensenceActivites(widget.activite).listen((lp) {
      setState(() {
        _listePresence = lp;
        _analysePresence();
      });
    });
  }

  Widget _getFixedRow(Widget w,int len,Color col){
    Widget ret=Container(
        width: MediaQuery.of(context).size.width-len,
        //color:col,
        /*child : Align(
          alignment: Alignment.centerLeft,
          child: te,
        )*/
      child : w

    );

    return(ret);
  }
  Widget _getInfoBody(){

    int len1=220;
    int len2=250;
    Widget w=Container();

    if(widget.activite.isMatch() == true){
      if(equipe == null) return(w);
      Text te=Text(equipe!.libelle ,
          style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).colorScheme.primary));


      Text ts=Text("scsc" ,
          style: TextStyle(fontSize: 15));
      Text tsep=Text("-" ,
          style: TextStyle(fontSize: 15));
      List<Widget> lr = [];
      if((widget.activite as EntMatch).flagDomicile == true){
        lr.add(ts);
        //lr.add(tsep);
        lr.add(_getFixedRow(te,len1,Colors.yellow));
      }
      else{

        lr.add(_getFixedRow(te,len1,Colors.yellow));
        //lr.add(tsep);
        lr.add(ts);
      }
      w=_getFixedRow(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children:lr,
      ),165,Colors.red);
    }
    if(widget.activite.isTraining() == true){
      w=Text("Entrainement" ,
          style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).colorScheme.primary));
    }

    return(w);
  }


  void _analysePresence(){
    int nb=0;
    EntPresenceActivite? ep=null;

    _listePresence.forEach((element) {
      if(element.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_YES) nb++;
      if(widget.currentUser != null){
        if(element.idUser == widget.currentUser!.id){
          ep=element;
        }
      }
    });

    setState(() {
      _nbPresent=nb;
      _myPresence=ep;
    });
  }

  void _togglePresence(){
    if((_myPresence != null) && (widget.mode == Constantes.MODE_ELEM_ACTIVITE_SAISIE)) {
      if(_myPresence!.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_YES) _myPresence!.codePresence =Constantes.CODE_PRESENCE_ACTIVITE_NO;
      else _myPresence!.codePresence =Constantes.CODE_PRESENCE_ACTIVITE_YES;

      PresenceActiviteService.getInstance().majPresenceActivite(_myPresence!);
    }
  }

  void _detailActivite(){
    Navigator.of(context).pushNamed('/DetailActivite',arguments: ParamActivite(widget.activite,widget.currentUser!));
  }

  void _deleteActivite(){
    switch(widget.activite.typeActivite){
          case Constantes.TYPE_ACTIVITE_NEXT_MATCH:
          case Constantes.TYPE_ACTIVITE_MATCH:
            String nom= equipe != null ? equipe!.libelle : "Inconnue";
          Util.confirme(context,"Confirmez-vous la suppression du match contre " + nom +" ?").then((ret) {
            if (ret == true) {
              MatchService.getInstance().deleteMatch(
                  widget.activite as EntMatch);
            }
          });
          break;
    }
  }

  void _editActivite(){
    switch(widget.activite.typeActivite){
      case Constantes.TYPE_ACTIVITE_NEXT_MATCH:
      case Constantes.TYPE_ACTIVITE_MATCH:
      Navigator.of(context).pushNamed('/EditMatch',arguments: widget.activite as EntMatch);
      break;
    }

  }


  Widget _getLogo(){
    Widget w=Container();

    if(widget.activite.isMatch() == true){
      if (equipe != null) {
        Widget l=Util.getLogoEquipe(equipe!);
        w=Padding(
          padding: EdgeInsets.only(right:10),
          child : l
        );
      }
    }


    return(w);
  }

  Widget _getButtonInfo(){
    Widget w=Container();
    if(widget.mode == Constantes.MODE_ELEM_ACTIVITE_SAISIE) {
      w = IconButton(
          icon: Icon(Icons.info_outline),
          tooltip: 'Détail',
          onPressed: _detailActivite);
    }

    return(w);

  }

  Widget _getButtonAdmin(){
    Widget w=Container();

      Row r=Row(
        children: [
          IconButton(
              icon: Icon(Icons.delete),
              tooltip: 'Suppression',
              onPressed: _deleteActivite),
          IconButton(
              icon: Icon(Icons.edit),
              tooltip: 'Modification',
              onPressed: _editActivite)
        ],
      );

      w=Align(
          alignment: Alignment.centerRight,
          child : r
      );



    return(w);

  }

  edit() {

  }

  @override
  Widget build(BuildContext context) {
    Widget w;
    Container c = Container(
      margin: EdgeInsets.only(top: 5, bottom: 5, right: 10, left: 10),
      padding: EdgeInsets.only(left: 5, bottom: 5),
      width: MediaQuery.of(context).size.width - 100,
      decoration: BoxDecoration(
        color: widget.activite.date.isAfter(DateTime.now()) ? Colors.white:Colors.black12,
        border: Border.all(
          color: Theme.of(context).colorScheme.primary,
          width: 2,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      child:Column(
          //crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              _getHeader(),
              _getBody(),
            ],
          )
    );

    GestureDetector gd = GestureDetector(
        onTap: _togglePresence,
        onLongPress: _detailActivite,
        child: c);
    if(widget.mode == Constantes.MODE_ELEM_ACTIVITE_SAISIE) w=gd;
    else w =c;
    return (w);
  }

  Widget _getFirstLine() {
    double titreSize = 15;

    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(widget.activite.getTitre(), style: TextStyle(fontSize: titreSize)),
        Text(widget.activite.getSousTitre(), style: TextStyle(fontSize: titreSize))
      ],
    );
    return (r);
  }

  Widget _getSecondLine() {
    double dateSize = 15;

    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(widget.activite.getDateShort(), style: TextStyle(fontSize: dateSize))

      ],
    );
    return (r);
  }

  Widget _getSecondLineEx() {
    double dateSize = 15;

    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(widget.activite.getDateShort(), style: TextStyle(fontSize: dateSize)),
        Text(" " + widget.activite.getHour(), style: TextStyle(fontSize: dateSize))

      ],
    );
    return (r);
  }

  Widget _getThirdLine() {
    double dateSize = 15;

    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(widget.activite.getHour(), style: TextStyle(fontSize: dateSize))

      ],
    );
    return (r);
  }

  Widget _getHeader() {

    List<Widget> lignes = [];
    List<Widget> cols = [];
    switch(widget.mode) {
      case Constantes.MODE_ELEM_ACTIVITE_SAISIE:
        lignes.add(_getFirstLine());
        lignes.add(_getSecondLine());
        lignes.add(_getThirdLine());
        cols.add(_getButtonInfo());
        break;
      case Constantes.MODE_ELEM_ACTIVITE_ADMIN:
        lignes.add(_getFirstLine());
        lignes.add(_getSecondLineEx());

        break;
    }

    cols.add(Padding(
        padding:EdgeInsets.only(right:5),
        child:Column(
          children:lignes,
        )

    ));

    Row r = Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: cols
    );

    return(r);

    }

  Widget _getBody() {

    double textSize = 15;

    List<Widget> lw = [];
    switch(widget.mode) {
      case Constantes.MODE_ELEM_ACTIVITE_SAISIE:
        lw.add(Util.getIconBadge(
            _nbPresent,
            Icon(Icons.accessibility),
            Theme.of(context).colorScheme.primary,
            Theme.of(context).colorScheme.secondary,
            _togglePresence,
            40,
            true));
        lw.add(_getInfoBody());
        lw.add(_getLogo());
        break;
      case Constantes.MODE_ELEM_ACTIVITE_ADMIN:
        lw.add(_getInfoBody());
        lw.add(_getButtonAdmin());
        break;
    }


    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: lw,
    );


    Container c = Container(
        margin: EdgeInsets.only(top: 15, bottom: 15),
        //padding: EdgeInsets.only(left: 5, bottom: 10),
        /*decoration: BoxDecoration(
          border: Border.all(
            color: Theme.of(context).colorScheme.primary,
            width: 2,
          ),
          borderRadius: BorderRadius.circular(12),
        ),*/
        child: r
    );
    return (c);
  }
}
