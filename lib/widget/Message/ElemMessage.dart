import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scsc_veteran/models/EntMessage.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/UserService.dart';
import '../Avatar/Avatar.dart';

class ElemMessage extends StatelessWidget {
  late EntMessage _msg;
  late bool _flagMe;

  ElemMessage(EntMessage m, bool flagMe) {
    _msg = m;
    _flagMe = flagMe;
  }

  @override
  Widget build(BuildContext context) {
    Color c = Theme.of(context).colorScheme.primary;
    Color colorNew = Color.fromRGBO(c.red, c.green, c.blue, 0.3);

    return new Container(
        width: (5 * MediaQuery.of(context).size.width) / 6,
        decoration: BoxDecoration(
          color: (_msg.flagNew && !_flagMe) ? colorNew : Color.fromRGBO(230, 230, 230, 1.0),
          borderRadius: BorderRadius.circular(20),
          //border: Border.all()
        ),
        margin: EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
        padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _getHeader(),
            _getCorpsText(),
           // _getThumbViewPJ(context),
            /*listePJ.length > 0 ? Column(
              children: listePJ,
            ) : Text("Pas de pj")*/
          ],
        ));
  }

  Widget _getCorpsText() {
    String corps = _msg.message;

    TextStyle textstyleCorps = TextStyle(fontSize: 20);
    return Text(corps, style: textstyleCorps, textAlign: TextAlign.start);
  }

  /*Widget _getThumbViewPJ(BuildContext context) {
    if (_msg.tabPJ.length == 0) return (Container());
    double wCont;
    double hCont;
    double wImg;
    double hImg;

    ListView lv = new ListView.builder(
        itemCount: _msg.tabPJ.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext ctxt, int index) {
          ImageProvider img =
              CachedNetworkImageProvider(_msg.tabPJ[index].urlSmall);
          bool flagBin =
              (_msg.tabPJ[index].typeFile != Constantes.TYPE_FILE_IMAGE);
          if (flagBin) {
            wImg = 50;
            hImg = 50;
            wCont = 70;
            hCont = 70;
          } else {
            wCont = 200;
            hCont = 200;
            wImg = 200;
            hImg = 200;
          }

          Padding p = new Padding(
              padding: EdgeInsets.only(right: 20),
              child: GestureDetector(
                  onTap: () {
                    if (flagBin) {
                      _openDocViewer(ctxt, _msg.tabPJ[index]);
                      //Util.alert(context, "fichier binaire !!!");
                    } else {
                      _openGallery(ctxt, index);
                    }
                  },
                  child: Container(
                      width: wCont,
                      height: hCont,
                      child: ProgressiveImage(
                        placeholder: img,
                        thumbnail: img,
                        image: img,
                        height: hImg,
                        width: wImg,
                        fit: BoxFit.contain,
                      ))));

          return p;
        });

    Container c = Container(
        //margin: const EdgeInsets.all(10.0),
        //padding: const EdgeInsets.all(5.0),
        width: (6 * MediaQuery.of(context).size.width) / 6,
        height: 200,
        decoration: BoxDecoration(
            // border: Border.all(),
            // borderRadius: BorderRadius.all(Radius.circular(10)),
            /* color: Theme
                .of(context)
                .accentColor //Colors.yellow,*/

            ),
        //       <--- BoxDecoration here

        child: _msg.tabPJ.length > 0 ? lv : Text("Aucune piece jointe"));

    return (c);
  }*/

  /*void _openDocViewer(BuildContext ctxt, EntUploadObject o) async {
    if (!kIsWeb)
      Navigator.of(ctxt).pushNamed('/ViewDocument', arguments: o);
    else {
    }
  }*/

  /*void _openGallery(BuildContext ctxt, int index) {
    EntContextGallery contextGallery = new EntContextGallery(index, _msg.tabPJ);

    Navigator.of(ctxt).pushNamed('/GalleryPhoto', arguments: contextGallery);
  }*/

  Widget _getAvatar(EntUser? user) {
    Widget w=Container();
    if(user != null)  w=Padding(padding: EdgeInsets.only(right: 25), child: Avatar(user));
    return(w);
  }

  Widget _getTitre1(EntUser? user) {
    TextStyle textstyleOwner =
        TextStyle(fontWeight: FontWeight.bold, fontSize: 15);

    TextStyle textstylePJ =
        TextStyle(fontWeight: FontWeight.bold, fontSize: 15, color: Colors.red);

    String heure =
        DateFormat('HH:mm').format(_msg.date); //.text.substring(0,indice);

    List<Widget> lh = [];
    lh.add(Text(heure));
    if (_msg.tabPJ.length > 0) {
      //lh.add(Text(" (" + _msg.tabPJ.length.toString() + " pj)",style: textstylePJ));
      //lh.add(Text(_msg.tabPJ.length.toString(),style: textstylePJ));
      lh.add(Padding(
          padding: EdgeInsets.only(left: 5),
          child: Icon(Icons.file_copy_outlined, color: Colors.red, size: 15)));
    }
    Row rHeure = Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //mainAxisSize: MainAxisSize.max,
        children: lh);

    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          _flagMe
              ? Text("Moi", style: textstyleOwner)
              : user != null
                  ? Text(user.getLibelle(), style: textstyleOwner)
                  : Text("?", style: textstyleOwner),
          rHeure
        ]);
  }

  Widget _getTitre2() {
    TextStyle textstyleTitre =
        TextStyle(fontSize: 15, fontStyle: FontStyle.italic);

    String titre = _msg.titre;
    if (titre.startsWith("<") == true) titre = "..";

    return Text(titre, style: textstyleTitre);
  }

  FutureBuilder<EntUser?> _getHeader() {
    return FutureBuilder<EntUser?>(
        future: UserService.getInstance().getUserById(_msg.userId),
        builder: (context, user) {
          return new Container(
              padding: EdgeInsets.only(bottom: 25),
              width: (5 * MediaQuery.of(context).size.width) / 6,
              /*decoration: BoxDecoration(
                  color: Color.fromRGBO(230, 0, 0, 0.5),
                ),*/
              child: new Row(

                  //mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    _getAvatar(user.data),
                    Expanded(
                        child: Container(
                            child: Column(
                                //crossAxisAlignment: CrossAxisAlignment.end,

                                children: <Widget>[
                          _getTitre1(user.data),
                          //_getTitre2()
                        ])))
                  ]));
        });
  }
}
