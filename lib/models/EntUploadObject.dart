import 'dart:typed_data';

class EntUploadObject {
  late String typeMime;
  late String typeMimeTB;
  late String typeFile;
  late String pathLocal;
  late String name;
  late Uint8List data;
  late Uint8List dataTB;
  //late UploadTask uploadTaskSmall;
  //late UploadTask uploadTaskBig;
  //late Future<bool> resultUpload;
  late String pathFB;
  late String pathThumbFB;
  late String urlSmall;
  late String urlBig;
  late bool flagSelect;

  EntUploadObject() {
    typeFile = "";
    typeMime = "";
    pathLocal = "";
    pathThumbFB = "";
    pathFB = "";
    name = "";
    urlSmall = "";
    urlBig = "";
    flagSelect = false;
  }
}
