import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';

import 'EntVersement.dart';

class EntUser {
  late String id;
  late String email;
  late bool flagMatch;
  late bool flagEntrainement;
  late bool flagPaiement;
  late bool flagNotifMessage;
  late bool flagNotifPresence;
  late String nom;
  late String prenom;
  late String tel;
  late String urlAvatar;
  late DateTime dateNaissance;
  late String lastTsCommentaire;
  late List<EntVersement> listeVersement;
  late int totalVersement;
  late String role;
  late String tokenFCMMob;
  late String tokenFCMWeb;

  EntUser() {
    id="";
    email="";
    nom="";
    prenom="";
    tel="";
    urlAvatar="";
    dateNaissance=DateTime.now();
    lastTsCommentaire="";
    role="";
    tokenFCMMob="";
    tokenFCMWeb="";
    listeVersement = [];
    totalVersement = 0;
    flagNotifMessage=true;
    flagNotifPresence=true;
    flagMatch=false;
    flagPaiement=false;
    flagEntrainement=false;
  }

  int addVersement(EntVersement v) {
    listeVersement.add(v);
    totalVersement += v.montant;

    return (totalVersement);
  }

  static EntUser fromRTDB(Map data, String id) {
    EntUser u = EntUser();
    u.id = id;
    int ts = data["dateNaissance"];
    if (ts != null)
      u.dateNaissance = new DateTime.fromMillisecondsSinceEpoch(ts);
    else
      u.dateNaissance = DateTime(1964, 7, 6);
    u.flagMatch = data["flagMatch"] ?? false;
    u.flagNotifMessage = data["flagNotifMessage"] ?? true;
    u.flagNotifPresence = data["flagNotifPresence"] ?? true;
    u.flagPaiement = data["flagPaiement"] ?? false;
    u.lastTsCommentaire = data["lastTsCommentaire"] ?? "";
    u.email = data["email"] ?? "";
    u.nom = data["nom"] ?? "";
    u.prenom = data["prenom"] ?? "";
    u.tel = data["tel"] ?? "";
    u.urlAvatar = data["urlAvatar"] ?? "";
    u.role = data["role"] ?? "joueur";
    u.tokenFCMWeb = data["tokenFCMWeb"] ?? "";
    u.tokenFCMMob = data["tokenFCMMob"] ?? "";


    return (u);
  }

  static EntUser fromFB(QueryDocumentSnapshot o) {
    EntUser u = EntUser();

    u.id=o.id;
    u.nom=o.get("nom");
    u.prenom=o.get("prenom");
    u.role=o.get("role");
    u.dateNaissance=Util.getDateTimeFromFB(o.get("dateNaissance"));
    u.flagEntrainement = o.get("flagEntrainement");
    u.flagMatch = o.get("flagMatch");
    u.flagNotifMessage = o.get("flagNotifMessage");
    u.flagNotifPresence = o.get("flagNotifPresence");
    u.flagPaiement = o.get("flagPaiement");
    //u.lastTsCommentaire = data["lastTsCommentaire"] ?? "";
    u.email = o.get("email");
    u.tel = o.get("tel");
    u.urlAvatar = o.get("urlAvatar");
    u.tokenFCMWeb = o.get("tokenFCMWeb");
    u.tokenFCMMob = o.get("tokenFCMMob");

    return (u);
  }

  static EntUser clone(dynamic data) {
    EntUser u = EntUser();
    u.id = data.id;
    u.dateNaissance = data.dateNaissance;
    u.urlAvatar = data.urlAvatar;
    u.email = data.email;
    u.flagMatch = data.flagMatch;
    u.flagEntrainement = data.flagEntrainement;
    u.flagNotifMessage = data.flagNotifMessage;
    u.flagNotifPresence = data.flagNotifPresence;
    u.flagPaiement = data.flagPaiement;
    u.lastTsCommentaire = data.lastTsCommentaire;
    u.nom = data.nom;
    u.prenom = data.prenom;
    u.tel = data.tel;
    u.role = data.role;
    u.listeVersement = [];
    EntVersement elem;
    for (EntVersement v in data.listeVersement) {
      elem = new EntVersement();
      elem.id = v.id;
      elem.montant = v.montant;
      elem.date = v.date;
      u.listeVersement.add(elem);
    }
    u.totalVersement = data.totalVersement;
    u.tokenFCMMob = data.tokenFCMMob;
    u.tokenFCMWeb = data.tokenFCMWeb;
    return (u);
  }

  Object toFB() {
    Object o = {
      "urlAvatar": urlAvatar,
      "dateNaissance": dateNaissance,
      "email": email,
      "flagMatch": flagMatch,
      "flagEntrainement": flagEntrainement,
      "flagNotifMessage": flagNotifMessage,
      "flagNotifPresence": flagNotifPresence,
      "flagPaiement": flagPaiement,
      "lastTsCommentaire": lastTsCommentaire,
      "nom": nom,
      "prenom": prenom,
      "tel": tel,
      "role": role,
      "tokenFCMMob": tokenFCMMob,
      "tokenFCMWeb": tokenFCMWeb
    };
    return (o);
  }

  String getLibelle() {
    return (prenom + " " + nom);
  }

  String getPrenomLimite() {
    if ((prenom.length + nom.length) > 17) return ("");

    return (prenom);
  }

  String getInitiales() {
    return (prenom.substring(0, 1) + nom.substring(0, 1));
  }

  String getLibelleCourt() {
    return (prenom.substring(0, 1) + ". " + nom);
  }

  String getPrenom() {
    String ret = "";
    List<String> tmp = this.prenom.split(" ");
    for (String s in tmp) {
      List<String> tmp2 = s.split(".");
      for (String s2 in tmp2) {
        ret = ret + s2.substring(0, 1).toUpperCase() + s2.substring(1) + " ";
      }
    }

    return (ret);
  }
}
