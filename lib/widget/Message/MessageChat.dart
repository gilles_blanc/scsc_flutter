import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:scsc_veteran/models/EntMessage.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'ElemMessage.dart';

class MessageChat extends StatefulWidget {
  MessageChat(dynamic l, EntUser u) {
    _listeMessage = l;
    _currrentUser = u;
  }

  late List<EntMessage> _listeMessage;
  late EntUser _currrentUser;

  @override
  _MessageChatPageState createState() => _MessageChatPageState();
}

class _MessageChatPageState extends State<MessageChat> {
  late ItemScrollController itemScrollController;
  late ItemPositionsListener itemPositionListener;
  //ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    initializeDateFormatting("fr_FR", null);
    //_scrollController = ScrollController();
    itemScrollController = ItemScrollController();
    itemPositionListener = ItemPositionsListener.create();
  }

  notifFromMessageFile(String action) {
    /* setState(() {
        _listImage = new List<File>();
      });*/
  }

  @override
  void dispose() {
    //_scrollController.dispose();

    super.dispose();
  }

  _jumpToFirstNewMessage() {
    if (widget._listeMessage.length > 0) {
      int indexNew = widget._listeMessage.length - 1;
      EntMessage elem;
      for (int i = widget._listeMessage.length - 1; i >= 0; i--) {
        elem = widget._listeMessage[i];
        if (elem.flagNew == true) {
          indexNew = i;
        } else {
          break;
        }
      }
      //print("************** indexNew="+indexNew.toString());
      itemScrollController.jumpTo(index: indexNew);
    }
  }

  @override
  Widget build(BuildContext context) {
    String lastDate = "";
    String strDate = "";

    Timer(Duration(milliseconds: 10), () => _jumpToFirstNewMessage());

    return Container(
        decoration: BoxDecoration(),
        //alignment: Alignment.topLeft,
        margin: const EdgeInsets.only(bottom: 10),
        child: ScrollablePositionedList.builder(
            itemScrollController: itemScrollController,

            //itemPositionListener: itemPositionListener,
            //controller: _scrollController,
            //reverse: true,
            itemCount: widget._listeMessage.length,
            itemBuilder: (BuildContext ctxt, int index) {
              bool flagMe =
                  widget._currrentUser.id == widget._listeMessage[index].userId;
              strDate = DateFormat.MMMMEEEEd("fr_FR")
                  .format(widget._listeMessage[index].date);
              Column c;
              if (strDate != lastDate) {
                c = new Column(
                  children: <Widget>[
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.primary,
                                borderRadius: BorderRadius.circular(20),
                                //border: Border.all()
                              ),
                              margin: EdgeInsets.only(
                                  left: 10, right: 10, top: 10, bottom: 10),
                              padding: EdgeInsets.only(
                                  left: 10, right: 10, top: 10, bottom: 10),
                              child: Text(strDate,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white)))
                        ]),
                    Row(
                      mainAxisAlignment: flagMe
                          ? MainAxisAlignment.end
                          : MainAxisAlignment.start,
                      children: <Widget>[
                        //Text("dd")
                        ElemMessage(widget._listeMessage[index], flagMe)
                      ],
                    )
                  ],
                );

                lastDate = strDate;
              } else {
                c = new Column(children: <Widget>[
                  Row(
                    mainAxisAlignment: flagMe
                        ? MainAxisAlignment.end
                        : MainAxisAlignment.start,
                    children: <Widget>[
                      ElemMessage(widget._listeMessage[index], flagMe)
                    ],
                  )
                ]);
              }
              return c;
            }));
  }
}
