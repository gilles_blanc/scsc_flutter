import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/widget/Avatar/Avatar.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class Parametre extends StatefulWidget {
  @override
  _ParametrePageState createState() => _ParametrePageState();
}

class _ParametrePageState extends State<Parametre> {
  final ctrlNom = TextEditingController();
  final ctrlPrenom = TextEditingController();
  final ctrlDateNaissance = TextEditingController();
  final ctrlTel = TextEditingController();
  bool flagWait = false;

  EntUser user=EntUser();

  @override
  void initState() {
    super.initState();
    initializeDateFormatting("fr_FR", null).then((o) {
      user = EntUser.clone(ModalRoute.of(context)!.settings.arguments);
      setState(() {
        ctrlNom.text = user.nom;
        ctrlPrenom.text = user.prenom;
        ctrlTel.text = formatTel(user.tel);
        ctrlDateNaissance.text = DateFormat.yMd("fr_FR").format(user.dateNaissance);
      });
    });

  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });
  }

  void _endWait() {
    setState(() {
      flagWait = false;
    });
  }

  void gestAdmin() async{
    String ret=await showDialog(
      context: context,
      builder: (context) => Util.choixTypeAdmin(context),
    );

    switch(ret){
      case "equipe":
        Navigator.of(context).pushNamed('/AdminEquipe');
        break;
      case "match":
        Navigator.of(context).pushNamed('/AdminMatch');
        break;
      case "entrainement":
        Navigator.of(context).pushNamed('/AdminEntrainement');
        break;
    }

  }

    void majUserInfo() {
    user.nom = ctrlNom.text;
    user.prenom = ctrlPrenom.text;
    user.tel = ctrlTel.text;
  /*  _startWait();
    UserService.getInstance().modifUser(user).then((ok) {
      _endWait();
      Util.alert(context, "Modification effectuée");
    }).catchError((onError) {
      _endWait();
      Util.error(context, "Erreur lors de la modification");
    });*/
  }

  void majSecu(int code) {
    switch (code) {
      case 1:
        //print("user mail" + user.email);
        Navigator.of(context).pushNamed('/ModifMail', arguments: user);
        break;
      case 2:
        Navigator.of(context).pushNamed('/ModifPass', arguments: user);
        break;
    }
  }

  void modifDate(DateTime initDate) async {
    // Imagine that this function is
    // more complex and slow.
    DateTime? d = await showDatePicker(
      context: context,
      initialDate: initDate,
      firstDate: DateTime(1940),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light(),
          child: child!,
        );
      },
    );
    if (d != null) {
      ctrlDateNaissance.text = DateFormat.yMd("fr_FR").format(d);
      user.dateNaissance = d;
    }
  }

  String formatTel(String nr) {
    String nrReduit = "";

    String ret = "";
    if ((nr != null) && (nr != "")) {
      for (int i = 0; i < nr.length; i++) {
        if (nr.substring(i, i + 1).compareTo(" ") != 0) {
          nrReduit = nrReduit + nr.substring(i, i + 1);
        }
      }

      for (int i = 0; i < nrReduit.length; i++) {
        if (ret != "") {
          if ((i % 2) == 0) {
            ret = ret + " ";
          }
        }
        ret = ret + nrReduit.substring(i, i + 1);
      }
    }

    return (ret);
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60.0),
          child: AppBar(
            title: Text("Paramètres"),
            actions: <Widget>[
              user.role == Constantes.ROLE_ADMIN ?  IconButton(
                    onPressed: gestAdmin,
                icon: Icon(Icons.menu),

              ) : Container(),

            ],
          ),
        ),
        body:

        ListView(
          //mainAxisAlignment: MainAxisAlignment.end,
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              color: Colors.white,
              height: 10,
              child: flagWait
                  ? LinearProgressIndicator(
                      backgroundColor: Colors.white,
                      valueColor: new AlwaysStoppedAnimation<Color>(
                          Theme.of(context).colorScheme.primary))
                  : Container(),
            ),
            Container(
                margin: const EdgeInsets.all(1.0),
                padding: const EdgeInsets.only(bottom: 10.0),
                height: MediaQuery.of(context).size.height-100,
                width: MediaQuery.of(context).size.width,
//color:Colors.red,
                child:info(user)

            ),


         ]
    )
    );

  }

  void _changeNotifMessage(bool? value) =>
      setState(() => user.flagNotifMessage = value!);
  void _changeNotifPresence(bool? value) =>
      setState(() => user.flagNotifPresence = value!);

  Widget info(EntUser u) {
    return (ListView(
      //mainAxisAlignment: MainAxisAlignment.end,
      // crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        _inputText("Nom", ctrlNom, TextInputType.text),
        _inputText("Prenom", ctrlPrenom, TextInputType.text),
        _inputTel("Téléphone", ctrlTel),
        _inputDate("Date de naissance", ctrlDateNaissance),

        CheckboxListTile(
            checkColor: Colors.white,
            activeColor: Theme.of(context).colorScheme.primary,
            value: user.flagNotifMessage,
            onChanged: _changeNotifMessage,
            title: Text('Notifications messages',style: TextStyle(
                fontSize: 15))),
        CheckboxListTile(
            checkColor: Colors.white,
            activeColor: Theme.of(context).colorScheme.primary,
            value: user.flagNotifPresence,
            onChanged: _changeNotifPresence,
            title: Text('Notifications presences',style: TextStyle(
          fontSize: 15,
        ))),
        Padding(
            padding: EdgeInsets.only(right: 10,left:10.0),
            child: ElevatedButton(
              child: Text("Valider"),
              onPressed: majUserInfo,

            )),
        /*user.role == Constantes.ROLE_ADMIN ? Padding(
            padding: EdgeInsets.only(right: 40,left:40.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Colors.grey),
              child: Text("Match ..."),
              onPressed: gestMatch,

            )) : Container(),
        user.role == Constantes.ROLE_ADMIN ? Padding(
            padding: EdgeInsets.only(right: 40,left:40.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Colors.grey),
              child: Text("Equipes ..."),
              onPressed: gestEquipe,

            )) : Container()
*/
      ],
    ));
  }

  /*Widget secuInfo(EntUser u) {
    String mail = u.email;
    return (Container(
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            //color: Colors.red,
            ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _title("Mon login"),
            Column(
                //mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  _inputSecu(u.email, 1), //"Adresse mail",1),
                  _inputSecu("Mot de passe", 2)
                ]),
          ],
        )));
  }*/

  Widget _inputText(
      String hint, TextEditingController controller, TextInputType kbType) {
    return Container(
        margin: const EdgeInsets.all(4.0),
        padding: EdgeInsets.only(left: 4, right: 4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            /*SizedBox(
            width: 80,
            child :Text(hint + ":"))
          ,*/
            new Flexible(
                child: TextField(
              decoration: new InputDecoration(
                  //hintText: hint,
                  labelText: hint),
              keyboardType: kbType,
              controller: controller,
              style: TextStyle(
                fontSize: 15,
              ),
            ))
          ],
        ));
  }

  Widget _inputDate(String hint, TextEditingController controller) {
    MaskTextInputFormatter dateFormatter = new MaskTextInputFormatter(mask: '##/##/####', filter: { "#": RegExp(r'[0-9]') });

    return Container(
        margin: const EdgeInsets.all(4.0),
        padding: EdgeInsets.only(left: 4, right: 4),
        width: (MediaQuery.of(context).size.width * 3) / 5,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            new Flexible(
                child: TextField(
                  //maxLength: 10,
                  keyboardType: TextInputType.number,
                  inputFormatters: [dateFormatter],
                  decoration: new InputDecoration(
                    //hintText: hint,
                      labelText: hint),
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  controller: controller,

                  /*inputDecoration: new InputDecoration(
                  counterText: "",

                  //hintText: hint,
                  labelText: hint),*/
            )),
            IconButton(
                icon: Icon(Icons.calendar_today),
                tooltip: 'Date de naissance',
                onPressed: () {
                  modifDate(user.dateNaissance);
                }),
          ],
        ));
  }

  Widget _inputTel(String hint, TextEditingController controller) {
    MaskTextInputFormatter telFormatter = new MaskTextInputFormatter(mask: '## ## ## ## ##', filter: { "#": RegExp(r'[0-9]') });

    return Container(
        margin: const EdgeInsets.all(4.0),
        padding: EdgeInsets.only(left: 4, right: 4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            new Flexible(
                child: TextField(
                  inputFormatters: [telFormatter],
                  keyboardType: TextInputType.number,
                  controller: controller,
                  decoration: new InputDecoration(
                    //hintText: hint,
                      labelText: hint),
                    style: TextStyle(
                      fontSize: 15,
                    )


                  /*inputDecoration: new InputDecoration(
                  counterText: "",
                  //hintText: hint,
                  labelText: hint),*/
            )),
          ],
        ));
  }

  /*Widget _inputSecu(String hint, int code) {
    return Container(
        margin: EdgeInsets.only(right: 10),
        padding: EdgeInsets.only(left: 4, right: 4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Text(hint),
            RaisedButton(
              child: Text("Modifier"),
              onPressed: () {
                majSecu(code);
              },
              color: Theme.of(context).colorScheme.primary,
              //textColor: Colors.yellow,
              //padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              splashColor: Colors.grey,
            )
          ],
        ));
  }*/

  Widget _title(String titre) {
    return Container(
        //margin: const EdgeInsets.all(10.0),
        //padding: EdgeInsets.only(left: 20,top:5,bottom:5),
        child: Text(titre,
            style: TextStyle(
                fontStyle: FontStyle.italic,
                color: Theme.of(context).colorScheme.primary)));
  }
}
