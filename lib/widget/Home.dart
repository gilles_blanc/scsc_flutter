import 'dart:async';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntAnniversaire.dart';
import 'package:scsc_veteran/models/EntEntrainement.dart';
import 'package:scsc_veteran/models/EntMatch.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/AuthService.dart';
import 'package:scsc_veteran/services/EntrainementService.dart';
import 'package:scsc_veteran/services/MatchService.dart';
import 'package:scsc_veteran/services/NotificationService.dart';
import 'package:scsc_veteran/services/UserService.dart';
import 'Activite/ElemActivite.dart';
import 'Avatar/Avatar.dart';
import 'package:intl/intl.dart';

class Home extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<Home> {
  //HandlerNotification notif;

  // TO DO
  /*EntActivite currentEntrainement;
  List<EntActivite> listActivite = new List<EntActivite>();
  EntEquipe equipe;
  EntMatch nextMatch;

*/
  late StreamSubscription<EntUser> _subCurrentUser;
  late StreamSubscription<List<EntMatch>> _subListMatch;
  late StreamSubscription<EntEntrainement?> _subEntrainement;
  late StreamSubscription<List<EntAnniversaire>> _subAnniversaire;
  EntUser? _currentUser=null;
  EntMatch? _nextMatch=null;
  EntEntrainement? _nextEntrainement=null;
  List<EntAnniversaire> listNextAnniv = [];

  int _nbMatch=-1;

  final GlobalKey key = GlobalKey();
  int nbNewMessage = 0;
  int nbPresentMatch = 0;
  bool flagPresenceMatch = false;
  int maxVersement = 0;
  late FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);

    _initTokenFCM();
    initializeDateFormatting("fr_FR", null).then((onValue) {
      _initNotificationHandler();
      _initCurrentUser();
      _initMatch();
      _initEntrainement();
      _initAnniversaire();
    });
  }

  Widget _getFixedRow(Widget w,int len){
    Widget ret=Container(
        width: MediaQuery.of(context).size.width-len,
        child : w
    );

    return(ret);
  }

  _showToast(String title,String body,String idUser) {


    UserService.getInstance().getUserById(idUser).then((u){
      Widget w=Container();
      if(u != null) w=Avatar(u);

      Widget mes=Padding(
          padding:EdgeInsets.only(left:10,right:10),
          child:Column(
            children: [
              _getFixedRow(Text(title,style:TextStyle(fontSize: 10,color:Colors.black)),150) ,
              _getFixedRow(Text(body,style:TextStyle(fontSize: 15,color:Theme.of(context).colorScheme.primary)),150)
            ],
          )
      );



      Widget toast = Container(
          width: MediaQuery.of(context).size.width - 10,
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25.0),
            border: Border.all(
              color: Theme.of(context).colorScheme.primary,
              width: 2,
            ),
            color: Colors.white,
          ),
          child: Row(
            children: [
              w,mes
            ],
          )


      );

      fToast.showToast(
        child: toast,
        gravity: ToastGravity.TOP,
        toastDuration: Duration(seconds: 5),
      );
    });

  }

  void _initNotificationHandler(){



    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      print("AAAAAAAAAAAAAa");
      _showToast(event.notification!.title!,event.notification!.body!,event.data["idUser"]);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print("BBBBBBBBBBBbbbb");
      switch(int.parse(message.data["type"])){
        case Constantes.TYPE_NOTIFICATION_PARTICIPATION_ENTRAINEMENT:
        case Constantes.TYPE_NOTIFICATION_PARTICIPATION_MATCH:
          break;
        case Constantes.TYPE_NOTIFICATION_MESSAGE:
          goMessage();
          break;
      }

    });

  }
  @override
  void dispose() {
    Util.freeSub(_subCurrentUser);
    Util.freeSub(_subListMatch);
    Util.freeSub(_subEntrainement);
    Util.freeSub(_subAnniversaire);

    super.dispose();
  }

  void _initAnniversaire() {
    _subAnniversaire = UserService.getInstance().getObsAnniversaires().listen((l) {
      setState(() {
        listNextAnniv = l;
      });
    });
  }


  // TO Dp
  /*refreshTokenFCM(String token, EntUser u) {
    if ((token != null) && (token != "")) {
      if (!kIsWeb) {
        if (token != u.tokenFCMMob) {
          u.tokenFCMMob = token;
          UserService.getInstance().modifUser(u);
        }
      } else {
        if (token != u.tokenFCMWeb) {
          u.tokenFCMWeb = token;
          UserService.getInstance().modifUser(u);
        }
      }
    }
  }*/

  void _initTokenFCM(){
    //print("_initTokenFCM");
    NotificationService.getInstance().getUserTokenFCM().then((token){
      print("token récupéré :" + token!);
      UserService.getInstance().majTokenFCM(token);
    });

  }


  void _initCurrentUser() {
    _subCurrentUser=UserService.getInstance().getObsCurrentUser().listen((u) {
      if(u == null) print("USER CHANGE null !!");
      else print("USER CHANGE");
      setState(() {
        _currentUser = u;
      });
    });


      //if (!kIsWeb) {
        //TO DO
        /* HandlerNotificationMob notif = new HandlerNotificationMob();
        notif.init(key).then((token) {
          refreshTokenFCM(token, u);
        });*/
    //  }
     // else {
        /*if (browser.isChrome) {
            HandlerNotificationWeb notif = HandlerNotificationWeb.getInstance();
            notif.requestPermission().then((_) {
              notif.getToken().then((token) {
                refreshTokenFCM(token, u);
              });
            });

            notif.stream.listen((event) {
              print('New Message: ${event}');
            });
          }*/
      //}

          // TO DO
          /*_initNextMatch();
      _initLastMessage();
      _initActivite();
      _initVersement();
      _initNextAnniv();
*/

  }

  void _initMatch() {
    _subListMatch=MatchService.getInstance().getObsMatchs().listen((lm) {
        List<EntMatch> filterList = lm.where((m) {
          return(m.date.isAfter(DateTime.now()));
        }).toList();

        setState(() {
          _nbMatch=filterList.length;
          if(filterList.isNotEmpty) {
            _nextMatch = filterList[0];
            _nextMatch!.typeActivite=Constantes.TYPE_ACTIVITE_NEXT_MATCH;
          }
          else{
            _nextMatch=null;
          }
        });
    });
  }

  void _initEntrainement() {
    _subEntrainement=EntrainementService.getInstance().getObsNextEntrainement().listen((e) {

      setState(() {
        _nextEntrainement=e;
      });
    });

  }

// TO Dp
/*void _verifVersion() {
    ParamService.getInstance().getParam().then((p) {
      if ((p.isLastVersion() == false) && (!kIsWeb)) {
        Util.confirme(
                context,
                "Attention, votre version actuelle (" +
                    p.getStrVersion() +
                    ") nécessite une mise à jour, voulez-vous la télécharger ?")
            .then((ret) {
          if (ret == true) {
            /*StoreRedirect.redirect(
                androidAppId: "fr.scsc_veteran", iOSAppId: "1111111111");*/
          }
        });
      }
    });
  }*/

// TO Dp
/*void _initNextAnniv() {
    UserService.getInstance().getProchainAnniversaire(30).then((l) {
      setState(() {
        listNextAnniv = l;
      });
    });
  }*/

// TO Dp
/*void _initVersement() {
    setState(() {
      maxVersement = UserService.getInstance().maxVersement;
    });
  }*/

// TO Dp
/*void _initLastMessage() {
    MessageService.getInstance().getStreamMessage().listen((listMessage) {
      int nbNew = 0;
      for (int i = listMessage.length - 1; i >= 0; i--) {
        if (listMessage[i].flagNew == true)
          nbNew++;
        else
          break;
      }

      setState(() {
        nbNewMessage = nbNew;
      });
    });
  }*/

// TO Dp
/**/

// TO Dp
/*void _initActivite() {
    ActiviteService.getInstance().getAllActivite().then((l) {
      setState(() {
        listActivite.clear();
        l.forEach((a) {
          if (a.id == Constantes.ACTIVITE_ENTRAINEMENT) {
            currentEntrainement = a;
          } else {
            listActivite.add(a);
          }
        });
      });
    });
  }*/


void goAlbum(){
  /*EntNotification notif = EntNotification(Constantes.TYPE_NOTIFICATION_PARTICIPATION_MATCH,
      _currentUser!,[_currentUser!],4);
  NotificationService.getInstance().sendNotificationFCM(notif);
  */
}
// TO Dp
void _nav(String route) {
    UserService.getInstance().getCurrentUser().then((u) {
      Navigator.of(context).pushNamed('$route', arguments: u).then((ret) {
        //_initCurrentUser();
        // TO DO
        /*_initLastMessage();
        _initNextMatch();
        _initActivite();*/
      });
    });
  }

  Future<bool> confirmeLogoff() async {
    bool ret = await Util.confirme(
        context, "Voulez vous vraiment vous déconnecter ?", "Déconnexion");

    if (ret == true) {
      AuthService.getInstance().logout();
   }

    return (false); // Empêche une double navigation (Back)
  }

  goActivite() {
    _nav("/ActiviteEdit");
  }

  goMessage() {
    _nav("/Message");
  }

  goAllMatch() {
    _nav("/AllMatch");
  }
// TO Dp
/*

  goMatch() {
    //Util.alert(context,"mathc");
    //print("go match avant");
    _nav("/Match");
    //print("go match apres");
  }
  //backgroundImage: new MemoryImage(user.getDecodeAvatar())

  goEntrainement() {
    Navigator.of(context)
        .pushNamed('/Activite', arguments: currentEntrainement)
        .then((ret) {
      _initActivite();
    });
  }
*/
  @override
  Widget build(BuildContext context) {
    double iconSize = 30;

    if(_currentUser == null) return(Container());
    Color colIcon = Colors.white;
    Color colIconParam = Colors.black;

    Color colNav = Theme.of(context).colorScheme.secondary;

    return new WillPopScope(
        onWillPop: confirmeLogoff, //() async => false,
        child: new Scaffold(
            key: key,
            bottomNavigationBar: Container(
                //margin: const EdgeInsets.all(10.0),
                //padding: const EdgeInsets.all(5.0),
                //width: w,
                decoration: BoxDecoration(
                    //border: Border.all(),
                    //borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: colNav //Colors.yellow,
                ), //       <--- BoxDecoration here
                child:
                  Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Util.getIconBadge(
                        nbNewMessage,
                        Icon(Icons.question_answer),
                        colIcon,
                        Theme.of(context).colorScheme.secondary,
                        goMessage,
                        iconSize,
                        false),
                    Util.getIconBadge(
                        _nbMatch,
                        Icon(Icons.calendar_today),
                        colIcon,
                        Theme.of(context).colorScheme.secondary,
                        goAllMatch,
                        iconSize,
                        false),
/*
                  Util.getIconBadge(
                        0,
                        Icon(Icons.beach_access),
                        colIcon,
                        Theme.of(context).colorScheme.secondary,
                        goActivite,
                        iconSize,
                        false),*/
                    IconButton(
                        color: colIcon,
                        icon: Icon(Icons.group),
                        iconSize: iconSize,
                        tooltip: 'Joueur',
                        onPressed: () {
                          _nav("/Joueur");
                        }),
                  /*  IconButton(
                        color: colIcon,
                        icon: Icon(Icons.photo_library),
                        iconSize: iconSize,
                        tooltip: 'Albums',
                        onPressed: () {
                          goAlbum();
                          //_nav("/Album");
                        }),*/
                    IconButton(
                        color: colIconParam,
                        icon: Icon(Icons.settings),
                        iconSize: iconSize,
                        tooltip: 'Paramètres',
                        onPressed: () {
                          _nav("/Parametre");
                        })
                  ],
                )),
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(60.0),
              child: AppBar(
                title: Text("SCSC Vétéran"),
                leading: Container(
                  padding: EdgeInsets.only(left: 5, top: 5),
                  child: Avatar(_currentUser!, 1),
                ),
                actions: <Widget>[
                  IconButton(
                      icon: Icon(Icons.exit_to_app),
                      tooltip: 'Déconnexion',
                      onPressed: confirmeLogoff)
                ],
              ),
            ),
            body: Container(
                height: MediaQuery.of(context).size.height,
                child: ListView(
                    //mainAxisAlignment: MainAxisAlignment.start,
                    //crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      getWelcome(),
                      // TO DO
                      getNextMatch(),
                      getNextEntrainement(),
                      getNextAnniversaire(),
                      /*getNextEntrainement(),
                      getNextActivite(),
                      getNextAnniversaire(),*/

                    ]))));
  }


Widget getWelcome() {
  double sizeWelcome = 25;
  Row r = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text("Bonjour ",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: sizeWelcome,
                    color: Colors.white)),
            _currentUser != null
                ? Text(_currentUser!.getPrenom(),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: sizeWelcome,
                        color: Colors.white))
                : Text(""),
          ],
        ),
        Util.getThumb(_currentUser, context, Constantes.ACTION_CAGNOTTE_VIEW)
      ],
    );

    Container cont = Container(
        margin: const EdgeInsets.all(10.0),
        padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
            //border: Border.all(),
            color: Theme.of(context).colorScheme.secondary,
            borderRadius: BorderRadius.all(
                Radius.circular(10))), //       <--- BoxDecoration here

        child: r);
    return (cont);
  }

// TO DO
Widget getNextMatch() {
    Widget w;

    if(_nbMatch >= 0) {
      if (_nextMatch != null) {
        w = ElemActivite(_nextMatch!,_currentUser);
      }
      else {

        w = Container(
            margin: EdgeInsets.only(top: 5, bottom: 5, right: 10, left: 10),
            padding: EdgeInsets.only(top: 10, bottom: 10),
            //width: MediaQuery.of(context).size.width - 100,
            decoration: BoxDecoration(
              border: Border.all(
                color: Theme.of(context).colorScheme.primary,
                width: 2,
              ),
              borderRadius: BorderRadius.circular(12),
            ),
            child:Center(
                child: Text("Aucun match programmé." ,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.primary))
            ));


      }
    }
    else w=Container();

    return(w);
  }

  Widget getNextEntrainement() {
    Widget w;

     if (_nextEntrainement != null) {
        w = ElemActivite(_nextEntrainement!,_currentUser);
      }
      else {
        w = Text("aucun entrainement programmé");
      }

    return(w);
  }

  Widget getNextAnniversaire() {
    double sizeText = 15;
    if (listNextAnniv.length == 0) {
      return (Text(""));
    }
    ListView lv = new ListView.builder(
        itemCount: listNextAnniv.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext ctxt, int index) {
          String strDate = DateFormat.MMMMEEEEd("fr_FR")
              .format(listNextAnniv[index].dateAnniversaire);

          return new Padding(
              padding: EdgeInsets.only(right: 20),
              child: Row(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(listNextAnniv[index].user.getLibelle(),
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: sizeText)),
                  Text(" :" + strDate, style: TextStyle(fontSize: sizeText))
                ],
              ));
        });
    /*Container maintCont=Container(
        height: 80.0,
        child:lv
    );*/

    String titre = "Anniversaire";
    if (listNextAnniv.length > 1)
      titre = titre + "s (" + listNextAnniv.length.toString() + ")";

    Container cont = Container(
        //margin: const EdgeInsets.all(10.0),
        //padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
            //border: Border.all(),
            //color:Theme.of(context).colorScheme.secondary,
            //borderRadius: BorderRadius.all(Radius.circular(10))

            ),
        height: 30.0,
        width: MediaQuery.of(context).size.width / 2,
        child: Row(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: Text(titre,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: sizeText,
                        color: Theme.of(context).colorScheme.primary))),
            Flexible(child: lv)
          ],
        ));

    return (cont);
  }

  Widget getTitre(String titre, DateTime d) {
    double sizeTitre = 20;
    Color colTitre = Theme.of(context).colorScheme.primary;
    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(titre,
            style: TextStyle(
                color: colTitre,
                fontWeight: FontWeight.bold,
                fontSize: sizeTitre)),
        d != null ? Text(" (" + Util.getElapse(d) + ")") : Text("")
      ],
    );
    return (r);
  }

// TO DO
/*Widget getNextActivite() {
    String titre = "Activité";
    if (listActivite.length > 1) {
      titre = titre + "s/Sondages (" + listActivite.length.toString() + ")";
    } else {
      titre = titre + "/Sondage";
    }

    /*if(listActivite.length > 1) {
      titre = titre + "(" + listActivite.length.toString() + ")";
    }*/

    List<Widget> lw = new List<Widget>();
    ActiviteBloc a;
    listActivite.forEach((element) {
      a = new ActiviteBloc(element, Icon(Icons.beach_access), 15.0, 40.0);
      lw.add(a);
    });

    return (getBloc(titre, lw, null, (MediaQuery.of(context).size.height / 5)));

    /*if(listActivite.length == 0){
      return Text("");
    }


    List<Widget> l=new List<Widget>();
    listActivite.forEach((a){
      l.add(ActiviteMain(a,Icon(Icons.accessibility)));
    });

    return(getBloc(titre,l));
*/
  }*/


}
