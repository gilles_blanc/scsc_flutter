import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:scsc_veteran/common/Constantes.dart';

class EntPresenceActivite {
  late String id;
  late String idActivite;
  late String idUser;
  late int codePresence;

  EntPresenceActivite() {
    this.id="";
    this.idActivite="";
    this.idUser="";
    codePresence=Constantes.CODE_PRESENCE_ACTIVITE_UNKNOWN;
  }

  static EntPresenceActivite fromFB(QueryDocumentSnapshot o) {
    EntPresenceActivite a = EntPresenceActivite();
    a.id = o.id;
    a.idUser = o.get("idUser");
    a.idActivite = o.get("idActivite");
    a.codePresence = o.get("codePresence");
    return (a);
  }

  Object toFB() {

    Object o = {
      "idUser": idUser,
      "idActivite": idActivite,
      "codePresence": codePresence

    };
    return (o);
  }
}
