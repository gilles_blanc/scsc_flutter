import 'package:flutter/material.dart';
import 'Constantes.dart';

class CustomDlg{
  late String mes;
  late String header;
  late Icon icon;
  late List<TextButton> listButton;
  late BuildContext context;

  CustomDlg(BuildContext context,[String mes="" , String header="",Icon icon=const Icon(Icons.info,size: Constantes.SIZE_ICON_DLG),List<TextButton> listButton=const []]){
    this.context=context;
    this.mes=mes;
    this.header=header;
    this.icon =icon;
    this.listButton = listButton;
  }
}