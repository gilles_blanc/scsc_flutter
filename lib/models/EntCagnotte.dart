import 'EntDepense.dart';
import 'EntVersement.dart';

class EntCagnotte{
  EntVersement? versement;
  EntDepense? depense;

  EntCagnotte clone(){
    EntCagnotte c = EntCagnotte();
    if(depense != null) c.depense=depense!.clone();
    if(versement != null) c.versement=versement!.clone();

    return(c);

  }
}