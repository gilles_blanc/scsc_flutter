import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/models/EntVersement.dart';

class VersementService {
  late BehaviorSubject<List<EntVersement>> _listSubject;
  late BehaviorSubject<int> _totalSubject;

  static VersementService? _singleton=null;

  static void razSingleton() {
    _singleton = null;
  }

  static VersementService getInstance() {
    if (_singleton == null) {
      _singleton = VersementService._();
    }
    return (_singleton!);
  }

  Stream<List<EntVersement>> getObsVersements() {
    return (_listSubject.stream);
  }

  Stream<int> getObsTotal() {
    return (_totalSubject.stream);
  }

  VersementService._() {
    _initListenerCollectionVersements();
  }

  void _initListenerCollectionVersements() {
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_VERSEMENTS);

    _listSubject = BehaviorSubject<List<EntVersement>>();
    _totalSubject = BehaviorSubject<int>();

    EntVersement v;
    List<EntVersement> listVersement;
    int totalVersement;

    refCollection.snapshots().listen((l) async {



      listVersement = [];
      totalVersement=0;
      l.docs.forEach((QueryDocumentSnapshot<Object?> o) async {

        if (o.exists) {
          v = EntVersement.fromFB(o);
          totalVersement+=v.montant;
          listVersement.add(v);
        }
      });
      _listSubject.add(listVersement);
      _totalSubject.add(totalVersement);
    });
  }

  Future<void> deleteVersement(EntVersement v) async {
    Future<void> f;
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_VERSEMENTS);
    f=refCollection.doc(v.id).delete();
    return(f);
  }

  Future<void> modifVersement(EntVersement v) async {
    Future<void> f;
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_VERSEMENTS);
    if (v.id != "") {
      f=refCollection.doc(v.id).set(v.toFB());
    } else {
      f=refCollection.add(v.toFB());
    }
    return(f);
  }
}
