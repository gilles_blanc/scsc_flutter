import 'dart:async';
import 'package:badges/badges.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'package:flutter/material.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntActivite.dart';
import 'package:scsc_veteran/models/EntEquipe.dart';
import 'package:scsc_veteran/models/EntMatch.dart';
import 'package:scsc_veteran/models/EntPresenceActivite.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/models/ParamActivite.dart';
import 'package:scsc_veteran/services/EquipeService.dart';
import 'package:scsc_veteran/services/PresenceActiviteService.dart';
import 'package:scsc_veteran/services/UserService.dart';
import 'package:scsc_veteran/widget/Activite/JoueurActivite.dart';

class DetailActivite extends StatefulWidget {

  DetailActivite() {

  }

  @override
  _DetailActivitePageState createState() => _DetailActivitePageState();
}

class _DetailActivitePageState extends State<DetailActivite> with TickerProviderStateMixin {
  StreamSubscription<List<EntEquipe>>? _subListEquipe=null;
  StreamSubscription<List<EntPresenceActivite>>? _subListPresenceActivite=null;
  ParamActivite? paramActivite=null;
  //int mode=1;
  int myMode=-1;
  List<EntUser> listeUser=[];
  List<EntPresenceActivite> listePresent=[];
  List<EntPresenceActivite> listeAbsent=[];
  List<EntPresenceActivite> listeNoResponse=[];

  EntEquipe? equipe=null;
  List<EntPresenceActivite> _listePresence=[];
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);

    _tabController!.addListener(() {
      print("tab change");
      setState(() {

      });
    });
    Future.delayed(
      const Duration(milliseconds: 50),
          () {
        _initParam();
      },
    );
  }

  void _initParam(){
    paramActivite = ModalRoute.of(context)!.settings.arguments as ParamActivite;
    if(paramActivite!.activite.isMatch() == true) _initMatch(paramActivite!.activite as EntMatch);
    _initAllUser();

  }

  @override
  void dispose() {
    Util.freeSub(_subListEquipe);
    Util.freeSub(_subListPresenceActivite);

    super.dispose();
  }

  _initMatch(EntMatch m) {
    _subListEquipe=EquipeService.getInstance().getObsEquipes().listen((le) {
      setState(() {
        equipe = le.firstWhereOrNull((element) => element.id == m.idEquipe);
      });
    });
  }

  _initAllUser() {
    UserService.getInstance().getObsUsers().first.then((l) {

      setState(() {
        listeUser = l;
      });
      _initPresence();
    });


  }

  _initPresence() {
    _subListPresenceActivite=PresenceActiviteService.getInstance().getObsListPrensenceActivites(paramActivite!.activite).listen((lp) {
      setState(() {
        _listePresence = lp;
        _analysePresence();
      });
    });
  }

  String _getLibelle(){
    String ret="";
    if(paramActivite!.activite.isMatch() == true){
      if (equipe != null) ret=equipe!.libelle;
    }
    if(paramActivite!.activite.isTraining() == true){
      ret="Entrainement";
    }


    return(ret);
  }

  void _analysePresence(){
    listePresent.clear();
    listeAbsent.clear();
    listeNoResponse.clear();

    _listePresence.forEach((element) {
      if(element.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_YES) listePresent.add(element);
      if(element.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_NO) listeAbsent.add(element);
      if(element.codePresence == Constantes.CODE_PRESENCE_ACTIVITE_UNKNOWN) listeNoResponse.add(element);

      if(paramActivite!.user != null){
        if(element.idUser == paramActivite!.user.id){
          myMode=element.codePresence;
        }
      }
    });

    setState(() {


    });
  }

  Widget _getInfoBody(){
    Widget w=Container();
    if(paramActivite!.activite.isMatch() == true){
      Text te=Text(equipe!.libelle ,

          style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold));


      Text ts=Text("scsc" ,
          style: TextStyle(fontSize: 15));
      Text tsep=Text("-" ,
          style: TextStyle(fontSize: 15));
      List<Widget> lr = [];
      if((paramActivite!.activite as EntMatch).flagDomicile == true){
        Container c=Container(
            width: MediaQuery.of(context).size.width-200,
            color:Colors.red,
            child : Align(
              alignment: Alignment.centerLeft,
              child:
              Expanded(child: te)


            )


        );

        lr.add(ts);
        lr.add(tsep);
        lr.add(c);
      }
      else{
        Container c=Container(
            width: MediaQuery.of(context).size.width-200,
            //color:Colors.red,
            child : Align(
              alignment: Alignment.centerRight,
              child: te,
            )

        );
        lr.add(c);
        lr.add(tsep);
        lr.add(ts);
      }
      w=Row(
        children:lr,
      );
    }
    if(paramActivite!.activite.isTraining() == true){
      w=Text("Entrainement" ,
          style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold));
    }

    return(w);
  }

  Widget _getLogo(){
    Widget w=Container();

    if(paramActivite!.activite.isMatch() == true){
      if (equipe != null) w=Util.getLogoEquipe(equipe!);
    }


    return(w);
  }

  _back(){
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    if(paramActivite == null) return(Container());
    return(Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(100.0),
          child:AppBar(
              toolbarHeight: 95,
              automaticallyImplyLeading: false,
              title: _getHeader()
          ),
        ),
        body :
        _getBody(),

      )
    );
  }

  Widget _getHeader() {

    Column col=Column(
            children: <Widget>[
              _getFirstLine(),
              _getSecondLine(),
              _getSubject(),
            ],
          );

    Container cont = Container(
        margin: const EdgeInsets.all(10.0),
        padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          //border: Border.all(),
            color: Theme.of(context).colorScheme.secondary,
            borderRadius: BorderRadius.all(
                Radius.circular(10))), //       <--- BoxDecoration here

        child: col);

    return(col);

    }

  Widget _getFirstLine() {
    double titreSize = 15;

    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(paramActivite!.activite.getTitre(), style: TextStyle(fontSize: titreSize)),
        Text(paramActivite!.activite.getSousTitre(), style: TextStyle(fontSize: titreSize))
      ],
    );
    return (r);
  }

  Widget _getSecondLine() {
    double dateSize = 15;

    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(paramActivite!.activite.getDateLong(), style: TextStyle(fontSize: dateSize))

      ],
    );
    return (r);
  }

  Widget _getSubject() {

    double textSize = 15;

    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_back),
            iconSize: 30,
            tooltip: 'Retour',
            onPressed: () {
              _back();
            }),
        _getInfoBody(),
                /*color: Theme.of(context).colorScheme.primary*/
         _getLogo()

      ],
    );

    return (r);
  }

  Widget _getBody() {
    Column col = Column(
      children: <Widget>[
        _getTabBar(),
        _getTabView()
      ],
    );
    return (col);
  }
  Widget _getSousTitre() {
    String title="";

    switch (_tabController!.index) {
      case 0: // Présents
        title =_getTitle(listePresent," présent");
        break;
      case 1: // Absents
        title =_getTitle(listeAbsent," absent");
        break;
      case 2: // Sans Réponse
        title =_getTitle(listeNoResponse," sans réponse");
        break;
    }
    Text t = Text(title,style:TextStyle(color: Theme.of(context).colorScheme.secondary,fontSize: 15 ,fontWeight:FontWeight.bold));
    Container cont = Container(
        margin: const EdgeInsets.all(10.0),
        //padding: const EdgeInsets.all(5.0),
        //height:MediaQuery.of(context).size.height-220,
        //color:Colors.yellow,
        child: Align(
            alignment: Alignment.centerLeft,
            child:t
        ));
    return(cont);
  }

  String _getTitle(List<EntPresenceActivite> l,String base){
    String nb=l.length.toString();
    if(l.length == 0) nb="Aucun";
    String title = base;
    title = nb + title;
    if(l.length > 1) title=title + "s";

      return(title);
  }

  Widget _getListePresent(){
    List<String> lid = [];
    listePresent.forEach((element) {
      lid.add(element.idUser);
    });

    return (getBlocUser(lid));
  }

  Widget _getListeAbsent(){
    List<String> lid = [];
    listeAbsent.forEach((element) {
      lid.add(element.idUser);
    });


    return (getBlocUser(lid));
  }

  Widget _getListeNoReponse(){
    List<String> lid = [];
    listeNoResponse.forEach((element) {
      lid.add(element.idUser);
    });

    return (getBlocUser(lid));
  }

   Widget _getTabBar(){

    String str="";
    Widget w=TabBar(
      indicatorColor: Theme.of(context).colorScheme.primary,
      labelColor: Colors.grey,
      controller: _tabController,

      tabs: const <Widget>[
        Tab(
          text : "Présents",
          icon: Icon(Icons.thumb_up),
        ),
        Tab(
          text:"Absents",
          icon: Icon(Icons.thumb_down),
        ),
        Tab(
          text:"Sans réponse",
          icon: Icon(Icons.alt_route),
        ),
      ],
    );

    return(w);
  }

  Widget _getTabView(){
    TabBarView tbv=TabBarView(
      controller: _tabController,
      children: <Widget>[
        _getListePresent(),
        _getListeAbsent(),
        _getListeNoReponse()
      ],
    );

    Container cont = Container(
        margin: const EdgeInsets.all(10.0),
        //padding: const EdgeInsets.all(5.0),
        height:MediaQuery.of(context).size.height-270,
       // color:Colors.yellow,
        child: tbv);

    Column col = Column(
      children: [
        _getSousTitre(),
        cont
      ],
    );
    return(col);
  }

  Widget getBlocUser(List<String> lid){

    List<JoueurActivite> lja=[];

    EntUser? u;
    lid.forEach((id) {
      u=listeUser.firstWhereOrNull((u) => u.id == id);
      if(u != null) lja.add(JoueurActivite(u!,paramActivite!.user.id));
    });

    lja.sort((j1,j2){
      int ret=j1.user.nom.toUpperCase().compareTo(j2.user.nom.toUpperCase());
      return(ret);
    });

    ListView lv = ListView(
      children: lja,
    );

    Container c = Container(
        //margin: EdgeInsets.all(10),
        height: MediaQuery.of(context).size.height-250,
        //padding: EdgeInsets.all(5),
        child: lv
    );


    return (c);


  }
}
