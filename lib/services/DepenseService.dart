import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/models/EntDepense.dart';


class DepenseService {
  late BehaviorSubject<List<EntDepense>> _listSubject;
  late BehaviorSubject<int> _totalSubject;

  static DepenseService? _singleton;

  static void razSingleton() {
    _singleton = null;
    
  }

  static DepenseService getInstance() {
    if (_singleton == null) {
      _singleton = DepenseService._();
    }
    return (_singleton!);
  }

  DepenseService._() {
    _initListenerCollectionDepenses();
  }

  void _initListenerCollectionDepenses() {
    CollectionReference refCollection =
        FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_DEPENSES);

    _listSubject = BehaviorSubject<List<EntDepense>>();
    _totalSubject = BehaviorSubject<int>();

    refCollection.snapshots().listen((l) async {
      List<EntDepense> ld = [];
      int totalDepense=0;
      EntDepense d;

      l.docs.forEach((QueryDocumentSnapshot<Object?> o) async {
        if (o.exists) {
          d = EntDepense.fromFB(o);
          totalDepense+=d.montant;
          ld.add(d);
        }
      });
      ld.sort((a,b){
        int ret=-1;
        if(a.date.isAfter(b.date) == true) ret=1;
        return(ret);
      });
      _listSubject.add(ld);
      _totalSubject.add(totalDepense);

    });
  }

  Stream<List<EntDepense>> getObsDepense() {
    return (_listSubject.stream);
  }

  Stream<int> getObsTotal() {
    return (_totalSubject.stream);
  }

  Future<void> majDepense(EntDepense d){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_DEPENSES);

    Future<void> ret;
    if(d.id == "") ret=refCollection.add(d.toFB());
    else ret=refCollection.doc(d.id).set(d.toFB());

    return(ret);

  }

  Future<void> deleteDepense(EntDepense d){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_DEPENSES);

    Future<void> ret;
    ret=refCollection.doc(d.id).delete();

    return(ret);

  }

}
