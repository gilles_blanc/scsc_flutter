import 'package:flutter/material.dart';
import 'package:scsc_veteran/models/EntUser.dart';

class ModifPass extends StatefulWidget {
  ModifPass() {}

  @override
  _ModifPassPageState createState() => _ModifPassPageState();
}

class _ModifPassPageState extends State<ModifPass> {
  bool flagWait = false;
  final ctrlPass = TextEditingController();
  final ctrlConfirmPass = TextEditingController();

  late EntUser user;

  @override
  void initState() {
    super.initState();
    user = ModalRoute.of(context)!.settings.arguments as EntUser;

  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });
  }

  void _endWait() {
    setState(() {
      flagWait = false;
    });
  }

  void majPass() {
   /* do {
      if (ctrlConfirmPass.text != ctrlPass.text) {
        Util.error(context, "Les 2 mots de passe sont différents.");
        break;
      }
      if (ctrlConfirmPass.text.length < 6) {
        Util.error(
            context, "Le mot de passe doit contenir au moins 6 caractères.");
        break;
      }
      _startWait();
      AuthService.getInstance().modifPassword(ctrlPass.text).then((onValue) {
        _endWait();
        Util.alert(context, "Modification effectuée").then((onValue) {
          Navigator.of(context).pop();
        }).catchError((error) {
          Util.error(context, "Password can't be changed" + error.toString());
          //This might happen, when the wrong password is in, the user isn't found, or if the user hasn't logged in recently.
        });
      });
    } while (false);*/
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
        appBar: AppBar(
          title: Text("Modification du mot de passe"),
        ),
        body: Container(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
              Container(
                color: Colors.white,
                height: 10,
                child: flagWait
                    ? LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Theme.of(context).colorScheme.primary))
                    : Container(),
              ),
              _inputText("Mot de passe", ctrlPass, TextInputType.text),
              _inputText("Confirmation mot de passe", ctrlConfirmPass,
                  TextInputType.text),
              Container(
                  margin: EdgeInsets.only(right: 20),
                  child: RaisedButton(
                    child: Text("Valider"),
                    onPressed: majPass,
                    color: Theme.of(context).colorScheme.primary,
                    //textColor: Colors.yellow,
                    //padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    splashColor: Colors.grey,
                  ))
            ])));
  }

  Widget _inputText(
      String hint, TextEditingController controller, TextInputType kbType) {
    return Container(
        margin: const EdgeInsets.all(10.0),
        padding: EdgeInsets.only(left: 4, right: 4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            /*SizedBox(
            width: 80,
            child :Text(hint + ":"))
          ,*/
            new Flexible(
                child: TextField(
              decoration: new InputDecoration(
                  //hintText: hint,
                  labelText: hint),
              keyboardType: kbType,
              controller: controller,
              style: TextStyle(
                fontSize: 20,
              ),
            ))
          ],
        ));
  }
}
