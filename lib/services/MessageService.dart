import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntMessage.dart';

class MessageService {
  late BehaviorSubject<List<EntMessage>> _messageSubject;
  static MessageService? _singleton;

  static void razSingleton() {
    _singleton = null;
  }

  static MessageService getInstance() {
    if (_singleton == null) {
      _singleton = MessageService._();
    }
    return (_singleton!);
  }

  MessageService._() {
    initListener();

  }

  void initListener() {

    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_MESSAGES);

    _messageSubject = BehaviorSubject<List<EntMessage>>();

    EntMessage m;
    List<EntMessage> listeMessage =[];
    refCollection.snapshots().listen((l) {
      listeMessage = [];
      l.docs.forEach((QueryDocumentSnapshot<Object?> o) {
        if (o.exists) {
          m = EntMessage.fromFB(o);
          listeMessage.add(m);
        }
      });

      listeMessage.sort((m1,m2){
        int ret=-1;
        if(m1.date.millisecondsSinceEpoch > m2.date.millisecondsSinceEpoch) ret=1;
        return(ret);
      });
      _messageSubject.add(listeMessage);
    });
  }

  Stream<List<EntMessage>> getObsMessage() {
    return (_messageSubject.stream);
  }


  Future<dynamic> createMessage(EntMessage message) async {
    //Completer completer = new Completer<EntMessage>();

    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_MESSAGES);

    return (refCollection.add(message.toFB())); //doc(e.id).set(e.toFB());

    /*EntUploadTaskMultiple ut;
    message.tabPJ.forEach((element) {
      switch(element.typeFile){
        case Constantes.TYPE_FILE_IMAGE:
          ut = uploadObject(element, Constantes.OPTION_PJ_ALL);
          element.uploadTaskBig=ut.uploadTaskBIG;
          element.uploadTaskSmall=ut.uploadTaskSMALL;
          break;
        default:
          ut = uploadObject(element, Constantes.OPTION_PJ_BIG);
          element.uploadTaskBig=ut.uploadTaskBIG;
          break;
      }
    });

    create("commentaires", message).then((value){
      message.id=value;
      completer.complete(message);
    }).catchError((onError) {
        completer.complete(null);
    });
*/

    //return (completer.future);
  }

  /*Future<void> completeMessagePJ(EntMessage message) async {
    Completer completer = new Completer<void>();

    update("commentaires", message.id,message).then((value) {
      completer.complete();
    }).catchError((onError) {
      completer.complete();
    });
  }*/

  /*Future<void> _consolideMessageBigPJ(
      String newKey, EntMessage message, List<EntUploadObject> listePJ) async {

    //print("Message consolide big In ");
    Completer completer = new Completer<void>();
    List<Future<EntObject>> listeFutureBIG = List<Future<EntObject>>();
    listePJ.forEach((element) {
      listeFutureBIG.add(uploadObjectUrl(element,Constantes.OPTION_PJ_BIG));
    });

    Future.wait(listeFutureBIG).then((tab) {
      int indice=0;
      tab.forEach((o) {
        message.tabPJ[indice].urlBig=o.urlBig;
        indice++;
      });

      update("commentaires", newKey,message).then((value) {
        print("Message consolide big FIN OK");
        completer.complete();
      }).catchError((onError) {
        completer.complete();
      });
    }).catchError((onError) {
      completer.complete();
    });
    return (completer.future);
  }*/

  /*Future<String> _uploadFile(Asset a, String userId, double format) async {
    ByteData byteData = await _getDataReduit(a, format);
    List<int> imageData = byteData.buffer.asUint8List();
    String path = "pj/" +
        userId +
        "/" +
        new DateTime.now().millisecondsSinceEpoch.toString();

    String url = "to do";
    /*= await uploadFile(path, imageData);
    print("URL is $url");*/
    return (url);
  }*/

  /*Future<ByteData> _getDataReduit(Asset a, double max) async {
    //int wTB=a.originalHeight;
    //int hTB;

    int h = a.originalHeight;
    int w = a.originalWidth;
    double ratio;

    if (h > w) {
      if (h > max) {
        ratio = h.toDouble() / max;
        h = max.round();
        w = ((w.toDouble()) / ratio).round();
      }
    } else {
      if (w > max) {
        ratio = w.toDouble() / max;
        w = max.round();
        h = ((h.toDouble()) / ratio).round();
      }
    }

    return (a.getThumbByteData(w, h));
  }*/
}
