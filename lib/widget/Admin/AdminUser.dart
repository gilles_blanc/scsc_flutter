import 'package:flutter/material.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/UserService.dart';

class AdminUser extends StatefulWidget {
  AdminUser() {}

  @override
  _AdminUserPageState createState() => _AdminUserPageState();
}

class _AdminUserPageState extends State<AdminUser> {
  bool flagVersement = false;
  bool flagEntrainement = false;
  bool flagMatch = false;

  bool flagWait = false;
  late EntUser? user=null;

  @override
  void initState() {
    super.initState();
    Future.delayed(
      const Duration(milliseconds: 50),
          () {
            _initUser();
          },
    );

  }

  void _initUser(){
      user = EntUser.clone(ModalRoute.of(context)!.settings.arguments);
      setState(() {
        flagVersement = user!.flagPaiement;
        flagMatch = user!.flagMatch;
        flagEntrainement = user!.flagEntrainement;
      });

  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });

    //EasyLoading.show(status :msg);
  }

  void _endWait() {
    setState(() {
      flagWait = false;
    });
  }

  void majPassword() async {
    _startWait();
    //await AuthService.getInstance().resetPassword(user.email);
    _endWait();
  }

  void majUserInfo() async {
    _startWait();
    user!.flagMatch = flagMatch;
    user!.flagPaiement = flagVersement;
    user!.flagEntrainement = flagEntrainement;
    //await UserService.getInstance().modifUser(user);
    //UserService.razSingleton();
    Navigator.of(context).pop();
    _endWait();
  }

  void _changeMatch(bool? value) {
    setState(() => flagMatch = value!);
  }
  void _changeVersement(bool? value) {
    setState(() => flagVersement = value!);
  }
  void _changeEntrainement(bool? value) {
    setState(() => flagEntrainement = value!);
  }
    @override
    Widget build(BuildContext context) {
      if (user == null) return(Container());

      return Scaffold(
          appBar: AppBar(
            title: Text("Admin User " + user!.getLibelle()),
          ),
          body: Container(
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.end,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    height: 10,
                    child: flagWait
                        ? LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Theme.of(context).colorScheme.primary))
                        : Container(),
                  ),
                  userInfo(user!),
                ],
              )));
    }

    Widget userInfo(EntUser u) {
      return (Container(
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _title("Options"),
              Column(
                //mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  CheckboxListTile(
                    checkColor: Colors.white,
                    activeColor: Theme.of(context).colorScheme.primary,
                    title: Text("Versement"),
                    value: flagVersement,
                    onChanged: _changeVersement,
                  ),
                  CheckboxListTile(
                    checkColor: Colors.white,
                    activeColor: Theme.of(context).colorScheme.primary,
                    title: Text("Entrainement"),
                    value: flagEntrainement,
                    onChanged: _changeEntrainement,
                  ),
                  CheckboxListTile(
                    checkColor: Colors.white,
                    activeColor: Theme.of(context).colorScheme.primary,
                    title: Text("Match"),
                    value: flagMatch,
                    onChanged: _changeMatch,
                  ),
                  Padding(
                      padding: EdgeInsets.only(right: 14, top: 10),
                      child: RaisedButton(
                        child: Text("Valider"),
                        onPressed: majUserInfo,
                        color: Theme.of(context).colorScheme.primary,
                        //textColor: Colors.yellow,
                        //padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        splashColor: Colors.grey,
                      )),
                  Padding(
                      padding: EdgeInsets.only(right: 14, top: 10),
                      child: RaisedButton(
                        child: Text("Re Init Password"),
                        onPressed: majPassword,
                        color: Theme.of(context).colorScheme.primary,
                        //textColor: Colors.yellow,
                        //padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        splashColor: Colors.grey,
                      ))
                ],
              )
            ],
          )));
    }

    Widget _title(String titre) {
      return Container(
        //margin: const EdgeInsets.all(10.0),
        //padding: EdgeInsets.only(left: 20,top:5,bottom:5),
          child: Text(titre,
              style: TextStyle(
                  fontStyle: FontStyle.italic,
                  color: Theme.of(context).colorScheme.primary)));
    }
  }

