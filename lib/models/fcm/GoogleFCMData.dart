class GoogleFCMData{

 late String idUser;
 late int type;


 GoogleFCMData(){
  idUser="";
  type=0;
 }

 Map<String, dynamic> toJson() => _itemToJson(this);

 Map<String, dynamic> _itemToJson(GoogleFCMData instance) {
  return <String, dynamic>{
   'idUser': instance.idUser,
   'type': instance.type
  };
 }
}
