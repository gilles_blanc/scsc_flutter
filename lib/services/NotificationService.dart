import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/models/EntNotification.dart';
import 'package:http/http.dart';

class NotificationService {
  static NotificationService? _singleton;
  late FirebaseMessaging messaging;

  static void razSingleton() {
    _singleton = null;
    
  }

  static NotificationService getInstance() {
    if (_singleton == null) {
      _singleton = NotificationService._();
    }
    return (_singleton!);
  }

  NotificationService._() {

  }

  Future<String?> getUserTokenFCM() async{
    Future<String?> f;
    if(kIsWeb) f=_getUserTokenFCMWeb();
    else f=_getUserTokenFCMMob();
    return(f);
  }

  Future<String?> _getUserTokenFCMMob() async{

    messaging = FirebaseMessaging.instance;
    return(messaging.getToken());
  }

  Future<String?> _getUserTokenFCMWeb() async{
    messaging = FirebaseMessaging.instance;
    await messaging.requestPermission();
    return(messaging.getToken(vapidKey :Constantes.SERVER_KEY_FCM_WEB));
  }

  void sendNotificationFCM(EntNotification notif) {

    Uri uri =  Uri.https(Constantes.URI_FCM_SERVER, Constantes.URI_FCM_PATH);
    Map<String, String> headers = Map<String, String>();
    headers["Content-type"] = "application/json";
    headers["Authorization"] = "key=" + Constantes.KEY_GOOGLE_PUSH_SERVICE;

    String body=notif.build();
    if(body != "") {
      print("body=[" + body + "]");
      post(uri, headers: headers, body: body).then((rep) {
        //print("RETOUR HTTP : " + rep.statusCode.toString());
      });
    }
  }
}
