import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/models/EntEquipe.dart';
import 'package:scsc_veteran/models/EntMatch.dart';

class EquipeService {
  late BehaviorSubject<List<EntEquipe>> _listSubject;
  static EquipeService? _singleton;

  static void razSingleton() {
    _singleton = null;
    
  }

  static EquipeService getInstance() {
    if (_singleton == null) {
      _singleton = EquipeService._();
    }
    return (_singleton!);
  }

  EquipeService._() {
    _initListenerCollectionEquipes();
  }

  void _initListenerCollectionEquipes() {
    CollectionReference refCollection =
        FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_EQUIPES);

    _listSubject = BehaviorSubject<List<EntEquipe>>();

    refCollection.snapshots().listen((l) async {
      List<EntEquipe> le = [];
      EntEquipe e;

      l.docs.forEach((QueryDocumentSnapshot<Object?> o) async {
        if (o.exists) {
          e = EntEquipe.fromFB(o);
          le.add(e);
        }
      });
      le.sort((a,b){
        return(a.libelle.toLowerCase().compareTo(b.libelle.toLowerCase()));
      });
      _listSubject.add(le);

    });
  }

  Stream<List<EntEquipe>> getObsEquipes() {
    return (_listSubject.stream);
  }

  Future<void> majEquipe(EntEquipe e){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_EQUIPES);

    Future<void> ret;
    if(e.id == "") ret=refCollection.add(e.toFB());
    else ret=refCollection.doc(e.id).set(e.toFB());

    return(ret);

  }

  Future<void> deleteEquipe(EntEquipe e){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_EQUIPES);

    Future<void> ret;
    ret=refCollection.doc(e.id).delete();

    return(ret);

  }

}
