import 'dart:async';

import 'package:flutter/material.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntEquipe.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/BinaryService.dart';
import 'package:scsc_veteran/services/EquipeService.dart';
import 'package:scsc_veteran/services/UserService.dart';
import 'package:scsc_veteran/widget/Equipe/ElemEquipe.dart';

class AdminEquipe extends StatefulWidget {
  AdminEquipe() {}

  @override
  _AdminEquipePageState createState() => _AdminEquipePageState();
}

class _AdminEquipePageState extends State<AdminEquipe> {
  bool flagWait = false;
  late StreamSubscription<List<EntEquipe>> _subEquipes;
  List<EntEquipe> _listeEquipe =[];
  @override
  void initState() {
    super.initState();
    _initEquipe();

  }


  @override
  void dispose() {
    Util.freeSub(_subEquipes);


    super.dispose();
  }

  void _initEquipe(){
    _subEquipes=EquipeService.getInstance().getObsEquipes().listen((l) {
      setState(() {
        _listeEquipe=l;

      });
    });


  }

  void addEquipe()  {
    EntEquipe newEquipe = EntEquipe();
    BinaryService.getInstance().getDefaultEquipeLogo().then((url){
      newEquipe.logo=url;
      Navigator.of(context).pushNamed('/AddEquipe',arguments: newEquipe);
    });

  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });


  }

  void _endWait() {
    setState(() {
      flagWait = false;
    });
  }

  void majEquipe() async {
    _startWait();
    //await AuthService.getInstance().resetPassword(user.email);
    //Navigator.of(context).pop();
    _endWait();
  }

    @override
    Widget build(BuildContext context) {

      return Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: AppBar(
              title: Text("Admin Equipe"),

              actions: <Widget>[
                IconButton(
                    icon: Icon(Icons.add),
                    tooltip: 'Add',
                    onPressed: addEquipe)
              ],
            ),
          ),
          body: Container(
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.end,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    height: 10,
                    child: flagWait
                        ? LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Theme.of(context).colorScheme.primary))
                        : Container(),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height-115,

                    child:  _getListeEquipe(),
                  ),
                ],
              )));
    }

    Widget _getListeEquipe(){
    List<Widget> lw = [];
    _listeEquipe.forEach((element) {
      lw.add(ElemEquipe(element));

    });

    ListView lv=ListView(
        children: lw,
    );
      return(lv);
    }
  }

