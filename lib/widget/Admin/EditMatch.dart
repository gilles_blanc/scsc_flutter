import 'dart:async';

import 'package:collection/src/iterable_extensions.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntEquipe.dart';
import 'package:scsc_veteran/models/EntMatch.dart';
import 'package:scsc_veteran/services/EquipeService.dart';
import 'package:scsc_veteran/services/MatchService.dart';


class EditMatch extends StatefulWidget {
  EditMatch() {}

  @override
  _EditMatchPageState createState() => _EditMatchPageState();
}

class _EditMatchPageState extends State<EditMatch> {
  bool flagWait = false;
  EntMatch currentMatch=new EntMatch();
  final ctrlDateMatch = TextEditingController();
  final ctrlHourMatch = TextEditingController();
  late StreamSubscription<List<EntEquipe>> _subEquipe;
  List<EntEquipe> listeEquip=[];
  EntEquipe? currentEquipe=null;

  @override
  void initState() {
    super.initState();

    _startWait();
    initializeDateFormatting("fr_FR", null).then((o) {
      currentMatch = (ModalRoute.of(context)!.settings.arguments as EntMatch).clone();
      _initEquipe();
    });

  }

  @override
  void dispose() {
    Util.freeSub(_subEquipe);

    super.dispose();
  }

  void _initEquipe(){

    _subEquipe=EquipeService.getInstance().getObsEquipes().listen((l) {
      setState(() {
        listeEquip=l;
        currentEquipe = l.firstWhereOrNull((element) => element.id == currentMatch.idEquipe);
        ctrlDateMatch.text = DateFormat.yMd("fr_FR").format(currentMatch.date);
        ctrlHourMatch.text = currentMatch.date.hour.toString() + "h" +currentMatch.date.minute.toString();
      });
      _endWait();
    });


  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });


  }

  void _endWait() {
    setState(() {
      flagWait = false;
    });
  }

  void majMatch() async {
    _startWait();
    currentMatch.idEquipe=currentEquipe!.id;
    MatchService.getInstance().majMatch(currentMatch).then((ret){
      _endWait();
      Navigator.of(context).pop();
    });
    //await AuthService.getInstance().resetPassword(user.email);
    //Navigator.of(context).pop();

  }

  void _changeFlagDomicile(bool? value) =>
      setState(() => currentMatch.flagDomicile = value!);

  @override
    Widget build(BuildContext context) {

      return Scaffold(
          appBar: AppBar(
            title: Text("Add/Modif Match"),
          ),
          body: body()
      );
    }

  Widget body() {

    ListView lv=ListView(
      children: <Widget>[
        loader(),
        ddEquipe(),
        _inputDate("Date du match", ctrlDateMatch),
        _inputHour("Heure du match", ctrlHourMatch),
        CheckboxListTile(
            checkColor: Colors.white,
            activeColor: Theme.of(context).colorScheme.primary,
            value: currentMatch.flagDomicile,
            onChanged: _changeFlagDomicile,
            title: Text('Domicile',style: TextStyle(
                fontSize: 15))),
        Padding(
            padding: EdgeInsets.only(right: 10,left:10.0),
            child: ElevatedButton(
              child: Text("Valider"),
              onPressed: isValide() ? majMatch : null,

            )),

      ],
    );

    Container c=Container(
        margin: const EdgeInsets.all(14.0),
        padding: EdgeInsets.only(left: 4, right: 4),
        width: MediaQuery.of(context).size.width -10,
        height: (MediaQuery.of(context).size.height ) / 2,
        child: lv
    );

    return(c);

  }

  Widget loader(){
    Container l=Container(
      color: Colors.white,
      height: 10,
      child: flagWait
          ? LinearProgressIndicator(
          backgroundColor: Colors.white,
          valueColor: new AlwaysStoppedAnimation<Color>(
              Theme.of(context).colorScheme.primary))
          : Container(),
    );
    return(l);
  }

  Widget ddEquipe(){
    DropdownButton dd=DropdownButton<EntEquipe>(
      selectedItemBuilder: (BuildContext context) => listeEquip
          .map((e) => Center(
        child: Text(
          e.libelle,
          style: TextStyle(
              fontSize: 16,
              color: Colors.green,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold),
        ),
      ))
          .toList(),
      style:TextStyle(
        color:Colors.green
      ),
      items: listeEquip.map((EntEquipe e) {
        return DropdownMenuItem<EntEquipe>(
          value: e,
          child: Text(e.libelle),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          currentEquipe=newValue;
        });

      },
      value: currentEquipe,
    );

    return(dd);
  }

  Widget _inputDate(String hint, TextEditingController controller) {
    MaskTextInputFormatter dateFormatter = new MaskTextInputFormatter(mask: '##/##/####', filter: { "#": RegExp(r'[0-9]') });

    Row r= Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            new Flexible(
                child: TextField(
                  //maxLength: 10,
                  keyboardType: TextInputType.number,
                  inputFormatters: [dateFormatter],
                  decoration: new InputDecoration(
                    //hintText: hint,
                      labelText: hint),
                  style: TextStyle(
                    fontSize: 15,
                  ),
                  controller: controller,

                  /*inputDecoration: new InputDecoration(
                  counterText: "",

                  //hintText: hint,
                  labelText: hint),*/
                )),
            IconButton(
                icon: Icon(Icons.calendar_today),
                tooltip: 'Date du match',
                onPressed:  () {
                  modifDate(currentMatch.date);
                } ),
          ],
        );
    return(r);
  }
  Widget _inputHour(String hint, TextEditingController controller) {
    MaskTextInputFormatter hourFormatter = new MaskTextInputFormatter(mask: '##h##', filter: { "#": RegExp(r'[0-9]') });

    Row r= Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        new Flexible(
            child: TextField(
              //maxLength: 10,
              keyboardType: TextInputType.number,
              inputFormatters: [hourFormatter],
              decoration: new InputDecoration(
                //hintText: hint,
                  labelText: hint),
              style: TextStyle(
                fontSize: 15,
              ),
              controller: controller,


            )),
        IconButton(
            icon: Icon(Icons.access_time),
            tooltip: 'Heure du match',
            onPressed:  () {
              modifHour(currentMatch.date);
            } ),
      ],
    );
    return(r);
  }

  bool isValide(){
    bool ret=true;
    if(currentEquipe == null) ret=false;

    return(ret);
  }

  void modifDate(DateTime initDate) async {
    // Imagine that this function is
    // more complex and slow.
    DateTime? d = await showDatePicker(
      context: context,
      initialDate: initDate,
      firstDate: DateTime.now()
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light(),
          child: child!,
        );
      },
    );
    if (d != null) {
      d = new DateTime(d.year,d.month,d.day,initDate.hour,initDate.minute);
      ctrlDateMatch.text = DateFormat.yMd("fr_FR").format(d);
      currentMatch.date = d;
    }
  }

  void modifHour(DateTime initDate) async {
    // Imagine that this function is
    // more complex and slow.
    TimeOfDay? t= await showTimePicker(
        context: context,
      initialTime: TimeOfDay.fromDateTime(initDate),

    );
    if (t != null) {
      DateTime d = DateTime(initDate.year, initDate.month, initDate.day, t.hour, t.minute);
      ctrlHourMatch.text = t.hour.toString() + "h" + t.minute.toString();
      currentMatch.date = d;
    }
  }
  }

