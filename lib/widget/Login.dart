import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/services/AuthService.dart';

class Login extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<Login> {
  final ctrlEmail = TextEditingController();
  final ctrlPassword = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  String version = "";


  bool flagRemember = false;
  bool flagWait = false;


  @override
  void initState() {
    super.initState();


    _restorePersist();

    _getVersion();
  }

  @override
  void dispose() {

    super.dispose();
  }

  void _restorePersist() async  {

      bool flag;
      String email;
      String pass;

      flag = await Util.getPersistBool(Constantes.PERSIST_REMEMBER_CREDENTIALS);
      if (flag == false) {
        pass = "";
        email = "";
      } else {
        pass = await Util.getPersistString(Constantes.PERSIST_PASSWORD);
        email = await Util.getPersistString(Constantes.PERSIST_EMAIL);
      }
      setState(() {
        flagRemember = flag;
        ctrlEmail.text = email;
        ctrlPassword.text = pass;
      });

  }

  void _savePersist() {
    String password = "";
    String email = "";

    if (flagRemember == true) {
      password = ctrlPassword.text;
      email = ctrlEmail.text;
    }

    Util.setPersistBool(Constantes.PERSIST_REMEMBER_CREDENTIALS, flagRemember);
    Util.setPersistString(Constantes.PERSIST_PASSWORD, password);
    Util.setPersistString(Constantes.PERSIST_EMAIL, email);
  }

  void _startWait(String msg) {
    setState(() {
      flagWait = true;
    });

  }

  void _endWait(String code, bool flagOK) {
    setState(() {
      flagWait = false;
    });

    String msg = "";

    if (flagOK == false) {
      msg = code;
      do {
        if (code.indexOf("invalid-email") >= 0) {
          msg = "Adresse mail invalide.";
          break;
        }
        if (code.indexOf("user-not-found") >= 0) {
          msg = "Adresse mail inconnue.";
          break;
        }
        if (code.indexOf("wrong-password") >= 0) {
          msg = "Mot de passe erroné.";
          break;
        }
        if (code.indexOf("too-many-request") >= 0) {
          msg =
          "Trops d'essais en erreur conséqutifs, veuillez ré-essayer dans 10 mns.";
          break;
        }
      } while (false);

      Util.error(context, msg);
    }
  }

  void _login() {
    _startWait("Identification en cours");
    _savePersist();

    Util.razCache();

    AuthService.getInstance()
        .loginEmailPassword(ctrlEmail.text, ctrlPassword.text)
        .then((u) {

    }, onError: (e) {
      _endWait(e.code, false);
    });
  }

  void _ajout() {

  }

  Future<bool> confirmeExit() async {
    bool ret = await Util.confirme(
        context, "Voulez vous vraiment quitter l'application ?", "Déconnexion");

    if (ret == true) {
      _savePersist();
      SystemNavigator.pop;
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    }

    return (ret); // Empêche une double navigation (Back)
  }

  void _getVersion() {
    // TO DO
    /*ParamService.getInstance().getParam().then((p) {
      setState(() {
        version = p.getStrVersion();
      });
    });*/
  }

  void _changeRemember(bool? value) => setState(() => flagRemember = value!);

  @override
  Widget build(BuildContext context) {
    return (new WillPopScope(
        onWillPop: confirmeExit,
        child: new Scaffold(
            resizeToAvoidBottomInset: false,
            key: _scaffoldKey,
            body: ListView(
              children: <Widget>[
                Container(
                  color: Colors.white,
                  height: 10,
                  child: flagWait == true
                      ? LinearProgressIndicator(
                          backgroundColor: Colors.white,
                          valueColor: new AlwaysStoppedAnimation<Color>(
                              Theme.of(context).colorScheme.primary))
                      : Container(),
                ),
                _title("SCSC Vétérans"),
                logo(),
                _input(
                    Icon(
                      Icons.mail,
                      color: Theme.of(context).colorScheme.primary,
                      size: 20.0,
                    ),
                    "Email",
                    ctrlEmail,
                    false,
                    TextInputType.emailAddress,
                    true),
                _input(
                    Icon(
                      Icons.lock,
                      color: Theme.of(context).colorScheme.primary,
                      size: 20.0,
                    ),
                    "Mot de passe",
                    ctrlPassword,
                    true,
                    TextInputType.text,
                    false),
                CheckboxListTile(
                    checkColor: Colors.white,
                    activeColor: Theme.of(context).colorScheme.primary,
                    value: flagRemember == null ? false : flagRemember,
                    onChanged: (bool? newValue) {
                      _changeRemember(newValue);
                    },
                    title: Text('Se souvenir de moi')),
                filledButton("S'identifier", Colors.blue, Colors.red,
                    Colors.green, Colors.white, _login),
                /*filledButton("Créer un compte", Colors.blue, Colors.red,
                    Colors.grey, Colors.white, _ajout),*/
                GestureDetector(
                    onTap: () {
                      AuthService.getInstance()
                          .resetPassword(ctrlEmail.text)
                          .then((ret) {
                        Util.alert(context,
                            "Un mail a été envoé à votre adresse (Attention, il se trouvera peut être dans le dossier des mails indésirables ..), veuillez suivre le lien pour saisir un nouveau mot de passe.",
                            "Mot de passe oublié",Constantes.DLG_BIG_HEIGHT);
                      }).catchError((onError) {
                        Util.error(context, onError.message, onError.code);
                      });
                    },
                    child: Padding(
                        padding: EdgeInsets.only(left: 20, right: 20, top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Mot de passe oublié",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color:
                                      Theme.of(context).colorScheme.secondary),
                            ),
                            Text(version,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blueGrey))
                          ],
                        )))
              ],
            ))));
  }

  Widget _title(String hint) {
    return Container(
        margin: const EdgeInsets.all(5.0),
        padding: EdgeInsets.only(left: 4, right: 4),
        child: Text(
          hint,
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 30.0,
              color: Theme.of(context).colorScheme.primary),
        ));
  }

  Widget _input(Icon icon, String hint, TextEditingController controller,
      bool obsecure, TextInputType kbType, bool def) {
    return Container(
      margin: const EdgeInsets.all(4.0),
      padding: EdgeInsets.only(left: 4, right: 4),
      child: TextField(
        keyboardType: kbType,
        //autofocus: def,
        controller: controller,
        obscureText: obsecure,
        style: TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
            hintStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 10),
            hintText: hint,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide(
                color: Theme.of(context).colorScheme.primary,
                width: 2,
              ),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide(
                color: Theme.of(context).colorScheme.primary,
                width: 3,
              ),
            ),
            prefixIcon: Padding(
              child: IconTheme(
                data:
                    IconThemeData(color: Theme.of(context).colorScheme.primary),
                child: icon,
              ),
              padding: EdgeInsets.only(left: 30, right: 10),
            )),
      ),
    );
  }

  Widget filledButton(String text, Color splashColor, Color highlightColor,
      Color fillColor, Color textColor, void function()) {
    return Container(
        margin: const EdgeInsets.all(4.0),
        padding: EdgeInsets.only(left: 4, right: 4),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(primary: fillColor),
          child: Text(
            text,
            style: TextStyle(
                fontWeight: FontWeight.bold, color: textColor, fontSize: 20),
          ),
          onPressed: () {
            function();
          },
        ));
  }

  Widget logo() {
    return Center(
        child: Container(
      margin: const EdgeInsets.all(4.0),
      padding: EdgeInsets.only(left: 4, right: 4),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 150,
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Container(
                  height: 150,
                  //width: MediaQuery.of(context).size.width,
                  child: Align(
                      alignment: Alignment.center,
                      child: new ClipRRect(
                        borderRadius: new BorderRadius.circular(15.0),
                        child: Image(
                            image: AssetImage('assets/images/bk_terrain.jpg')),
                      ))),
            ),
          ],
        ),
      ),
    ));
  }
}
