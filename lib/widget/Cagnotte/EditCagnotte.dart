import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:scsc_veteran/models/EntCagnotte.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:scsc_veteran/services/DepenseService.dart';
import 'package:scsc_veteran/services/VersementService.dart';

class EditCagnotte extends StatefulWidget {
  EditCagnotte() {}

  @override
  _EditCagnottePageState createState() => _EditCagnottePageState();
}

class _EditCagnottePageState extends State<EditCagnotte> {
  bool flagWait = false;
  bool flagVersement = false;
  bool flagDepense = false;
  final ctrlMontant = TextEditingController();
  final ctrlLibelle = TextEditingController();
  final ctrlDate = TextEditingController();
  EntCagnotte _currentCagnotte = new EntCagnotte();

  @override
  void initState() {
    super.initState();
    initializeDateFormatting("fr_FR", null).then((o) {
      _initCagnotte();
    });


  }

  void _initCagnotte(){

      setState(() {
        _currentCagnotte = (ModalRoute.of(context)!.settings.arguments as EntCagnotte).clone();
        if(_currentCagnotte.versement != null) {
          ctrlMontant.text=_currentCagnotte.versement!.montant.toString();
          ctrlDate.text = DateFormat.yMd("fr_FR").format(_currentCagnotte.versement!.date);

          flagVersement = true;
        }

        if(_currentCagnotte.depense != null){
          ctrlMontant.text=_currentCagnotte.depense!.montant.toString();
          ctrlLibelle.text=_currentCagnotte.depense!.libelle;
          ctrlDate.text = DateFormat.yMd("fr_FR").format(_currentCagnotte.depense!.date);
          flagDepense=true;
        }



      });

  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });


  }

  void _endWait() {
    setState(() {
      flagWait = false;
    });
  }

  void majCagnotte() async {
    _startWait();
    if(flagVersement == true){
      _currentCagnotte.versement!.montant=int.parse(ctrlMontant.text);
      VersementService.getInstance().modifVersement(_currentCagnotte.versement!).then((ret){
        _endWait();
        Navigator.of(context).pop();
      });
    }
    if(flagDepense == true){
      _currentCagnotte.depense!.montant=int.parse(ctrlMontant.text);
      _currentCagnotte.depense!.libelle=ctrlLibelle.text;
      DepenseService.getInstance().majDepense(_currentCagnotte.depense!).then((ret){
        _endWait();
        Navigator.of(context).pop();
      });
    }

  }


    @override
    Widget build(BuildContext context) {
      if((flagDepense == false) && (flagVersement == false)) return(Container());
      String title="Add/Modif ";
      if(flagVersement == true) title+="Versement";
      if(flagDepense == true) title+="Dépense";
      return Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: AppBar(
              title: Text(title),
            ),
          ),
          body: _getBody()
      );
    }

    Widget _getBody(){
      List<Widget> lw = [Container(
        color: Colors.white,
        height: 10,
        child: flagWait
            ? LinearProgressIndicator(
            backgroundColor: Colors.white,
            valueColor: new AlwaysStoppedAnimation<Color>(
                Theme.of(context).colorScheme.primary))
            : Container(),
      ),


      ];
      String titre="";
      if(flagVersement == true) titre="Date versement";
      if(flagDepense == true) titre="Date dépense";
      lw.add(_inputDate(titre, ctrlDate));
      if(flagDepense == true){
        lw.add(_input("Libellé",ctrlLibelle));
      }
      lw.add(_inputNumber("Montant",ctrlMontant));

      Padding p=Padding(
        padding: EdgeInsets.only(right: 10,left:10.0),
        child: ElevatedButton(
        child: Text("Valider"),
        onPressed: majCagnotte ,

        ));
      lw.add(p);

      ListView lv = ListView(
        children: lw,
      );
      return(lv);
    }
  Widget _inputDate(String hint, TextEditingController controller) {
    MaskTextInputFormatter dateFormatter = new MaskTextInputFormatter(mask: '##/##/####', filter: { "#": RegExp(r'[0-9]') });

    Row r= Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        new Flexible(
            child: TextField(
              //maxLength: 10,
              keyboardType: TextInputType.number,
              inputFormatters: [dateFormatter],
              decoration: new InputDecoration(
                //hintText: hint,
                  labelText: hint),
              style: TextStyle(
                fontSize: 15,
              ),
              controller: controller,

              /*inputDecoration: new InputDecoration(
                  counterText: "",

                  //hintText: hint,
                  labelText: hint),*/
            )),
        IconButton(
            icon: Icon(Icons.calendar_today),
            tooltip: 'Date du match',
            onPressed:  () {
              modifDate();
            } ),
      ],
    );
    Padding p=Padding(
        padding: EdgeInsets.only(left: 14, right: 14),
        child: r
    );
    return(p);
  }

  void modifDate() async {
    // Imagine that this function is
    // more complex and slow.
    DateTime initDate=DateTime.now();
    if(flagVersement == true) initDate=_currentCagnotte.versement!.date;
    if(flagDepense == true) initDate=_currentCagnotte.depense!.date;

    DateTime? d = await showDatePicker(
        context: context,
        initialDate: initDate,
        firstDate: DateTime.now()
        lastDate: DateTime(2030),
        builder: (BuildContext context, Widget? child) {
    return Theme(
    data: ThemeData.light(),
    child: child!,
    );
    },
    );
    if (d != null) {
    ctrlDate.text = DateFormat.yMd("fr_FR").format(d);
    if(flagVersement == true) _currentCagnotte.versement!.date=d;
    if(flagDepense == true) _currentCagnotte.depense!.date=d;
    }
  }

  Widget _input(String hint, TextEditingController controller) {

    Widget w=TextField(
              //maxLength: 10,
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                //hintText: hint,
                  labelText: hint),
              style: TextStyle(
                fontSize: 15,
              ),
              controller: controller,


            );

    Padding p=Padding(
        padding: EdgeInsets.only(left: 14, right: 14),
        child: w
    );
        return(p);
  }

  Widget _inputNumber(String hint, TextEditingController controller) {

    Widget w=TextField(
      //maxLength: 10,
      keyboardType: TextInputType.number,
      decoration: new InputDecoration(
        //hintText: hint,
          labelText: hint),
      style: TextStyle(
        fontSize: 15,
      ),
      controller: controller,


    );

    Padding p=Padding(
        padding: EdgeInsets.only(left: 14, right: 14),
        child: w
    );
    return(p);
  }
}

