import 'dart:async';

import 'package:flutter/material.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntMatch.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/MatchService.dart';
import 'package:scsc_veteran/services/UserService.dart';
import 'package:scsc_veteran/widget/Activite/ElemActivite.dart';

class AdminMatch extends StatefulWidget {
  AdminMatch() {}

  @override
  _AdminMatchPageState createState() => _AdminMatchPageState();
}

class _AdminMatchPageState extends State<AdminMatch> {
  bool flagWait = false;
  late StreamSubscription<List<EntMatch>> _subMatchs;
  late StreamSubscription<EntUser> _subUser;
  late EntUser _currentUser;
  List<ElemActivite> _listeMatch=[];

  @override
  void initState() {
    super.initState();
    _initUser();


  }

  @override
  void dispose() {
    Util.freeSub(_subMatchs);
    Util.freeSub(_subUser);

    super.dispose();
  }


  void _initUser() {
    _subUser = UserService.getInstance().getObsCurrentUser().listen((u) {
      _initMatch();
      setState(() {
        _currentUser = u;
      });
    });
  }

  void _initMatch(){
      _subMatchs=MatchService.getInstance().getObsMatchs().listen((event) {
        setState(() {
          _fillListMatch(event);

        });
      });


  }

  void addMatch() {
    EntMatch newMatch = EntMatch();

    Navigator.of(context).pushNamed('/AddMatch',arguments: newMatch);
  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });


  }

  void _endWait() {
    setState(() {
      flagWait = false;
    });
  }



  void _fillListMatch(List<EntMatch> lm){
    ElemActivite ea;

    _listeMatch=[];
    lm.forEach((element) {
      ea = ElemActivite(element,_currentUser,Constantes.MODE_ELEM_ACTIVITE_ADMIN);
      _listeMatch.add(ea);
      //print("AJOUT MATCH id=" +element.id +" -> equipe = " + element.idEquipe);
    });

  }

  Widget getListMatch(){
    ListView lv=ListView(
      children: List.from(_listeMatch),
    );
    return(lv);
  }
    @override
    Widget build(BuildContext context) {

      return Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: AppBar(
              title: Text("Admin Match"),
            /*  leading: Container(
                padding: EdgeInsets.only(left: 5, top: 5),
                child: Container(), // tO DO Avatar(currentUser, 1),
              ),*/
              actions: <Widget>[
                IconButton(
                    icon: Icon(Icons.add),
                    tooltip: 'Add',
                    onPressed: addMatch)
              ],
            ),
          ),
          body: Container(
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.end,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    height: 10,
                    child: flagWait
                        ? LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Theme.of(context).colorScheme.primary))
                        : Container(),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height-115,

                    child:  getListMatch(),
                  ),

                ],
              )));
    }


  }

