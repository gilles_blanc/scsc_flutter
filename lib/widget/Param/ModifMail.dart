import 'dart:async';

import 'package:flutter/material.dart';
import 'package:scsc_veteran/models/EntUser.dart';


class ModifMail extends StatefulWidget {
  ModifMail() {}

  @override
  _ModifMailPageState createState() => _ModifMailPageState();
}

class _ModifMailPageState extends State<ModifMail> {
  bool flagWait = false;
  final ctrlMail = TextEditingController();
  late EntUser user;

  @override
  void initState() {
    super.initState();
    user = ModalRoute.of(context)!.settings.arguments as EntUser;

    setState(() {
      ctrlMail.text = user.email;
    });

  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });
  }

  void _endWait() {
    setState(() {
      flagWait = false;
    });
  }

  void majMail() {
   /* _startWait();
    String newMail = ctrlMail.text;
    AuthService.getInstance().modifEmail(newMail).then((onValue) {
      //AuthService.getInstance().getCurrentUser().email=newMail;
      user.email = newMail;
      UserService.getInstance().modifUser(user).then((ok) {
        _endWait();
        Util.alert(context, "Modification effectuée").then((onValue) {
          Navigator.of(context).pop();
        });
      }).catchError((error) {
        _endWait();
        Util.alert(context, "Modification effectuée").then((onValue) {
          Navigator.of(context).pop();
        });
      }).catchError((error) {
        Util.error(context, "Modification email pb" + error.toString());
        //This might happen, when the wrong password is in, the user isn't found, or if the user hasn't logged in recently.
      });
    });*/
  }

  Future<bool> error(String mes) async {
    bool ret;

    ret = await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text("Erreur"),
        content: new Text(mes),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('Ok'),
          ),
        ],
      ),
    );
    return (ret);
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
        appBar: AppBar(
          title: Text("Modification de l'adresse mail"),
        ),
        body: Container(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
              Container(
                color: Colors.white,
                height: 10,
                child: flagWait
                    ? LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Theme.of(context).colorScheme.primary))
                    : Container(),
              ),
              _inputText("Adresse mail", ctrlMail, TextInputType.text),
              Container(
                  margin: EdgeInsets.only(right: 20),
                  child: RaisedButton(
                    child: Text("Valider"),
                    onPressed: majMail,
                    color: Theme.of(context).colorScheme.primary,
                    //textColor: Colors.yellow,
                    //padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    splashColor: Colors.grey,
                  ))
            ])));
  }

  Widget _inputText(
      String hint, TextEditingController controller, TextInputType kbType) {
    return Container(
        margin: const EdgeInsets.all(10.0),
        padding: EdgeInsets.only(left: 4, right: 4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            /*SizedBox(
            width: 80,
            child :Text(hint + ":"))
          ,*/
            new Flexible(
                child: TextField(
              decoration: new InputDecoration(
                  //hintText: hint,
                  labelText: hint),
              keyboardType: kbType,
              controller: controller,
              style: TextStyle(
                fontSize: 20,
              ),
            ))
          ],
        ));
  }
}
