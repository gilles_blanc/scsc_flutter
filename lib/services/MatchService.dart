import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/models/EntMatch.dart';

class MatchService {
  late BehaviorSubject<List<EntMatch>> _listSubject;
  static MatchService? _singleton;

  static void razSingleton() {
    _singleton = null;
    
  }

  static MatchService getInstance() {
    if (_singleton == null) {
      _singleton = MatchService._();
    }
    return (_singleton!);
  }

  MatchService._() {
    _initListenerCollectionMatchs();
  }

  void _initListenerCollectionMatchs() {
    CollectionReference refCollection =
        FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_MATCHS);

    _listSubject = BehaviorSubject<List<EntMatch>>();

    refCollection.snapshots().listen((l) async {
      List<EntMatch> lm = [];
      EntMatch m;

      l.docs.forEach((QueryDocumentSnapshot<Object?> o) async {
        if (o.exists) {
          m = EntMatch.fromFB(o);
          lm.add(m);
        }
      });
      lm.sort((a,b){
        return(a.date.compareTo(b.date));
      });
      _listSubject.add(lm);

    });
  }

  Stream<List<EntMatch>> getObsMatchs() {
    return (_listSubject.stream);
  }

  Future<void> majMatch(EntMatch m){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_MATCHS);

    Future<void> ret;
    if(m.id == "") ret=refCollection.add(m.toFB());
    else ret=refCollection.doc(m.id).set(m.toFB());

    return(ret);

  }

  Future<void> deleteMatch(EntMatch m){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_MATCHS);

    Future<void> ret;
    ret=refCollection.doc(m.id).delete();

    return(ret);

  }

}
