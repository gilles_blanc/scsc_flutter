import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';

import 'EntActivite.dart';

class EntEntrainement extends EntActivite{

  EntEntrainement() : super(Constantes.TYPE_ACTIVITE_NEXT_TRAINING){

    date = new DateTime(date.year, date.month, date.day, Constantes.TRAINING_HOUR);

  }

  static EntEntrainement fromFB(QueryDocumentSnapshot o) {
    EntEntrainement m = EntEntrainement();
    m.id = o.id;
    m.date = Util.getDateTimeFromFB(o.get("date"));
    return (m);
  }

  Object toFB() {
    Object o = {
      "date": date,
    };
    return (o);
  }

  EntEntrainement clone(){
    EntEntrainement clone = EntEntrainement();
    clone.date=date;
    clone.id=id;
    //clone.libelle=libelle;
    clone.typeActivite=typeActivite;

    return(clone);
  }

  String getSousTitre(){
    String ret="";

    return(ret);
  }


}
