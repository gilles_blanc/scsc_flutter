import 'package:flutter/material.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/UserService.dart';
import 'package:scsc_veteran/widget/Avatar/Avatar.dart';

class ElemJoueur extends StatefulWidget {
  late EntUser user;

  ElemJoueur(EntUser u) {
    user = u;
  }

  @override
  _ElemJoueurPageState createState() => _ElemJoueurPageState();
}

enum ActionMenu { telephone, sms, mail }

class _ElemJoueurPageState extends State<ElemJoueur> {
  int actionAvatar = Constantes.ACTION_AVATAR_NONE;
  int actionCagnote = Constantes.ACTION_CAGNOTTE_NONE;
  bool _me=false;

  @override
  void initState() {
    super.initState();

    UserService.getInstance().getCurrentUser().then((user) {

      if (user.role == Constantes.ROLE_ADMIN) {
        setState(() {
          _me=(user.id == widget.user.id);
          actionAvatar = Constantes.ACTION_AVATAR_ADMIN;
          actionCagnote = Constantes.ACTION_CAGNOTTE_ADMIN;
        });
      } else {
        if (user.id == widget.user.id) {
          setState(() {
            _me=true;
            actionAvatar = Constantes.ACTION_AVATAR_EDIT;
            actionCagnote = Constantes.ACTION_CAGNOTTE_VIEW;
          });
        }
      }
    });
  }

  _gestMenuAction(ActionMenu action) {
    String url = "";

    switch (action) {
      case ActionMenu.telephone:
        url = "tel:" + widget.user.tel;
        break;
      case ActionMenu.sms:
        url = "sms:" + widget.user.tel;
        break;
      case ActionMenu.mail:
        url = "mailto:" + widget.user.email;
    }
    //launch(url);
  }

  PopupMenuButton _getButtonMenu() {
    PopupMenuButton menu = PopupMenuButton<ActionMenu>(
        onSelected: (ActionMenu result) {
          _gestMenuAction(result);
        },
        icon: Icon(
          Icons.more_vert,
          color: Theme.of(context).colorScheme.primary,
        ),
        itemBuilder: (BuildContext context) {
          List<PopupMenuEntry<ActionMenu>> l = [];


          l.add(PopupMenuItem<ActionMenu>(
            value: ActionMenu.telephone,
            child: Row(
              children: <Widget>[
                Icon(Icons.phone, color: Theme.of(context).colorScheme.primary),
                Padding(
                    padding: EdgeInsets.only(left: 20, top: 2),
                    child: Text('Téléphoner'))
              ],
            ),
            enabled: widget.user.tel != "",
          ));

          l.add(PopupMenuItem<ActionMenu>(
            value: ActionMenu.sms,
            child: Row(
              children: <Widget>[
                Icon(Icons.message,
                    color: Theme.of(context).colorScheme.primary),
                Padding(
                    padding: EdgeInsets.only(left: 20, top: 2),
                    child: Text('Envoyer SMS'))
              ],
            ),
            enabled: widget.user.tel != "",
          ));

          l.add(PopupMenuItem<ActionMenu>(
            value: ActionMenu.mail,
            child: Row(
              children: <Widget>[
                Icon(Icons.mail, color: Theme.of(context).colorScheme.primary),
                Padding(
                    padding: EdgeInsets.only(left: 20, top: 2),
                    child: Text('Envoyer MAIL'))
              ],
            ),
            enabled: widget.user.email != "",
          ));

          return (l);
        });
    return (menu);
  }

  @override
  Widget build(BuildContext context) {
    Container c = Container(
      margin: EdgeInsets.only(top: 5, bottom: 5, right: 10, left: 10),
      padding: EdgeInsets.only(left: 5, bottom: 10),
      //color: Colors.orangeAccent,
      decoration: BoxDecoration(
        border: Border.all(
          color: Theme.of(context).colorScheme.primary,
          width: _me ? 2 : 2,
        ),
        borderRadius: BorderRadius.circular(12),
        color: _me ? Theme.of(context).colorScheme.secondary : Colors.white
      ),
      child: Row(
        children: <Widget>[
          getAvatar(widget.user),
          Flexible(
              child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width-130,
                    //color:Colors.red,
                    child :
                    Text(widget.user.nom + " " + widget.user.getPrenom(),
                          style: TextStyle(fontSize: 20))
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[_getButtonMenu()],
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                      width: MediaQuery.of(context).size.width-100,
                      //color:Colors.red,
                      child: widget.user.email != null
                          ? Text(widget.user.email, style: TextStyle(fontSize: 15))
                          : Text("", style: TextStyle(fontSize: 15))
                  )


                ],
              ),
              Row(
                children: <Widget>[
                  widget.user.tel != null
                      ? Text(widget.user.tel, style: TextStyle(fontSize: 15))
                      : Text("", style: TextStyle(fontSize: 15))
                ],
              )
            ],
          )),
        ],
      ),
    );
    return (c);
  }

  Widget getAvatar(EntUser u) {
    Padding p = Padding(
        padding: EdgeInsets.only(left: 5, right: 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(top: 10, bottom: 5),
                child: Avatar(u, actionAvatar)),
            u.flagPaiement ? Util.getThumb(u, context, actionCagnote) : Text("")
          ],
        ));
    return (p);
  }
}
