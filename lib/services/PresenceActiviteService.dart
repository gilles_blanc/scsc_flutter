import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'package:rxdart/rxdart.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/models/EntActivite.dart';
import 'package:scsc_veteran/models/EntPresenceActivite.dart';
import 'package:scsc_veteran/services/UserService.dart';

class PresenceActiviteService {
  Map<String,BehaviorSubject<List<EntPresenceActivite>>> _dicSubject = Map();
  static PresenceActiviteService? _singleton;

  static void razSingleton() {
    _singleton = null;
    
  }

  static PresenceActiviteService getInstance() {
    if (_singleton == null) {
      _singleton = PresenceActiviteService._();
    }
    return (_singleton!);
  }

  PresenceActiviteService._() {

  }

  Stream<List<EntPresenceActivite>> getObsListPrensenceActivites(EntActivite activite) {

    if(!_dicSubject.containsKey(activite.id)){
      BehaviorSubject<List<EntPresenceActivite>> sub = BehaviorSubject<List<EntPresenceActivite>>();
      _dicSubject[activite.id]=sub;
      _initListenerCollectionPresenceActivite(sub,activite);
    }

    return (_dicSubject[activite.id]!.stream);
  }
  
  void _initListenerCollectionPresenceActivite(BehaviorSubject<List<EntPresenceActivite>> sub,EntActivite activite) {
    CollectionReference refCollection =
        FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_PRENSENCE_ACTIVITES);

    refCollection
    .where("idActivite", isEqualTo: activite.id)
    .snapshots().listen((l) async {
      List<EntPresenceActivite> lp = [];
      EntPresenceActivite p;

      l.docs.forEach((QueryDocumentSnapshot<Object?> o) async {
        if (o.exists) {
          p = EntPresenceActivite.fromFB(o);
          lp.add(p);
        }
      });

      _consolidePresenceUser(lp,activite).then((lpRet){
        sub.add(lpRet);
      });


    });
  }

  Future<dynamic> _consolidePresenceUser(List<EntPresenceActivite> lp,EntActivite activite) async{

    Completer completer = new Completer<List<EntPresenceActivite>>();
    bool flagAdd;
    UserService.getInstance().getObsUsers().first.then((lu) {
      lu.forEach((user) {
        flagAdd=false;
        if(activite.isMatch() == true) flagAdd=user.flagMatch;
        if(activite.isTraining() == true) flagAdd=user.flagEntrainement;
        if(flagAdd == true){
          EntPresenceActivite? p = lp.firstWhereOrNull((ep) => ep.idUser == user.id);
          if(p == null){
            p=EntPresenceActivite();
            p.idActivite=activite.id;
            p.idUser=user.id;
            p.id="";
            p.codePresence=Constantes.CODE_PRESENCE_ACTIVITE_UNKNOWN;
            lp.add(p);
          }
        }
      });

      completer.complete(lp);
    });
    return (completer.future);
  }

  Future<void> majPresenceActivite(EntPresenceActivite p){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_PRENSENCE_ACTIVITES);

    Future<void> ret;
    if(p.id == "") ret=refCollection.add(p.toFB());
    else ret=refCollection.doc(p.id).set(p.toFB());

    return(ret);

  }

  Future<void> deletePresenceActivite(EntPresenceActivite p){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_PRENSENCE_ACTIVITES);

    Future<void> ret;
    ret=refCollection.doc(p.id).delete();

    return(ret);

  }

}
