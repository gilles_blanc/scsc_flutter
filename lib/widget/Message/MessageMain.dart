import 'package:flutter/material.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/MessageService.dart';
import 'package:scsc_veteran/services/UserService.dart';
import 'MessageChat.dart';

class MessageMain extends StatefulWidget {
  @override
  _MessageMainPageState createState() => _MessageMainPageState();
}

class _MessageMainPageState extends State<MessageMain> {
  EntUser? currentUser=null;
  TextEditingController? tc;
  @override
  void initState() {
    super.initState();
    //FlutterAppBadger.removeBadge();
    tc = TextEditingController();
    setState(() {
      tc!.text="edfredr rfde fr";
    });
    UserService.getInstance().getCurrentUser().then((u) {
      setState(() {
        currentUser = u;
      });
    });
  }

  void _nav(String route) {
    Navigator.of(context).pushNamed('$route');
  }

  Future<bool> razLastTs() async {
   // MessageService.getInstance().reInitMessage();

    //print ("RE INIT MESSAGE");
    return (true); // Empêche une double navigation (Back)
  }

  @override
  Widget build(BuildContext context) {
    if (currentUser == null) return Container();
    return new WillPopScope(
        onWillPop: razLastTs, //() async => false,
        child: new Scaffold(
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () {
              _nav("/CreateMessage");
            },
            backgroundColor: Theme.of(context).colorScheme.primary,
          ),
          appBar: AppBar(
            title: Text("Messages"),
          ),
          body:Container()//_getBody()


              ,
        ));
  }

 /* Widget _getBody(){


    Column c=Column(
      children: [

    Container(
    margin: const EdgeInsets.all(10.0),
    //padding: const EdgeInsets.all(5.0),
        height:MediaQuery.of(context).size.height-330,
    color:Colors.green,
    child: StreamBuilder(
            stream: MessageService.getInstance().getStreamMessage(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return (Container());
              } else {
                return MessageChat(snapshot.data, currentUser!);

              }
            })),
        Util.getInputMessage()
      ],
    );

    Container cont = Container(
        margin: const EdgeInsets.all(10.0),
        //padding: const EdgeInsets.all(5.0),
        height:MediaQuery.of(context).size.height-270,
         color:Colors.yellow,
        child: c);

    return(cont);
  }*/
}
