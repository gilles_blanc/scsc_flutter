import 'dart:async';

import 'package:flutter/material.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntEntrainement.dart';
import 'package:scsc_veteran/models/EntMatch.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/EntrainementService.dart';
import 'package:scsc_veteran/services/MatchService.dart';
import 'package:scsc_veteran/services/UserService.dart';
import 'package:scsc_veteran/widget/Activite/ElemActivite.dart';

class AdminEntrainement extends StatefulWidget {
  AdminMatch() {}

  @override
  _AdminEntrainementPageState createState() => _AdminEntrainementPageState();
}

class _AdminEntrainementPageState extends State<AdminEntrainement> {
  bool flagWait = false;
  StreamSubscription<List<EntEntrainement>>? _subEntrainements=null;
  List<EntEntrainement> _listeInterruptionEntrainement=[];

  @override
  void initState() {
    super.initState();
    _initInterruptionEntrainement();
  }

  @override
  void dispose() {
    Util.freeSub(_subEntrainements);

    super.dispose();
  }


  void _initInterruptionEntrainement() {
    _subEntrainements = EntrainementService.getInstance().getObsInterruption().listen((l) {
      setState(() {
        _listeInterruptionEntrainement = l;
      });
    });
  }

  void addInterruption() async {
    EntEntrainement newInterruption = EntEntrainement();
    _modifInterruption(newInterruption);
  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });


  }

  void _endWait() {
    setState(() {
      flagWait = false;
    });
  }

  _deleteInterruption(EntEntrainement e){
    Util.confirme(context,"Confirmez-vous la suppression de l'interruption " + e.getDateShort() +" ?").then((ret) {
      if (ret == true) {
        EntrainementService.getInstance().deleteInterruption(e);
      }
    });

  }

  _modifInterruption(EntEntrainement e) async{
    DateTime? d=await _modifDate(e.date);
    if(d != null){
      e.date = new DateTime(d.year, d.month, d.day, Constantes.TRAINING_HOUR);
      EntrainementService.getInstance().majInterruption(e);
    }
//
  }

  Future<DateTime?> _modifDate(DateTime initDate) async {
    // Imagine that this function is
    // more complex and slow.
    DateTime? d = await showDatePicker(
        context: context,
        initialDate: initDate,
        firstDate: DateTime.now()
        lastDate: DateTime(2030),
        builder: (BuildContext context, Widget? child) {
    return Theme(
    data: ThemeData.light(),
    child: child!,
    );
    },
    );
    return(d);
  }
  /*void _fillListMatch(List<EntMatch> lm){
    ElemActivite ea;

    _listeMatch=[];
    lm.forEach((element) {
      ea = ElemActivite(element,_currentUser,Constantes.MODE_ELEM_ACTIVITE_ADMIN);
      _listeMatch.add(ea);
      //print("AJOUT MATCH id=" +element.id +" -> equipe = " + element.idEquipe);
    });

  }

  */

  Widget getListInterruption(){
    List<Widget> lw =[];
    Row r;
    Padding p;
    _listeInterruptionEntrainement.forEach((element) {
      r=Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(element.getDateShort(), style: TextStyle(fontSize: 15)),
          Row(
            children: [
              IconButton(
                  icon: Icon(Icons.delete),
                  tooltip: 'Suppression',
                  onPressed: () {
                    _deleteInterruption(element);
                  }
              ),
              IconButton(
                  icon: Icon(Icons.edit),
                  tooltip: 'Modification',
                  onPressed: () {
                    _modifInterruption(element);
                  })
            ],
          )


      ],
      );
      p =Padding(
        padding: EdgeInsets.only(right:10,left:10),
        child: r,
      );
      lw.add(p);
    });

    ListView lv=ListView(
      children: lw,
    );
    return(lv);
  }

    @override
    Widget build(BuildContext context) {

      return Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: AppBar(
              title: Column(
                children: [
                  Text("Entrainements"),
                  Text("Interruption",style:TextStyle(fontSize: 10))
                ],
              ),

            /*  leading: Container(
                padding: EdgeInsets.only(left: 5, top: 5),
                child: Container(), // tO DO Avatar(currentUser, 1),
              ),*/
              actions: <Widget>[
                IconButton(
                    icon: Icon(Icons.add),
                    tooltip: 'Add',
                    onPressed: addInterruption)
              ],
            ),
          ),
          body: Container(
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.end,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    height: 10,
                    child: flagWait
                        ? LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Theme.of(context).colorScheme.primary))
                        : Container(),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height-115,

                    child:  getListInterruption(),
                  ),

                ],
              )));
    }


  }

