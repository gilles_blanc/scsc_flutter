import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:scsc_veteran/common/Constantes.dart';

abstract class EntActivite {
  late String id;
  late DateTime date;
  late int typeActivite;
  //late String libelle;


  EntActivite(int type){
    typeActivite=type;
    //libelle="";
    id="";
    DateTime t = DateTime.now();
    date =DateTime(t.year, t.month, t.day, 9,30);


  }

  String getDateLong(){
    String ret="le ";

    ret+=DateFormat("MMMMEEEEd", "fr_FR").format(date);
    ret+=" à ";
    ret+=DateFormat("Hm", "fr_FR").format(date);

    return(ret);
  }

  String getDateShort(){
    String ret="le ";

    ret+=DateFormat("MMMMEEEEd", "fr_FR").format(date);

    return(ret);
  }

  String getHour(){
    String ret="à ";

    ret+=DateFormat("Hm", "fr_FR").format(date);

    return(ret);
  }

  String getTitre(){
    String ret="";

    switch(typeActivite){
      case Constantes.TYPE_ACTIVITE_MATCH:
        ret="Match";
        break;
      case Constantes.TYPE_ACTIVITE_NEXT_MATCH:
        ret="Prochain match";
        break;
      case Constantes.TYPE_ACTIVITE_NEXT_TRAINING:
        ret="Prochain entrainement";
        break;
      default:
        ret="Activité";
        break;
    }
    return(ret);
  }

  String getSousTitre();

  bool isMatch(){
    bool ret=false;

    switch(typeActivite) {
      case Constantes.TYPE_ACTIVITE_MATCH:
      case Constantes.TYPE_ACTIVITE_NEXT_MATCH:
        ret=true;
        break;
    }
    return(ret);
  }

  bool isTraining(){
    bool ret=false;

    switch(typeActivite) {
      case Constantes.TYPE_ACTIVITE_NEXT_TRAINING:
        ret=true;
        break;
    }
    return(ret);
  }

}
