import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/UserService.dart';
import 'package:scsc_veteran/widget/Avatar/Avatar.dart';

class JoueurActivite extends StatefulWidget {
  bool me=false;
  late EntUser user;
  late String currentId;

  JoueurActivite(EntUser u,String currentId) {
    this.user=u;
    this.currentId=currentId;
    this.me=(user.id == this.currentId);
  }

  @override
  _JoueurActivitePageState createState() => _JoueurActivitePageState();
}

class _JoueurActivitePageState extends State<JoueurActivite> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(JoueurActivite ja) {

    super.didUpdateWidget(ja);

  }

  @override
  Widget build(BuildContext context) {
    if(widget.user == null) return(Container());
    Container c = Container(
      margin: EdgeInsets.only(top: 5, bottom: 5, right: 10, left: 10),
      padding: EdgeInsets.all(3),
      //color: Colors.orangeAccent,
      decoration: BoxDecoration(
        border: Border.all(
          color: Theme.of(context).colorScheme.primary,
          width: 1,//widget.me ? 5 : 2,
        ),
        borderRadius: BorderRadius.circular(12),
        color: widget.me ? Theme.of(context).colorScheme.secondary : Colors.white
      ),
      child: Row(
        children: <Widget>[
          Padding(
            padding:EdgeInsets.only(right:3),
            child:Avatar(widget.user)
          ),
          Text(widget.user.nom,style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          Text(" " + widget.user.getPrenomLimite(),style: TextStyle(fontSize: 20))
        ],
       ),
    );
    return (c);
  }

}
