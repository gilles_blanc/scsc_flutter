import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntCagnotte.dart';
import 'package:scsc_veteran/models/EntDepense.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/models/EntVersement.dart';
import 'package:scsc_veteran/services/DepenseService.dart';
import 'package:scsc_veteran/services/UserService.dart';
import 'package:scsc_veteran/services/VersementService.dart';


class Cagnotte extends StatefulWidget {
  @override
  _CagnottePageState createState() => _CagnottePageState();
}

class _CagnottePageState extends State<Cagnotte> with TickerProviderStateMixin {
  EntUser? currentUser = null;
  List<EntDepense> listeDepense = [];
  late StreamSubscription<List<EntDepense>> _subDepense;
  late StreamSubscription<int> _subTotalDepense;
  late StreamSubscription<int> _subTotalVersement;

  int totalDepense = 0;
  int totalVersement = 0;

  bool flagAdmin = false;

  TabController? _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);

    _initUser();

  }

  @override
  void dispose() {
    Util.freeSub(_subDepense);
   Util.freeSub(_subTotalDepense);
    Util.freeSub(_subTotalVersement);

    super.dispose();
  }

  void _initUser() {

    UserService.getInstance().getCurrentUser().then((u) {
      setState(() {
        currentUser = ModalRoute.of(context)!.settings.arguments as EntUser;
        if (u.role == Constantes.ROLE_ADMIN) flagAdmin = true;
        _initCagnotte();
      });
    });
  }

  void _initCagnotte() {
    _subDepense=DepenseService.getInstance().getObsDepense().listen((l) {
      setState(() {
        listeDepense = l;
      });
    });


    _subTotalDepense=DepenseService.getInstance().getObsTotal().listen((total) {
          setState(() {
            totalDepense = total;
        });
      });

    _subTotalVersement=VersementService.getInstance().getObsTotal().listen((total) {
      setState(() {
        totalVersement = total;
      });
    });


  }

  void deleteDepense(EntDepense d){
    Util.confirme(context,"Confirmez-vous la suppression de la dépense de  " + d.montant.toString() +"€ ?").then((ret) {
      if (ret == true) {
        DepenseService.getInstance().deleteDepense(d);
      }
    });
  }

  void modifDepense(EntDepense d){
    EntCagnotte c = new EntCagnotte();
    c.depense=d;
    Navigator.of(context).pushNamed('/EditCagnotte',arguments: c);
  }

  void deleteVersement(EntVersement v){
    Util.confirme(context,"Confirmez-vous la suppression du versement de  " + v.montant.toString() +"€ ?").then((ret) {
      if (ret == true) {
        VersementService.getInstance().deleteVersement(v);
      }
    });
  }

  void modifVersement(EntVersement v){
    EntCagnotte c = new EntCagnotte();
    c.versement=v;
    Navigator.of(context).pushNamed('/EditCagnotte',arguments: c);
  }
  
  void add(){
    switch(_tabController!.index){
      case 0: // Depenses
        modifDepense(EntDepense());
        break;
      case 1: // Versements
        EntVersement d=EntVersement();
        d.userId=currentUser!.id;
        d.montant=50;
        modifVersement(d);
        break;
    }
  }

  edit(EntCagnotte c) {
    Navigator.of(context).pushNamed('/CagnotteEdit', arguments: c).then((ret) {
      if (ret != null) {
        UserService.getInstance().getUserById(currentUser!.id).then((u) {
          setState(() {
            currentUser = u;
          });
        });
        _initCagnotte();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if((currentUser == null) || (_tabController == null)) return(Container());
    List<Widget> lwTitre=[Text("Cagnotte SCSC")];
    List<Widget> lwAction=[];

    if(flagAdmin == true){
      lwAction.add(IconButton(
          icon: Icon(Icons.add),
          tooltip: 'Ajout',
          onPressed: () {
            add();
          }));
      lwTitre.add(Text(currentUser!.getLibelle(),style:TextStyle(fontSize: 10)));
    }
    return Scaffold(
        appBar: AppBar(
          title:Container(
           // height: 50,
            child : Column(
              children: lwTitre,
            )
          ),

         actions: lwAction,
        ),
        body: _getBody());
  }

  Widget _getBody() {
    Widget w;

    w = _getCagnotte();

    Container cont = Container(
        margin: const EdgeInsets.all(10.0),
        padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
        ), //       <--- BoxDecoration here

        child: w);

    return (cont);
  }

  Widget _getCagnotte() {
    Column col = Column(
      children: <Widget>[
        _synthese(),
        TabBar(
          indicatorColor: Theme.of(context).colorScheme.primary,
          labelColor: Colors.grey,
          controller: _tabController,
          tabs: const <Widget>[
            Tab(
              text : "Dépenses",
              icon: Icon(Icons.wine_bar_sharp),
            ),
            Tab(
              text:"Mes versements",
              icon: Icon(Icons.volunteer_activism),
            ),
          ],
        ),
        _getTabView()
      ],
    );
    return (col);
  }

  Widget lineSynthese(String title,int montant,[Color col=Colors.black]){

    double sizeTitre = 20;
    if (montant < 0) col = Colors.red;

    Row line = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(title + " :", style: TextStyle(fontSize: sizeTitre)),
        Text(
          montant.toString() + "€",
          style: TextStyle(fontSize: sizeTitre, fontWeight: FontWeight.bold,color: col),
        )
      ],
    );
    return(line);
  }

  Widget _synthese() {

    Column col = Column(
      children: <Widget>[
        lineSynthese("Versements",totalVersement),
        lineSynthese("Dépenses",totalDepense),
        lineSynthese("Solde",totalVersement-totalDepense,Colors.green),
       ],
    );

    Container cont = Container(
        margin: const EdgeInsets.all(10.0),
        padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
            border: Border.all(),
            //color:Theme.of(context).colorScheme.secondary,
            borderRadius: BorderRadius.all(
                Radius.circular(10))), //       <--- BoxDecoration here
        child: col);

    return (cont);
  }

  Widget _getTabView(){
    TabBarView tbv=TabBarView(
      controller: _tabController,
      children: <Widget>[
        _getListeDepense(),
        _getListeVersement(),
      ],
    );

    Container cont = Container(
        margin: const EdgeInsets.all(10.0),
        padding: const EdgeInsets.all(5.0),
        height:MediaQuery.of(context).size.height-320,
        decoration: BoxDecoration(
            border: Border.all(),
            borderRadius: BorderRadius.all(
                Radius.circular(10))), //       <--- BoxDecoration here
        child: tbv);

    return(cont);
  }

  Widget _getListeDepense() {
    List<Widget> ld = [];
    listeDepense.forEach((element) {
      ld.add(_getElemDepense(element));
    });

    ListView list = ListView(
      children: ld,
    );


    return(list);
  }

  Widget _getElemDepense(EntDepense d) {
    String strDate = DateFormat.yMd("fr_FR").format(d.date);
    double sizeItemDepense = 15;

    List<Widget> lr = [];
    if(flagAdmin == true) {
      lr.add(IconButton(
          icon: Icon(Icons.delete),
          tooltip: 'Suppression',
          onPressed: () {
            deleteDepense(d);
          }));
      lr.add(IconButton(
          icon: Icon(Icons.edit),
          tooltip: 'Modification',
          onPressed: () {
            modifDepense(d);
          }));

    }
    lr.add(Text(d.libelle,
        style: TextStyle(
            fontSize: sizeItemDepense, fontStyle: FontStyle.italic)));

    List<Widget> lc = [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(strDate, style: TextStyle(fontSize: sizeItemDepense)),
          Text(d.montant.toString() + "€",
              style: TextStyle(fontSize: sizeItemDepense))
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: lr,
      )
    ];

    Column col = Column(
      children: lc,
    );

    Container cont = Container(
        margin: const EdgeInsets.only(bottom: 5),
        //padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
                width: 1, color: Theme.of(context).colorScheme.primary),
          ),
          //color:Colors.red,
          //borderRadius: BorderRadius.all(Radius.circular(10))
        ), //       <--- BoxDecoration here

        child: col);

    return (cont);
  }

  Widget _getElemVersement(EntVersement v) {
    String strDate = DateFormat.yMd("fr_FR").format(v.date);
    double sizeItemVersement = 20;

   
    List<Widget> lc=[Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(strDate, style: TextStyle(fontSize: sizeItemVersement)),
        Text(v.montant.toString() + "€",
            style: TextStyle(fontSize: sizeItemVersement))
      ],
    )];

    if(flagAdmin == true) {
      Row r = Row(
        children: [
          IconButton(
            icon: Icon(Icons.delete),
            tooltip: 'Suppression',
            onPressed: () {
              deleteVersement(v);
            }),
          IconButton(
              icon: Icon(Icons.edit),
              tooltip: 'Modification',
              onPressed: () {
                modifVersement(v);
              })
        ],
      );

      lc.add(r);

    }

    Column col = Column(
      children: lc,
    );

    Container cont = Container(
        margin: const EdgeInsets.only(bottom: 5),
        //padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
                width: 1, color: Theme.of(context).colorScheme.primary),
          ),
          //color:Colors.red,
          //borderRadius: BorderRadius.all(Radius.circular(10))
        ), //       <--- BoxDecoration here

        child: col);


    return (cont);
  }

  Widget _getListeVersement() {
    List<Widget> lv = [];
    int total=0;
    currentUser!.listeVersement.forEach((element) {
      lv.add(_getElemVersement(element));
      total+=element.montant;
    });

    Text tt=Text("Total = " + total.toString() + "€",
        style: TextStyle(fontSize: 15));

    ListView list = ListView(
      children: lv,
    );

    Container cont = Container(
        //margin: const EdgeInsets.all(10.0),
        //padding: const EdgeInsets.all(5.0),

        height:MediaQuery.of(context).size.height-360,
        //color:Colors.yellow,
         //       <--- BoxDecoration here
        child: list);

    Column col = Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [cont,tt]
    );
    return(col);
  }


}
