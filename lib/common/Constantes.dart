class Constantes {

  static const String KEY_GOOGLE_PUSH_SERVICE="AAAA9lp_4UM:APA91bFWKx1Qrv1bPWOpeT0qsGr7hyQvFsnjofbdb8gM8NcOehNiNDPTpkIZRzFboW60f7UVB8aLGTCDqNhG3GElqfBRPISiquJclpgCvYfkkEoHxAdnhuk8AdSEuYKy7n26V7FladQB";
  static const String SERVER_KEY_FCM_WEB="BIytFelTklVMS7bxi7rpNRzRDDyXLaLyscB1xcj1F6ZAQt5rE7UkZrG_sLvXsD23qWbu2cH-Ovytg7ACW0-IsgY";

  static const int MODE_ELEM_ACTIVITE_SAISIE=1;
  static const int MODE_ELEM_ACTIVITE_ADMIN=2;

  static const int TRAINING_HOUR=19;
  static const int NB_DAY_ANNIVERSAIRE=30;

  static String COLLECTION_FB_USERS = "users";
  static String COLLECTION_FB_EQUIPES = "equipes";
  static String COLLECTION_FB_MESSAGES = "messages";
  static String COLLECTION_FB_VERSEMENTS = "versements";
  static String COLLECTION_FB_DEPENSES = "depenses";
  static String COLLECTION_FB_MATCHS = "matchs";
  static String COLLECTION_FB_ENTRAINEMENTS = "entrainements";
  static String COLLECTION_FB_PRENSENCE_ACTIVITES = "presenceActivites";
  static String COLLECTION_FB_INTERRUPTIONS = "interruptionEntrainement";

  static const int  CODE_PRESENCE_ACTIVITE_YES=1;
  static const int  CODE_PRESENCE_ACTIVITE_NO=2;
  static const int  CODE_PRESENCE_ACTIVITE_UNKNOWN=3;

  static const int TYPE_ACTIVITE_NEXT_MATCH=1;
  static const int TYPE_ACTIVITE_MATCH=2;
  static const int TYPE_ACTIVITE_NEXT_TRAINING=3;

  static String SEP_TO = ",";

  static String TOKEN_NOTIFICATION_MES = "##mes##";
  static String TOKEN_NB_PRESENT = "##nb-present##";
  static String TOKEN_NOTIFICATION_TITLE = "##title##";
  static String TOKEN_NOTIFICATION_REGISTRATION_IDS = "##registrationIds##";
  static String TOKEN_LIBELLE_FROM = "##from##";
  static String TOKEN_URL_AVATAR = "##avatar##";

  static String BODY_NOTIFICATION_FCM = '{"notification": {"body": "' +
  TOKEN_NOTIFICATION_MES +
  '","image": "' +
  TOKEN_URL_AVATAR +
  '","title": "' +
  TOKEN_NOTIFICATION_TITLE +
  '","sound" : "default"},"priority": "high","data": {"click_action": "FLUTTER_NOTIFICATION_CLICK","id": "1","status": "done"},"registration_ids": [' +
      TOKEN_NOTIFICATION_REGISTRATION_IDS +
  ']}';

  static String BODY_NOTIFICATION_EMAIL = TOKEN_NOTIFICATION_MES;
  static String LINK_AVATAR_EMAIL = "<img style='border-bottom-left-radius:50' width='70' height='70' src=" + TOKEN_URL_AVATAR + ">";

  static String URI_FCM_SERVER = "fcm.googleapis.com";
  static String URI_FCM_PATH = "fcm/send";
  static String URI_EMAIL_SERVER = "https://api.mailgun.net/v3/scsc-veteran.fr/messages";

  static String API_KEY_MAIL = "key-b45c7a5f40097236738654c1fdbe747d";
  static String AUTH_BASIC ="Basic YXBpOmtleS1iNDVjN2E1ZjQwMDk3MjM2NzM4NjU0YzFmZGJlNzQ3ZA==";
  static String MAIL_SCSC = "scsc@scsc-veteran.fr";

  static String TOKEN_BODY_MESSAGE =
  "Nouveau message de " + TOKEN_LIBELLE_FROM ;
  static String TOKEN_BODY_PARTICIPATION_ENTRAINEMENT =
  "Participation de " + TOKEN_LIBELLE_FROM + " au prochain entrainement ";
  static String TOKEN_BODY_PARTICIPATION_MATCH = "Participation de " +
  TOKEN_LIBELLE_FROM + " au prochain match ";

  static String TITLE_MESSAGE = "Message";
  static String TITLE_ENTRAINEMENT = "Entrainement " +  TOKEN_NB_PRESENT;
  static String TITLE_MATCH = "Match "  + TOKEN_NB_PRESENT;

  static const int TYPE_NOTIFICATION_MESSAGE = 1;
  static const int TYPE_NOTIFICATION_PARTICIPATION_ENTRAINEMENT = 2;
  static const int TYPE_NOTIFICATION_PARTICIPATION_MATCH = 3;

  static const int OPTION_PJ_SMALL = 0;
  static const int OPTION_PJ_BIG = 1;
  static const int OPTION_PJ_ALL = 2;

  static const String TYPE_FILE_IMAGE = "image";

  static const String TYPE_MIME_JPEG = "image/jpeg";
  static const String TYPE_MIME_BMP = "image/bmp";
  static const String TYPE_MIME_GIF = "image/gif";
  static const String TYPE_MIME_PNG = "image/png";
  static const String TYPE_MIME_BIN = "application/octet-stream";

  static final double DLG_WIDTH = 300.0;
  static const double DLG_HEIGHT = 200.0;
  static final double DLG_BIG_HEIGHT = 300.0;

  static const int ACTION_AVATAR_NONE = 0;
  static const int ACTION_AVATAR_EDIT = 1;
  static const int ACTION_AVATAR_ADMIN = 2;

  static const int ACTION_CAGNOTTE_NONE = 0;
  static const int ACTION_CAGNOTTE_VIEW = 1;
  static const int ACTION_CAGNOTTE_ADMIN = 2;

  static const int TYPE_DLG_ERROR = 1;
  static const int TYPE_DLG_ALERT = 2;
  static const int TYPE_DLG_CONFIRME = 3;
  static const int TYPE_DLG_AVATAR = 4;
  static const double SIZE_ICON_DLG = 50.0;

  static const int NB_MAX_MESSAGE = 30;

  static final PERSIST_EMAIL = "PERSIST_EMAIL";
  static final PERSIST_PASSWORD = "PERSIST_PASSWORD";
  static final PERSIST_LAST_ID_MESSAGE = "PERSIST_LAST_ID_MESSAGE";
  static final PERSIST_REMEMBER_CREDENTIALS = "PERSIST_REMEMBER_CREDENTIALS";

  static final ROLE_ADMIN = "admin";
  static final ROLE_JOUEUR = "joueur";

  static const ACTIVITE_ENTRAINEMENT = "ENTRAINEMENT";

  static const TYPE_UPLOAD_AVATAR = 1;
  static const TYPE_UPLOAD_PJ = 2;
  static const TYPE_UPLOAD_PHOTO = 3;

  static final int MAX_SIZE_THUMB = 200;
  static final int MAX_SIZE_PHOTO = 2500;
  static final int MAX_SIZE_THUMB_PJ = 150;
  static final int MAX_SIZE_PHOTO_PJ = 2000;
  static final double MAX_SIZE_PHOTO_AVATAR = 200;

  static const int TYPE_OBJECT_IMAGE = 1;
  static const int TYPE_OBJECT_OTHER = 2;

  static const int SOURCE_OBJECT_FILE = 1;
  static const int SOURCE_OBJECT_CAMERA = 2;
  static const int SOURCE_OBJECT_ALL = 3;

  static const int MAX_SIZE_AVATAR = 300;
  //static const int MAX_SIZE_MESSAGE_PJ = 300;

  static const int STATE_UPLOAD_OBJECT_NO_STARTED = 0;
  static const int STATE_UPLOAD_OBJECT_STARTED = 1;
  static const int STATE_UPLOAD_OBJECT_FINISH_OK = 2;
  static const int STATE_UPLOAD_OBJECT_FINISH_KO = 3;


}