import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';

import 'EntActivite.dart';

class EntMatch extends EntActivite{
  late String idEquipe;
  late bool flagDomicile;

  EntMatch() : super(Constantes.TYPE_ACTIVITE_MATCH){
    flagDomicile=true;
    idEquipe="";
  }

  static EntMatch fromFB(QueryDocumentSnapshot o) {
    EntMatch m = EntMatch();
    m.id = o.id;
    m.idEquipe = o.get("idEquipe");
    m.date = Util.getDateTimeFromFB(o.get("date"));
    m.flagDomicile = o.get("flagDomicile");
     return (m);
  }

  Object toFB() {
    Object o = {
      "idEquipe": idEquipe,
      "flagDomicile": flagDomicile,
      "date": date,
    };
    return (o);
  }

  EntMatch clone(){
    EntMatch clone = EntMatch();
    clone.idEquipe=idEquipe;
    clone.flagDomicile=flagDomicile;
    clone.date=date;
    clone.id=id;
    //clone.libelle=libelle;
    clone.typeActivite=typeActivite;

    return(clone);
  }

  String getSousTitre(){
    String ret=" à domicile ";
    if(flagDomicile == false) ret= "à l'extérieur ";

    return(ret);
  }



}
