import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/models/StatusUpload.dart';
import 'package:scsc_veteran/services/BinaryService.dart';
import 'package:scsc_veteran/services/UserService.dart';

class Avatar extends StatefulWidget {
  late EntUser user;
  late int modeAction;

  Avatar(EntUser u, [int mode = 0]) {
    user = u;
    modeAction = mode;
  }

  @override
  _AvatarPageState createState() => _AvatarPageState();
}

class _AvatarPageState extends State<Avatar> {
  bool flagWait=false;

  @override
  void initState() {
    super.initState();
  }

  actionAvatar(BuildContext context) async {
    switch (widget.modeAction) {
      case Constantes.ACTION_AVATAR_NONE:
        break;
      case Constantes.ACTION_AVATAR_EDIT:

        Navigator.of(context)
            .pushNamed('/PhotoEdit')
            .then((buffer) {
          _editAvatar(buffer as Uint8List);
        });


        break;
      case Constantes.ACTION_AVATAR_ADMIN:
        Navigator.of(context).pushNamed('/AdminUser', arguments: widget.user);
        break;
    }
  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });


  }

  void _endWait() {
    setState(() {
      flagWait = false;
    });

  }

  _editAvatar(Uint8List buffer) async{
    if(buffer != null){
     _startWait();
      StatusUpload su=await BinaryService.getInstance().majAvatar(buffer,widget.user);
      if(su.status == false){
        _endWait();
        Util.error(context,su.reason + " " + su.strType);
      }
      else{
        widget.user.urlAvatar=su.url;
        UserService.getInstance().majUser(widget.user).then((ret){
          _endWait();
        }).catchError((e){
           Util.error(context,"erreur lors de la maj du user :" + e.toString());
           _endWait();
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.user == null) return(Container());

    List<Widget> lw = [];
    Widget w;
    CircleAvatar ca;

    if (widget.user.urlAvatar != "") {
          CachedNetworkImageProvider p =
          CachedNetworkImageProvider(widget.user.urlAvatar);

          ca = new CircleAvatar(
              foregroundColor: Colors.white,
              backgroundColor: Theme
                  .of(context)
                  .colorScheme
                  .secondary,
              backgroundImage: p);
    }
    else {
          ca = new CircleAvatar(
              foregroundColor: Colors.white,
              backgroundColor: Theme
                  .of(context)
                  .colorScheme
                  .secondary,
              child: (Text(widget.user.getInitiales())));
    }
    if ((widget.modeAction > 0) && (widget.user != null)) {
      w = new GestureDetector(
        onTap: () {
          actionAvatar(context);
        },
        child: ca);
    }
    else {
      w = ca;
    }

    lw.add(w);
    if(flagWait == true) {
      CircularProgressIndicator ci = CircularProgressIndicator(

          backgroundColor: Colors.white,
          valueColor: new AlwaysStoppedAnimation<Color>(
              Theme
                  .of(context)
                  .colorScheme
                  .primary));
      lw.add(ci);
    }
    Stack s = Stack(
      children: lw,
    );

    return (s);
  }
}
