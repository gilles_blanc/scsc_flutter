
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:scsc_veteran/common/Util.dart';

class EntVersement  {
  late DateTime date;
  late int montant;
  late String id;
  late String userId;

  EntVersement() {
    montant = 0;
    id="";
    userId="";
    date = DateTime.now();
  }

  EntVersement clone(){
    EntVersement v = EntVersement();
    v.montant=montant;
    v.id=id;
    v.userId=userId;
    v.date=date;

    return(v);

  }

  static EntVersement fromFB(QueryDocumentSnapshot o) {
    EntVersement v = EntVersement();
    v.id=o.id;
    v.date = Util.getDateTimeFromFB(o.get("date"));
    v.montant=o.get("montant");
    v.userId=o.get("userId");
    return (v);
  }

  Object toFB() {
    Object o = {
      "id": id,
      "montant": montant,
      "date": date,
      "userId":userId
    };
    return (o);
  }
}
