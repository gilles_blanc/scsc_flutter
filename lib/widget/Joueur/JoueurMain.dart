import 'package:flutter/material.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/UserService.dart';

import 'ElemJoueur.dart';

class JoueurMain extends StatefulWidget {
  @override
  _JoueurMainPageState createState() => _JoueurMainPageState();
}

class _JoueurMainPageState extends State<JoueurMain> {
  @override
  void initState() {
    super.initState();
  }

  void _nav(String route, EntUser user) {
    Navigator.of(context).pushNamed('$route', arguments: user);
  }

  @override
  Widget build(BuildContext context) {
    final EntUser user = ModalRoute.of(context)!.settings.arguments as EntUser;

    return Scaffold(
        appBar: AppBar(
          title: Text("Team SCSC"),
        ),
        body:StreamBuilder(
            stream: UserService.getInstance().getObsUsers(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                /*return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                        Theme.of(context).colorScheme.primary,
                      ),
                    ),
                  );*/
                return (Container());
              } else {

                List<EntUser> lu = snapshot.data as List<EntUser>;
                lu.sort((a,b){
                  return(a.nom.compareTo(b.nom));
                });

                return (ScrollablePositionedList.builder(
                  //itemScrollController: itemScrollController,
                    itemCount: lu.length,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return (ElemJoueur(lu[index]));
                    }));

              }
            }),

      /* FutureBuilder<List<EntUser>>(
            future: UserService.getInstance().getObsUsers().first,
            builder: (context, listeUser) {
              switch (listeUser.connectionState) {
                case ConnectionState.none:
                case ConnectionState.waiting:
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                        Theme.of(context).colorScheme.primary,
                      ),
                    ),
                  );
                default:
                  return (ScrollablePositionedList.builder(
                      //itemScrollController: itemScrollController,
                      itemCount: listeUser.data!.length,
                      itemBuilder: (BuildContext ctxt, int index) {
                        return (ElemJoueur(listeUser.data![index]));
                      }));
              }
            }
            )*/
    );
  }
}
