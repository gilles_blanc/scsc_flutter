import 'dart:async';
import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:scsc_veteran/models/EntEquipe.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/services/AuthService.dart';
import 'package:scsc_veteran/services/BinaryService.dart';
import 'package:scsc_veteran/services/DepenseService.dart';
import 'package:scsc_veteran/services/EntrainementService.dart';
import 'package:scsc_veteran/services/EquipeService.dart';
import 'package:scsc_veteran/services/MatchService.dart';
import 'package:scsc_veteran/services/MessageService.dart';
import 'package:scsc_veteran/services/PresenceActiviteService.dart';
import 'package:scsc_veteran/services/UserService.dart';
import 'package:scsc_veteran/services/VersementService.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_moment/simple_moment.dart';
import 'Constantes.dart';
import 'CustomDlg.dart';

class Util {


  static Future<String> choixSourceImage(
      BuildContext context, String titre) async {
    String ret;

    ret = await showDialog(
        context: context,
        builder: (context) => _choixSourceAvatarDialog(context, "titre"));
    return (ret);
  }

  static Future<void> setPersistString(String key, String value) async {
    String fullKey = key;
    if (kDebugMode) fullKey = fullKey + "_DEBUG";

    Completer completer = new Completer<void>();
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      sp.setString(fullKey, value);
      //print("SET key=" + fullKey + " = " + value);

      completer.complete();
    });

    return (completer.future);
  }

  static Future<void> setPersistInt(String key, int value) async {
    String fullKey = key;
    if (kDebugMode) fullKey = fullKey + "_DEBUG";

    Completer completer = new Completer<void>();

    SharedPreferences.getInstance().then((SharedPreferences sp) {
      sp.setInt(fullKey, value);
      completer.complete();
    });

    return (completer.future);
  }

  static Future<void> setPersistBool(String key, bool value) async {
    String fullKey = key;
    if (kDebugMode) fullKey = fullKey + "_DEBUG";

    Completer completer = new Completer<void>();

    SharedPreferences.getInstance().then((SharedPreferences sp) {
      sp.setBool(fullKey, value);
      completer.complete();
    });

    return (completer.future);
  }

  static Future<dynamic> getPersistString(String key) async {
    String fullKey = key;
    if (kDebugMode) fullKey = fullKey + "_DEBUG";

    Completer completer = new Completer<String>();

    SharedPreferences.getInstance().then((SharedPreferences sp) {
      String? val=sp.getString(fullKey);

      if(val == null) val="";
      //print("GET key=" + fullKey + " = " + val);
      completer.complete(val);
    });

    return (completer.future);
  }

  static Future<dynamic> getPersistInt(String key) async {
    String fullKey = key;
    if (kDebugMode) fullKey = fullKey + "_DEBUG";

    Completer completer = new Completer<int>();

    SharedPreferences.getInstance().then((SharedPreferences sp) {
      int? val=sp.getInt(fullKey);
      if(val == null) val=0;
      completer.complete(val);
    });

    return (completer.future);
  }

  static Future<dynamic> getPersistBool(String key) async {
    String fullKey = key;
    if (kDebugMode) fullKey = fullKey + "_DEBUG";

    Completer completer = new Completer();

    SharedPreferences.getInstance().then((SharedPreferences sp) {
      bool? val=sp.getBool(fullKey);
      if(val == null) val=false;
      completer.complete(val);
    });

    return (completer.future);
  }

  static DateTime getDateTimeFromFB(Timestamp ts){

    DateTime d = ts.toDate();
    return(d);
  }

  static Future<bool> error(BuildContext context, String mes,
      [String title = "Erreur"]) async {
    bool ret;

    ret = await showDialog(
        context: context,
        builder: (context) =>
            _baseDialog(context, mes, title, Constantes.TYPE_DLG_ERROR));
    return (ret);
  }

  static Future<bool> alert(BuildContext context, String mes,
      [String title = "Information",double height=Constantes.DLG_HEIGHT]) async {
    bool ret;

    ret = await showDialog(
        context: context,
        builder: (context) =>
            _baseDialog(context, mes, title, Constantes.TYPE_DLG_ALERT,height));
    return (ret);
  }

  static Future<bool> custom(CustomDlg c) async {
    bool ret;

    ret = await showDialog(
        context: c.context, builder: (context) => _baseDialogCustom(c));
    return (ret);
  }

  static Future<bool> confirme(BuildContext context, String mes,
      [String title = "Confirmation"]) async {
    bool ret;

    ret = await showDialog(
      context: context,
      builder: (context) =>
          _baseDialog(context, mes, title, Constantes.TYPE_DLG_CONFIRME),
    );
    return (ret);
  }

  static Dialog _baseDialog(
      BuildContext context, String mes, String title, int type,[double height=Constantes.DLG_HEIGHT]) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        height: height,
        width: Constantes.DLG_WIDTH,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _header(context, title),
            _body(context, mes, type),
            _footer(context, type)
          ],
        ),
      ),
    );
  }

  static Dialog _baseDialogCustom(CustomDlg c) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        height: Constantes.DLG_HEIGHT,
        width: Constantes.DLG_WIDTH,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _headerCustom(c),
            _bodyCustom(c),
            _footerCustom(c)
          ],
        ),
      ),
    );
  }

  static Dialog _choixSourceAvatarDialog(BuildContext context, String titre) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        height: Constantes.DLG_HEIGHT,
        width: Constantes.DLG_WIDTH,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _header(context, titre),
            _bodyChoixAvatar(context),
            _footer(context, Constantes.TYPE_DLG_AVATAR)
          ],
        ),
      ),
    );
  }

  static Widget _header(BuildContext context, String title) {
    Container c = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(left: 10, top: 5),
              child: Text(title,
                  style:
                      TextStyle(color: Theme.of(context).colorScheme.primary)))
        ],
      ),
    );

    return (c);
  }

  static Widget _body(BuildContext context, String mes, int type) {
    late Icon ico;
    switch (type) {
      case Constantes.TYPE_DLG_ERROR:
        ico = Icon(
          Icons.error,
          color: Colors.red,
          size: Constantes.SIZE_ICON_DLG,
        );
        break;
      case Constantes.TYPE_DLG_ALERT:
        ico = Icon(Icons.info,
            size: Constantes.SIZE_ICON_DLG,
            color: Theme.of(context).colorScheme.secondary);
        break;
      case Constantes.TYPE_DLG_CONFIRME:
        ico = Icon(Icons.message,
            size: Constantes.SIZE_ICON_DLG,
            color: Theme.of(context).colorScheme.secondary);
        break;
    }
    Container c = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: ico,
          ),
          Flexible(
              child: Padding(padding: EdgeInsets.all(10), child: Text(mes)))
        ],
      ),
    );

    return (c);
  }

  static Widget _footer(BuildContext context, int type) {
    FlatButton butOK = FlatButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: Text('Ok'));

    FlatButton butCANCEL = FlatButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: Text('Annuler'));

    FlatButton butOui = FlatButton(
        onPressed: () {
          Navigator.of(context).pop(true);
        },
        child: Text('Oui'));

    FlatButton butNon = FlatButton(
        onPressed: () {
          Navigator.of(context).pop(false);
        },
        child: Text('Non'));
    List<Widget> listButton = [];

    switch (type) {
      case Constantes.TYPE_DLG_ERROR:
        listButton.add(butOK);
        break;
      case Constantes.TYPE_DLG_AVATAR:
        listButton.add(butCANCEL);
        break;
      case Constantes.TYPE_DLG_ALERT:
        listButton.add(butOK);
        break;
      case Constantes.TYPE_DLG_CONFIRME:
        listButton.add(butOui);
        listButton.add(butNon);
        break;
    }

    Container c = Container(
      child:
          Row(mainAxisAlignment: MainAxisAlignment.end, children: listButton),
    );

    return (c);
  }

  static Widget _headerCustom(CustomDlg c) {
    Container cont = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(left: 10, top: 5), child: Text(c.header))
        ],
      ),
    );

    return (cont);
  }

  static Widget _bodyCustom(CustomDlg c) {
    Container cont = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: c.icon,
          ),
          Flexible(
              child: Padding(padding: EdgeInsets.all(10), child: Text(c.mes)))
        ],
      ),
    );

    return (cont);
  }

  static Widget _footerCustom(CustomDlg c) {
    Container cont = Container(
      child:
          Row(mainAxisAlignment: MainAxisAlignment.end, children: c.listButton),
    );

    return (cont);
  }

  static Widget _bodyChoixAvatar(BuildContext context) {
    FlatButton butPhoto = FlatButton(
        onPressed: () {
          Navigator.of(context).pop("photo");
        },
        child: Text('Photo'));

    FlatButton butGallerie = FlatButton(
        onPressed: () {
          Navigator.of(context).pop("gallerie");
        },
        child: Text('Gallerie'));

    List<Widget> listButton = [];
    listButton.add(butPhoto);
    listButton.add(butGallerie);

    Container c = Container(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: listButton),
    );

    return (c);
  }

  static Dialog choixTypeAdmin(BuildContext context){
    return Dialog(

      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        height: Constantes.DLG_BIG_HEIGHT,
        width: Constantes.DLG_WIDTH,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _header(context,"Type administration"),
            _bodyChoixTypeAdmin(context),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
                child: Text('Annuler'))

          ],
        ),
      ),
    );
  }

  static Widget _bodyChoixTypeAdmin(BuildContext context){
    Column c=Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary : Colors.green), // set the background color

            onPressed:  (){
              Navigator.of(context).pop("match");
            },
            child: Text("Match")
        ),
        ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary : Colors.green), // set the background color

            onPressed:  (){
              Navigator.of(context).pop("equipe");
            },
            child: Text("Equipe")
        ),
        ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary : Colors.green), // set the background color

            onPressed:  (){
              Navigator.of(context).pop("entrainement");
            },
            child: Text("Entrainement")
        ),
      ],
    );

    return(c);

  }

  static Widget getThumb(EntUser? u, BuildContext context, int action) {
    if (u == null) return Text("");
    int max = UserService.getInstance().maxVersement;
    int diff = (u.totalVersement - max);
    Color c;
    IconData i = Icons.thumb_down;

    if (max > 0) {
      if (u.totalVersement == max) {
        c = Colors.green;
        i = Icons.thumb_up;
      } else {
        if (u.totalVersement >= (max - 20)) {
          c = Colors.orange;
        } else {
          c = Colors.red;
        }
      }
    } else {
      if (u.role == Constantes.ROLE_ADMIN) {
        c = Colors.black;
        i = Icons.monetization_on;
        diff = 0;
      } else {
        return (Text(""));
      }
    }

    Icon thumb = Icon(
      i,
      color: c,
      size: 24.0,
    );

    Column col = Column(
      children: <Widget>[
        thumb,
        diff != 0 ? Text(diff.toString() + "€") : Text(u.totalVersement.toString()+ "€")
      ],
    );

    GestureDetector gd = new GestureDetector(
        onTap: () {
          if (action != Constantes.ACTION_CAGNOTTE_NONE) {
            Navigator.of(context)
                .pushNamed('/Cagnotte', arguments: u)
                .then((ret) {});
          }
          //_detailVersement(context,u);
        },
        child: col);
    return (gd);
  }

  static void _detailVersement(BuildContext context, EntUser u) {
    String mes = "Total versements :" +
        UserService.getInstance().maxVersement.toString() +
        "€";
    mes = mes + "\n" + "Mes versements : " + u.totalVersement.toString() + "€";
    int diff = u.totalVersement - UserService.getInstance().maxVersement;
    if (diff != 0) {
      mes = mes + "\n" + "Débiteur de : " + diff.toString() + "€";
    }

    List<TextButton> l = [];
    TextButton butOK = TextButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: Text('Ok'));
    l.add(butOK);

    CustomDlg c = CustomDlg(context, mes, "Mes versements",
        Icon(Icons.euro_symbol, color: Colors.blue, size: 25), l);

    custom(c);
  }

  static Widget getIconBadge(int nb, Icon ico, Color colorButton,
      Color colorBage, VoidCallback handler, double iconSize, bool flagPresence,
      [bool flagAlway = false]) {
    Widget w;
    Color colorText = Colors.black;
    if (flagPresence == true) {
      colorBage = Colors.deepPurpleAccent;
      colorText = Colors.white;
    }
    IconButton b = IconButton(
        color: colorButton, icon: ico, iconSize: iconSize, onPressed: handler);
    // TO DO
    if ((nb > 0) || (flagAlway == true)) {
      w = Badge(
          badgeContent: Text(
            nb.toString(),
            style: TextStyle(fontWeight: FontWeight.bold, color: colorText),
          ),
          badgeColor: colorBage, //Theme.of(context).colorScheme.secondary,
          position: BadgePosition(end: -2, top: -2),
          child: b);
    } else {
      w = b;
    }
    return (w);
  }

  static Widget getIconBadgeDetail(int nb, Icon ico, Color colorButton,
      Color colorBage, VoidCallback handler, double iconSize, bool flagPresence,
      [bool flagAlway = false]) {
    Widget w;
    Color colorText = Colors.black;
    if (flagPresence == true) {
      colorBage = Colors.deepPurpleAccent;
      colorText = Colors.white;
    }
    IconButton b = IconButton(
        color: colorButton, icon: ico, iconSize: iconSize, onPressed: handler);
    // TO DO
    if ((nb > 0) || (flagAlway == true)) {
      w = Badge(
          badgeContent: Text(
            nb.toString(),
            style: TextStyle(fontWeight: FontWeight.bold, color: colorText),
          ),
          badgeColor: colorBage, //Theme.of(context).colorScheme.secondary,
          position: BadgePosition(end: -7, top: -5),
          child: b);
    } else {
      w = b;
    }
    return (w);
  }

  static void freeSub(StreamSubscription<dynamic>? sub){
    if (sub != null) {
      sub.cancel();
    }
  }

  static razCache() {
    AuthService.razSingleton();
    BinaryService.razSingleton();
    DepenseService.razSingleton();
    EntrainementService.razSingleton();
    EquipeService.razSingleton();
    MatchService.razSingleton();
    MessageService.razSingleton();
    PresenceActiviteService.razSingleton();
    UserService.razSingleton();
    VersementService.razSingleton();

  }

  static String getElapse(DateTime d) {
    Moment.setLocaleGlobally(new LocaleFr());
    var moment = new Moment.now();

    String ret = moment.from(d);
    ret = ret.replaceAll("en", "Dans");

    return (ret);
  }

  static Widget getLogoEquipe(EntEquipe e) {
    Icon icoDef = Icon(Icons.info,
        size: Constantes.SIZE_ICON_DLG,
        color: Colors.red);

    CachedNetworkImage i=CachedNetworkImage(
      imageUrl: e.logo,
      placeholder: (context, url) =>
      const CircularProgressIndicator(),
      errorWidget: (context, url, error) => icoDef ,
    );
    return SizedBox(
      width: 50.0,
      height: 50.0,
      child: Center(child: i),
    );
  }

  static Widget getInputMessage(){
    Widget w=new TextField(
      minLines: 1,
      maxLines: 5,

      decoration: new InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 5.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 5.0),
        ),
        hintText: 'Mobile Number',
      ),
    );
    return(w);
  }
}
