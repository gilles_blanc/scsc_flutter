import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntEquipe.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/models/StatusUpload.dart';
import 'package:scsc_veteran/services/BinaryService.dart';
import 'package:scsc_veteran/services/EquipeService.dart';
import 'package:scsc_veteran/services/UserService.dart';

class EditEquipe extends StatefulWidget {
  EditEquipe() {}

  @override
  _EditEquipePageState createState() => _EditEquipePageState();
}

class _EditEquipePageState extends State<EditEquipe> {
  bool flagWait = false;
  final ctrlNomEquipe = TextEditingController();
  EntEquipe _currentEquipe = new EntEquipe();
  Uint8List? buffer=null;

  @override
  void initState() {
    super.initState();
    Future.delayed(
      const Duration(milliseconds: 50),
          () {
            _initEquipe();
      },
    );

  }

  void _initEquipe(){

      setState(() {
        _currentEquipe = (ModalRoute.of(context)!.settings.arguments as EntEquipe).clone();
        ctrlNomEquipe.text = _currentEquipe.libelle;

      });

  }

  void _startWait() {
    setState(() {
      flagWait = true;
    });


  }

  void _endWait() {
    setState(() {
      flagWait = false;
    });
  }

  void majEquipe() async {
    _startWait();
    _currentEquipe.libelle=ctrlNomEquipe.text;
    EquipeService.getInstance().majEquipe(_currentEquipe).then((ret){
      if(buffer != null){
        _majLogo();
      }
      _endWait();
      Navigator.of(context).pop();
    });



  }

  void _editLogo(){
    Navigator.of(context)
        .pushNamed('/PhotoEdit')
        .then((buf) {
          setState(() {
            buffer = buf as Uint8List;
          });


    });
  }

  _majLogo() async{

      _startWait();
      StatusUpload su=await BinaryService.getInstance().majLogoEquipe(buffer!,_currentEquipe);
      if(su.status == false){
        _endWait();
        Util.error(context,su.reason + " " + su.strType);
      }
      else{
        _currentEquipe.logo=su.url;
        EquipeService.getInstance().majEquipe(_currentEquipe).then((ret){
          _endWait();
        }).catchError((e){
          Util.error(context,"erreur lors de la maj du logo :" + e.toString());
          _endWait();
        });
      }

  }

    @override
    Widget build(BuildContext context) {

      return Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: AppBar(
              title: Text("Add/Modif Equipe"),
              /*leading: Container(
                padding: EdgeInsets.only(left: 5, top: 5),
                child: Container(), // tO DO Avatar(currentUser, 1),
              ),*/

            ),
          ),
          body: Container(
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.end,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    height: 10,
                    child: flagWait
                        ? LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Theme.of(context).colorScheme.primary))
                        : Container(),
                  ),
                  _input("Libellé",ctrlNomEquipe),
                  _getLogo(),
                  Padding(
                      padding: EdgeInsets.only(right: 10,left:10.0),
                      child: ElevatedButton(
                        child: Text("Valider"),
                        onPressed: isValide() ? majEquipe : null,

                      ))
                ],
              )));
    }

  bool isValide(){
    bool ret=true;
    //if(ctrlNomEquipe.text == "") ret=false;

    return(ret);
  }

  Widget _getLogo(){
    Widget w=Container();

    if(_currentEquipe != null){
      Widget l;
      if(buffer != null){
        Image img=Image(
            image: MemoryImage(buffer!));
        l=SizedBox(
          width: 50.0,
          height: 50.0,
          child: Center(child: img),
        );
      }
      else{
        l=Util.getLogoEquipe(_currentEquipe);
      }

      Widget p=Padding(
          padding: EdgeInsets.only(right:10,top:10),
          child : l
      );

      w = GestureDetector(
          onTap: _editLogo,
          child: p);
    }



    return(w);
  }

  Widget _input(String hint, TextEditingController controller) {

    Widget w=TextField(
              //maxLength: 10,
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                //hintText: hint,
                  labelText: hint),
              style: TextStyle(
                fontSize: 15,
              ),
              controller: controller,


            );

    Padding p=Padding(
        padding: EdgeInsets.only(left: 14, right: 14),
        child: w
    );
        return(p);
  }

}

