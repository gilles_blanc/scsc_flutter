import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:scsc_veteran/widget/Activite/DetailActivite.dart';
import 'package:scsc_veteran/widget/Admin/AdminEntrainement.dart';
import 'package:scsc_veteran/widget/Admin/EditEquipe.dart';
import 'package:scsc_veteran/widget/Admin/EditMatch.dart';
import 'package:scsc_veteran/widget/Admin/AdminEquipe.dart';
import 'package:scsc_veteran/widget/Admin/AdminMatch.dart';
import 'package:scsc_veteran/widget/Admin/AdminUser.dart';
import 'package:scsc_veteran/widget/Cagnotte/Cagnotte.dart';
import 'package:scsc_veteran/widget/Cagnotte/EditCagnotte.dart';
import 'package:scsc_veteran/widget/Match/AllMatch.dart';
import 'package:scsc_veteran/widget/Message/Message.dart';
import 'package:scsc_veteran/widget/Photo/PhotoEdit.dart';
import 'package:scsc_veteran/widget/Joueur/JoueurMain.dart';
import 'package:scsc_veteran/widget/Message/MessageCreate.dart';
import 'package:scsc_veteran/widget/Message/MessageMain.dart';
import 'package:scsc_veteran/widget/Param/Parametre.dart';

import 'widget/Home.dart';
import 'widget/Login.dart';
import 'widget/Mire.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp() {}

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MyApp> {
  final ThemeData theme = ThemeData();
  @override
  void initState() {
    super.initState();



  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'SCSC Vétéran',
        debugShowCheckedModeBanner: false,
        theme: theme.copyWith(
          colorScheme: theme.colorScheme
              .copyWith(primary: Colors.green, secondary: Colors.green[200]),
        ),
        routes: {
          '/': (context) => Mire(),
          '/Login': (context) => Login(),
          '/Home': (context) => Home(),
          '/AdminUser': (context) => AdminUser(),
          '/AdminMatch': (context) => AdminMatch(),
          '/AdminEquipe': (context) => AdminEquipe(),
          '/AddMatch': (context) => EditMatch(),
          '/AdminEntrainement': (context) => AdminEntrainement(),
          '/EditMatch': (context) => EditMatch(),
          '/AddEquipe': (context) => EditEquipe(),
          '/EditEquipe': (context) => EditEquipe(),
          '/Joueur': (context) => JoueurMain(),
          '/Message': (context) => Message(),
          '/CreateMessage': (context) => MessageCreate(),
          '/Parametre': (context) => Parametre(),
          '/DetailActivite': (context) => DetailActivite(),
          '/PhotoEdit': (context) => PhotoEdit(),
          '/Cagnotte': (context) => Cagnotte(),
          '/EditCagnotte': (context) => EditCagnotte(),
          '/AllMatch': (context) => AllMatch(),
        },
        initialRoute: '/');
  }


}
