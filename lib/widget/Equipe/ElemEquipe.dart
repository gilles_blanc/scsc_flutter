import 'package:flutter/material.dart';
import 'package:scsc_veteran/common/Util.dart';
import 'package:scsc_veteran/models/EntEquipe.dart';
import 'package:scsc_veteran/services/EquipeService.dart';


class ElemEquipe extends StatefulWidget {
  late EntEquipe equipe;

  ElemEquipe(EntEquipe e) {
    equipe=e;

  }

  @override
  _ElemEquipePageState createState() => _ElemEquipePageState();
}

class _ElemEquipePageState extends State<ElemEquipe> {

  @override
  void initState() {
    super.initState();

    //_init();

  }

  @override
  void didUpdateWidget(ElemEquipe ee) {

    super.didUpdateWidget(ee);
    //_init();

  }

  /*_init(){
    _initEquipe();

  }*/

  @override
  void dispose() {
    super.dispose();
  }

  /*_initEquipe() {
    setState(() {
    });
  }*/

  void _deleteEquipe(){
    Util.confirme(context,"Confirmez-vous la suppression de l'équipe " +  widget.equipe.libelle +" ?").then((ret){
      if(ret == true) {
        EquipeService.getInstance().deleteEquipe(widget.equipe);
      }
    });
  }

  void _editEquipe(){
    Navigator.of(context).pushNamed('/EditEquipe',arguments: widget.equipe);

  }


  @override
  Widget build(BuildContext context) {
    Widget w = Container(
      margin: EdgeInsets.only(top: 5, bottom: 5, right: 10, left: 10),
      padding: EdgeInsets.only(left: 5, bottom: 5),
      width: MediaQuery.of(context).size.width - 100,
      decoration: BoxDecoration(
        border: Border.all(
          color: Theme.of(context).colorScheme.primary,
          width: 2,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      child:Column(
          //crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              _getHeader(),
              _getBody(),
            ],
          )
    );


    return (w);
  }

  Widget _getLogo(){
    Widget w=Container();

    Widget l=Util.getLogoEquipe(widget.equipe);
    w=Padding(
        padding: EdgeInsets.only(right:10,top:10),
        child : l
    );
    return(w);
  }

  Widget _getHeader() {

    Row r = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        _getLogo(),
        Container(

            width: MediaQuery.of(context).size.width - 100,
          child: Text(widget.equipe.libelle, style: TextStyle(fontSize: 20))
        )

      ],
    );

    return(r);

    }

  Widget _getBody() {

    Widget w=Container();

      Row r=Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          IconButton(
              icon: Icon(Icons.delete),
              tooltip: 'Suppression',
              onPressed: _deleteEquipe),
          IconButton(
              icon: Icon(Icons.edit),
              tooltip: 'Modification',
              onPressed: _editEquipe)
        ],
      );

     return(r);
   }
}
