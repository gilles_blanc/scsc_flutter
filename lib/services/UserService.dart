import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/src/iterable_extensions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/models/EntAnniversaire.dart';
import 'package:scsc_veteran/models/EntEquipe.dart';
import 'package:scsc_veteran/models/EntUser.dart';
import 'package:scsc_veteran/models/EntVersement.dart';
import 'package:scsc_veteran/services/AuthService.dart';

import 'VersementService.dart';

class UserService {
  late BehaviorSubject<List<EntUser>> _listSubject;
  late BehaviorSubject<EntUser> _currentUserSubject;
  late BehaviorSubject<List<EntAnniversaire>> _listAnniversaireSubject;
  static StreamSubscription<List<EntVersement>>? _subListeVersement=null;
  late List<EntUser> _listeUser;
  static UserService? _singleton;

  int maxVersement=0;

  static void razSingleton() {
    _singleton = null;
    
  }

  static UserService getInstance() {
    if (_singleton == null) {
      _singleton = UserService._();
    }
    return (_singleton!);
  }

  UserService._() {
    _initListenerCollectionUsers();
  }

  void _initListenerCollectionUsers() {
    // **************
    migration();
    // **************

    CollectionReference refCollection =
        FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_USERS);

    _listSubject = BehaviorSubject<List<EntUser>>();
    _currentUserSubject = BehaviorSubject<EntUser>();
    _listAnniversaireSubject =BehaviorSubject<List<EntAnniversaire>>();

    EntUser u;


    refCollection.snapshots().listen((l) async {
      _listeUser = [];
      l.docs.forEach((QueryDocumentSnapshot<Object?> o) async {
        if (o.exists) {
          u = EntUser.fromFB(o);
          _listeUser.add(u);
        }
      });
      _listSubject.add(_listeUser);
      _consolideCurrentUser();
      _consolideVersement();
      _consolideAnniversaire();
    });
  }

  void _consolideVersement(){
    if(_subListeVersement != null){
      _subListeVersement!.cancel();
    }
    _subListeVersement=VersementService.getInstance().getObsVersements().listen((lVersement) {
      EntUser u;
      maxVersement=0;
      _listeUser.forEach((element) {
        element.listeVersement=[];
        element.totalVersement=0;
      });
      lVersement.forEach((v) {
        u=_listeUser.singleWhere((element) => element.id == v.userId);
        if(u != null) u.addVersement(v);
        if(u.totalVersement > maxVersement) maxVersement=u.totalVersement;
      });
      _listSubject.add(_listeUser);
      _consolideCurrentUser();

    });
  }

  void _consolideCurrentUser(){
    getCurrentUser().then((u) {
      _currentUserSubject.add(u);
    });
  }

  Stream<List<EntUser>> getObsUsers() {
    return (_listSubject.stream);
  }

  Stream<List<EntAnniversaire>> getObsAnniversaires() {
    return (_listAnniversaireSubject.stream);
  }

  Stream<EntUser> getObsCurrentUser() {
    return (_currentUserSubject.stream);
  }

  Future<EntUser> getCurrentUser() async {
    Completer<EntUser> completer = new Completer<EntUser>();


    User? uFB=AuthService.getInstance().getCurrentUserFB();
    if(uFB == null){
      completer.complete(null);
    }
    else{
      getUserById(uFB.uid).then((u) async {
        completer.complete(u);
      });
    }
    return (completer.future);
  }

  Future<EntUser?> getUserById(String id) async {
    Completer<EntUser?> completer = new Completer<EntUser?>();

    _listSubject.stream.first.then((liste) {
      EntUser? u=null;
      if(liste.isNotEmpty) {
        u = liste.firstWhereOrNull((element) => element.id == id);
      }
      completer.complete(u);
    });

    return (completer.future);
  }

  Future<void> majUser(EntUser u){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_USERS);

    Future<void> ret;
    if(u.id == "") ret=refCollection.add(u.toFB());
    else ret=refCollection.doc(u.id).set(u.toFB());

    return(ret);

  }

  Future<void> majTokenFCM(String? token) async{
    if(token == null) {
      print("TOKEN NULL !!");

      return;
    }
      EntUser u= await getCurrentUser();

      CollectionReference refCollection =
      FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_USERS);

      bool flagMaj=false;
      if(kIsWeb){
        if(u.tokenFCMWeb != token){
          flagMaj=true;
          u.tokenFCMWeb=token;
        }
        else print("TOKEN IDENTIQUE :" + token);
      }
      else{
        if(u.tokenFCMMob != token){
          flagMaj=true;
          u.tokenFCMMob=token;
        }
        else print("TOKEN IDENTIQUE :" + token);
      }
      if(flagMaj == false) return;

      print("MAJ TOKEN : "+ token);
      Future<void> ret=refCollection.doc(u.id).set(u.toFB());

      return(ret);
    }



  void _consolideAnniversaire(){

    DateTime today = DateTime.now();
    DateTime limite1 = DateTime.now().add(new Duration(days: Constantes.NB_DAY_ANNIVERSAIRE));
    DateTime? limite2 = null;
    DateTime dateAnniv;
    List<EntAnniversaire> lRet = [];
    bool flagAdd;

    if (limite1.year > today.year) {
      limite2 = new DateTime(today.year, 12, 31);
    }

    for (EntUser u in _listeUser) {
      flagAdd = false;
      do {
        if (limite2 != null) {
          dateAnniv = new DateTime(
              today.year, u.dateNaissance.month, u.dateNaissance.day);
          if ((dateAnniv.compareTo(today) >= 0) &&
              (dateAnniv.compareTo(limite2) <= 0)) {
            flagAdd = true;
            break;
          }
          dateAnniv = new DateTime(
              limite1.year, u.dateNaissance.month, u.dateNaissance.day);

          if ((dateAnniv.compareTo(limite2) >= 0) &&
              (dateAnniv.compareTo(limite1) <= 0)) {
            flagAdd = true;
            break;
          }
        } else {
          dateAnniv = new DateTime(
              today.year, u.dateNaissance.month, u.dateNaissance.day);
          if ((dateAnniv.compareTo(today) >= 0) &&
              (dateAnniv.compareTo(limite1) <= 0)) {
            flagAdd = true;
            break;
          }
        }
      } while (false);
      if (flagAdd == true) {
        lRet.add(new EntAnniversaire(u, dateAnniv));
      }
    }

    lRet.sort((a, b) {
      return a.dateAnniversaire.compareTo(b.dateAnniversaire);
    });
    _listAnniversaireSubject.add(lRet);

  }


  /*Future<T> readDocument<T extends EntFB>(
      String collectionName, String idDocument) {
    Completer<T> completer = new Completer<T>();

    final userRef = FirebaseFirestore.instance
        .collection(collectionName)
        .withConverter<T>(
          fromFirestore: (snapshot, _) =>
              new T(snapshot.data()!)(T as EntFB).convertToFB(snapshot.data()!),
          toFirestore: (movie, _) => movie.toJson(),
        );

    return (completer.future);
  }*/

  /*
  Future<dynamic> resetPassword(String email) async {
    Future<dynamic> ret;

    if (kIsWeb) {
      ret = _web.resetPassword(email);
    } else {
      ret = _mob.resetPassword(email);
    }
    return ret;
  }
*/
  /*Future<EntUser> createUser(EntUser userCreate, String password,File f) async {
    try {
      var result = await _auth.createUserWithEmailAndPassword(
          email: userCreate.email, password: password);

      sendMailConfirmation();
      if(f != null) userCreate.avatar= await _setAvatar(f,result.user.uid);
      userCreate.id = result.user.uid;
      await _createUser(userCreate);
      _currentUser = userCreate;

      _initFromAuthResult(result);

      return _currentUser;
    } catch (e) {
      throw new AuthException(e.code, e.message);
    }
  }*/

  /* Future<void> modifEmail(String newEmail) async {
    Future<void> ret;
    if (kIsWeb) {
      ret = _web.modifEmail(newEmail);
    } else {
      ret = _mob.modifEmail(newEmail);
    }
    return (ret);
  }

  Future<void> modifPassword(String newPassword) async {
    Future<void> ret;
    if (kIsWeb) {
      ret = _web.modifPassword(newPassword);
    } else {
      ret = _mob.modifPassword(newPassword);
    }
    return (ret);
  }
*/
/*void _initFromAuthResult(AuthResult result) {
    _currentUser.idp = result.user.providerId;
    _currentUser.email = result.user.email;
    _currentUser.flagVerif = result.user.isEmailVerified;
  }*/

/*Future<EntUser> _getUserById(String id) async {
    DataSnapshot snapshot = await _db.child("users").child(id).once();

    EntUser u;

    u = EntUser.fromFB(snapshot.value, id);

    return (u);
  }*/

/************void sendMailConfirmation() async{
    FirebaseUser fbu = await _auth.currentUser();

    await fbu.sendEmailVerification();

    }

 */

//}


// MIGRATION

  void migration() async{
    List<EntUser> lu = await getAllUserRTDB();
    lu.forEach((element) {
      createUser(element);
    });

    List<EntEquipe> le = await getAllEquipe();
    le.forEach((element) {
      element.logo="https://firebasestorage.googleapis.com/v0/b/scsc-veteran-dev-640ee.appspot.com/o/equipes%2Flogo_provence.png?alt=media&token=ce5eb04c-b666-4796-b365-c8f250b64dd1";
      //createEquipe(element);
    });
  }
  void createUser(EntUser u){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_USERS);

    u.flagEntrainement=true;
    refCollection.doc(u.id).set(u.toFB());


  }

  void createEquipe(EntEquipe e){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_EQUIPES);

    refCollection.doc(e.id).set(e.toFB());


  }

  Future<List<EntUser>> getAllUserRTDB() async {
    Completer<List<EntUser>> completer = new Completer<List<EntUser>>();

    List<EntUser> lu = [];

    DatabaseReference dbRef=FirebaseDatabase.instance.ref("users");

    dbRef.once().then((data) {


      EntUser elem;


      var list=data.snapshot.children;
      if(list != null){
        list.forEach((ss) {
          elem = EntUser.fromRTDB(ss.value! as Map,ss.key!);
          lu.add(elem);
        });
        //if (data.snapshot.value != null) {
        completer.complete(lu);
      }
    }
    ,onError: (Object o) {
          String ss          ="";
        });


    return (completer.future);
  }

  Future<List<EntEquipe>> getAllEquipe() async {
    Completer<List<EntEquipe>> completer = new Completer<List<EntEquipe>>();

    List<EntEquipe> le = [];

    FirebaseDatabase.instance.ref("equipes").once().then((data) {
      EntEquipe elem;

      var list=data.snapshot.children;
      if(list != null){
        list.forEach((ss)  {
          elem = EntEquipe.fromRTDB(ss.value! as Map,ss.key!);
          le.add(elem);
        });
        completer.complete(le);
      }
    });


    return (completer.future);
  }
}
