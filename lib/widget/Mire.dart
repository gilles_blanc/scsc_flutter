import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'dart:async';
import 'package:scsc_veteran/services/AuthService.dart';
import 'package:scsc_veteran/services/NotificationService.dart';
import 'package:scsc_veteran/services/UserService.dart';
import 'package:scsc_veteran/widget/Avatar/Avatar.dart';


class Mire extends StatefulWidget {
  Mire();

  @override
  _MirePageState createState() => _MirePageState();
}

class _MirePageState extends State<Mire> {
  late StreamSubscription<User?> _sub;

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  void dispose() {
    if (_sub != null) {
      //_sub.cancel();
    }
    super.dispose();
  }

  void _init() async {
    if (!kIsWeb) await Firebase.initializeApp();








    // TODO
    /*ParamService.getInstance().getParam().then((p) {
      if (p.open == true) {
        _start();
      } else {
        Util.alert(
            context,
            "Application indisponible... veuillez contacter qui vous savez ...",
            "Maintenance");
      }
    });*/
    //Navigator.pushNamed(context, "/Login");
    _start();
    // END TODO
  }

  _start() {

    _sub=AuthService.getInstance().getObsUserFB().listen((userFB) {
      print ("LISTERNER USER dans MIRE");

      if (userFB != null) {
        Navigator.pushNamed(context, "/Home");
        
      } else {
        Navigator.pushNamed(context, "/Login");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        
        //border: Border.all(color: Colors.green),

        image: DecorationImage(
            image: AssetImage('assets/images/logo.jpg'), fit: BoxFit.fitWidth),
      ),
    );
  }
}
