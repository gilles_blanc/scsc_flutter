import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';
import 'package:scsc_veteran/common/Constantes.dart';
import 'package:scsc_veteran/models/EntEntrainement.dart';
import 'package:collection/src/iterable_extensions.dart';

class EntrainementService {
  static int TRAINIG_DAY=3;

  late BehaviorSubject<EntEntrainement?> _trainingSubject;
  late BehaviorSubject<List<EntEntrainement>> _interruptionSubject;
  static EntrainementService? _singleton;

  static void razSingleton() {
    _singleton = null;
    
  }

  static EntrainementService getInstance() {
    if (_singleton == null) {
      _singleton = EntrainementService._();
    }
    return (_singleton!);
  }

  EntrainementService._() {
    //_initListenerEntrainement();
    _initListenerInterruption();
  }

  void _refreshEntrainement() {
    CollectionReference refCollection =
        FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_ENTRAINEMENTS);



    refCollection.snapshots().listen((l) async {
      DateTime next = await _findNextDateTraining();

      List<EntEntrainement> le = [];
      EntEntrainement e;

      l.docs.forEach((QueryDocumentSnapshot<Object?> o) async {
        if (o.exists) {
          e = EntEntrainement.fromFB(o);
          if(!e.date.isBefore(DateTime.now())) le.add(e);
        }
      });


      EntEntrainement? nextTraining = le.firstWhereOrNull((e) {
        bool ret=false;
        if(e.date.compareTo(next) == 0) ret=true;
        return(ret);
      });

      _trainingSubject.add(nextTraining);

      if(nextTraining == null){
        nextTraining = EntEntrainement();
        nextTraining.date=next;
        majTraining(nextTraining);
      }

    });
  }

  void _initListenerInterruption() {
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_INTERRUPTIONS);

    _interruptionSubject = BehaviorSubject<List<EntEntrainement>>();
    _trainingSubject = BehaviorSubject<EntEntrainement?>();

    refCollection.snapshots().listen((l) async {

      List<EntEntrainement> le = [];
      EntEntrainement e;

      l.docs.forEach((QueryDocumentSnapshot<Object?> o) async {
        if (o.exists) {
          e = EntEntrainement.fromFB(o);
          le.add(e);
        }
      });

      _interruptionSubject.add(le);
      _refreshEntrainement();
    });
  }

  Stream<EntEntrainement?> getObsNextEntrainement() {
    return (_trainingSubject.stream);
  }

  Stream<List<EntEntrainement>> getObsInterruption() {
    return (_interruptionSubject.stream);
  }

  Future<void> majTraining(EntEntrainement e){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_ENTRAINEMENTS);

    Future<void> ret;
    if(e.id == "") ret=refCollection.add(e.toFB());
    else ret=refCollection.doc(e.id).set(e.toFB());

    return(ret);

  }

  Future<void> majInterruption(EntEntrainement e){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_INTERRUPTIONS);

    Future<void> ret;
    if(e.id == "") ret=refCollection.add(e.toFB());
    else ret=refCollection.doc(e.id).set(e.toFB());

    return(ret);

  }

  Future<void> deleteInterruption(EntEntrainement e){
    CollectionReference refCollection =
    FirebaseFirestore.instance.collection(Constantes.COLLECTION_FB_INTERRUPTIONS);

    Future<void> ret;
    ret=refCollection.doc(e.id).delete();

    return(ret);

  }

  Future<DateTime> _findNextDateTraining() async{
    //var monday=1;
    DateTime now = new DateTime.now();
    now = new DateTime(now.year, now.month, now.day, Constantes.TRAINING_HOUR);

    List<EntEntrainement> li=await getObsInterruption().first;
      while((now.weekday != TRAINIG_DAY) || (_isInInterruption(li,now) == true))
      {
        now=now.add(new Duration(days: 1));
      }

      return(now);
  }

  bool _isInInterruption(List<EntEntrainement> li,DateTime d){
    bool ret=false;

    EntEntrainement? ei = li.firstWhereOrNull((e) {
      bool ret=false;
      if(e.date.compareTo(d) == 0) ret=true;
      return(ret);
    });
    if(ei != null) ret=true;
    return(ret);
  }
}
